package com.cheney.common;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class Swagger2Config{
	
	@Bean
    public Docket ProductApi() {
        return new Docket(DocumentationType.SWAGGER_2);
             /*   .genericModelSubstitutes(DeferredResult.class)
                .useDefaultResponseMessages(false)
                .forCodeGeneration(false)
                .pathMapping("/")
                .select()
                .build()
                .apiInfo(productApiInfo());*/
    }

    /*private ApiInfo productApiInfo() {
        ApiInfo apiInfo = new ApiInfo("Api Documentation",
                "Api Documentation",
                "1.0.0",
                "",
                "Cheney",
                "",
                "");
        return apiInfo;
    }*/
}


