package com.cheney.common;

import javax.annotation.Resource;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.cheney.share.mid.prop.ShareProperties;


@Component
@Aspect
public class LogAop {

	Logger logger = LoggerFactory.getLogger(LogAop.class);

	@Resource
	ShareProperties shareProps;


	@Pointcut("execution(* com.x*.*.api.controller..*.*(..))")
	public void executeService() {

	}

//	@AfterReturning(value = "executeService()", returning="rtn")
//	public void doAfterAdvice(JoinPoint joinPoint, Object rtn) {
//		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
//		HttpServletRequest request = (HttpServletRequest) requestAttributes
//				.resolveReference(RequestAttributes.REFERENCE_REQUEST);
//		MethodSignature sign = (MethodSignature) joinPoint.getSignature();
//		Log log = sign.getMethod().getAnnotation(Log.class);
//		if (log != null) {
//			String userToken = request.getHeader("utk");
//			MyUser user = null;
//			if (StringUtils.isNotBlank(userToken) && userToken.length() > 30) {
//				UToken token = UToken.from(userToken, shareProps.getAeskey());
//				user = myUserMapper.selectByPrimaryKey(token.getId());
//			} else {
//				@SuppressWarnings("unchecked")
//				JSONObject json = new JSONObject((Map<String, Object>) rtn);
//				if ( json.getInteger("code") == 0) {
//					user = myUserMapper
//							.selectByPrimaryKey(json.getJSONObject("data").getLong("id"));
//				}
//			}
//			/*if (user != null) {
//				CmsOperationLog olog = new CmsOperationLog();
//				olog.setAction(log.action());
//				olog.setRemark(log.remark());
//				olog.setOperatorId(user.getId());
//				String operatorName = StringUtils.isNotBlank(user.getNickName())
//						? String.format("%s-%s", user.getNickName(), user.getPhone()) : user.getPhone();
//				operatorName = StringUtils.isBlank(operatorName) ? user.getUserName(): operatorName;
//				olog.setOperatorName(operatorName);
//				olog.setOperateTime(new Date());
//				olog.setMethod(String.format("%s-%s.%s()", request.getRequestURI(), joinPoint.getTarget().getClass().getName(), sign.getName()));
//				String ip = request.getHeader("x-forwarded-for");
//				if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
//					ip = request.getHeader("Proxy-Client-IP");
//				}
//				if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
//					ip = request.getHeader("WL-Proxy-Client-IP");
//				}
//				if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
//					ip = request.getRemoteAddr();
//				}
//				ip = ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
//				olog.setIp(ip);
//				cmsOperationLogMapper.insert(olog);
//			}*/
//		}
//	}

}
