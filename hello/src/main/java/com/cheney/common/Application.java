package com.cheney.common;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import springfox.documentation.swagger2.annotations.EnableSwagger2;


@EnableSwagger2
@EnableAutoConfiguration
@SpringBootApplication
@EnableTransactionManagement
@EnableScheduling
@MapperScan({ "com.cheney.modules.maye.db.mapper"/*"com.xshop.us.db.mapper", "com.xshop.mt.db.mapper", "com.xshop.td.db.mapper", "com.xshop.cms.db.mapper",
"com.xshop.cms.mapper", "com.xshop.td.mapper", "com.xshop.mt.mapper" ,"com.xmld.us.db.mapper", "com.xmld.us.mapper","com.tuohu.pd.db.mapper"*/ })
@ComponentScan(value = {"com.cheney.modules","com.cheney.share"/* "com.xshop", "com.x", "com.xmld","com.tuohu"*/ })
public class Application{

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
	
	

	/*public CommonsMultipartResolver multipartResolver() {
		CommonsMultipartResolver res = new CommonsMultipartResolver();
		res.setMaxUploadSize(5242880);// =5 M
		res.setMaxInMemorySize(10240);// =10 K
		return res;
	}*/

	/*@Bean(name = "captchaProducer")
	public DefaultKaptcha getKaptchaBean() {
		DefaultKaptcha defaultKaptcha = new DefaultKaptcha();
		Properties properties = new Properties();
		properties.setProperty("kaptcha.border", "yes");
		properties.setProperty("kaptcha.border.color", "105,179,90");
		properties.setProperty("kaptcha.textproducer.font.color", "blue");
		properties.setProperty("kaptcha.image.width", "110");
		properties.setProperty("kaptcha.image.height", "40");
		properties.setProperty("kaptcha.textproducer.font.size", "30");
		properties.setProperty("kaptcha.session.key", "code");
		properties.setProperty("kaptcha.textproducer.char.length", "4");
		properties.setProperty("kaptcha.textproducer.font.names", "宋体,楷体,微软雅黑");
		Config config = new Config(properties);
		defaultKaptcha.setConfig(config);
		return defaultKaptcha;
	}*/

	
}
