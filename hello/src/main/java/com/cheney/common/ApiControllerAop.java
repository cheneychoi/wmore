package com.cheney.common;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import com.alibaba.fastjson.JSON;
import com.cheney.share.api.controller.BaseController;
import com.cheney.share.mid.model.AToken;
import com.cheney.share.mid.prop.ShareProperties;

@Component
@Aspect
public class ApiControllerAop extends BaseController {

	Logger logger = LoggerFactory.getLogger(ApiControllerAop.class);

	@Resource
	ShareProperties shareProps;


	/*@Pointcut("execution(* com.x*.*.api.controller..*.*(..))")
	public void executeService() {

	}*/
	@Pointcut("execution(* com.*.*.api.controller..*.*(..))")
	public void executeService() {

	}

	@Around("executeService()")
	public Object doAroundAdvice(ProceedingJoinPoint proceedingJoinPoint) throws Throwable {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		HttpServletRequest request = (HttpServletRequest) requestAttributes
				.resolveReference(RequestAttributes.REFERENCE_REQUEST);
		/*String uri = request.getRequestURI();
		if (uri.equals("/api/auth") || uri.contains("/td/yijipay/notify")
				|| uri.contains("/td/wxpay/notify") || uri.contains("/td/alipay/notify") 
				|| uri.contains("/us/qq/login") || uri.contains("/us/wx/mp/login") 
				|| uri.contains("/us/wx/open/app/login")|| uri.contains("/us/captcha")) {
			return proceedingJoinPoint.proceed();
		}*/
		String appId = request.getHeader("appId");
		String accessToken = request.getHeader("atk");
		if (StringUtils.isBlank(appId) || StringUtils.isBlank(accessToken)) {
			logger.info(">>>> invalid request url(with out appId or atk in request header): {}",
					request.getRequestURL().toString());
			return fail("请联系管理员申请api访问权限!");
		}
		/*MethodSignature sign = (MethodSignature) proceedingJoinPoint.getSignature();
		CmsRoleCheck anno = sign.getMethod().getAnnotation(CmsRoleCheck.class);
		if (anno != null) {
			String userToken = request.getHeader("utk");
			UToken token = UToken.from(userToken, shareProps.getAeskey());
			try {
				CmsAuthOut out = cmsUserService.authByUserId(token.getId());
				if ((out.getRole() & anno.weight()) != anno.weight()) {
					return fail("权限不足，请联系管理员申请权限!");
				}
			} catch (HelperException e) {
				logger.warn("!!!! helper exception: {}", e);
				return fail(e.getMessage());
			}
		}*/
		try {
			AToken atk = AToken.from(accessToken, shareProps.getAeskey());
			if (atk != null && atk.getAppId().equals(appId)) {
				return proceedingJoinPoint.proceed();
			} else {
				logger.info(">>>> request url: {}", request.getRequestURL().toString());
				logger.warn("!!!! invalid request: appId={}, access token={}", appId, JSON.toJSONString(atk));
				return fail("非法的访问token!");
			}
		} catch (Throwable e) {
			throw e;
		}
	}
}
