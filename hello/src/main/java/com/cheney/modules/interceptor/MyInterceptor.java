package com.cheney.modules.interceptor;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.alibaba.fastjson.JSON;
import com.cheney.modules.maye.db.mapper.AdminRoleMapper;
import com.cheney.modules.maye.db.mapper.AuthorityMapper;
import com.cheney.modules.maye.db.mapper.RoleAuthorityMapper;
import com.cheney.modules.maye.db.model.AdminRole;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.redis.AuthKey;
import com.cheney.share.mid.redis.RedisService;

public class MyInterceptor implements HandlerInterceptor {

	@Autowired
	AdminRoleMapper adminRoleMapper;

	@Autowired
	RoleAuthorityMapper roleAuthorityMapper;

	@Autowired
	AuthorityMapper authorityMapper;
	
	@Autowired
	RedisService redisService;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		
		String url = request.getRequestURI();
		System.out.println(url);
//		HandlerMethod h = (HandlerMethod) handler;
//		String method = h.getMethod().getName();
		
		if (url.indexOf("/wmore/") != -1||url.indexOf("/admin/login")!= -1||url.indexOf("/error")!= -1||url.indexOf("/wmore_back/admin/matchAdmin")!= -1) {
			return true;
		}
		
		Cookie[] cookies = request.getCookies();
		String value = null;
		if (cookies.length != 0 ) {
			for (Cookie cookie : cookies) {
				if ("username".equals(cookie.getName())) {
					value = cookie.getValue();
					break;
				}
			}
		}
		if (value == null) {
			response.sendRedirect("/admin/login");
			return false;
		}
		
		List<String> authList = new ArrayList<>();
		authList.add("/wmore_back/admin/updateAdmin");
		authList.add("/wmore_back/admin/updatePsd");
		authList.add("/wmore_back/company/updateCompany");
		authList.add("/wmore_back/company/selectUserByOne");
		authList.add("/wmore_back/course/getCourseByFuzzy");
		authList.add("/wmore_back/picture/editBanner");
		authList.add("/wmore_back/course/updateCourse");
		authList.add("/wmore_back/noticeMasterplate/updateNoticeMasterplateById");
		authList.add("/wmore_back/noticeMasterplate/updateNoticeMasterplateByType");
		authList.add("/wmore_back/point/updatePointRule");
		authList.add("/wmore_back/role/updateAuthority");
		authList.add("/wmore_back/user/getUser");
		authList.add("/wmore_back/user/getAllUser");
		authList.add("/wmore_back/user/selectUserByOne");
		authList.add("/wmore_back/user/selectUserByMany");
		authList.add("/wmore_back/index/info");
		authList.add("/admin/login");
		authList.add("/admin/auth");
		authList.add("/admin/index");
		authList.add("/wmore_back/loginOut");
		authList.add("/error");
		authList.add("/wmore_back/company/selectCompanyByOne");
		authList.add("/wmore_back/role/updateRole");
		authList.add("/wmore_back/log/getLogByCondition");
		authList.add("/wmore_back/course/getCourseById");
		authList.add("/wmore_back/user/getUserById");
		authList.add("/wmore_back/course/getCourseByFuzzy");
		authList.add("/wmore_back/course/getCompanyByName");
		authList.add("/wmore_back/course/getCompanyByName1");
		authList.add("/wmore_back/course/getCourseByName1");
		authList.add("/wmore_back/user/getCompanyById");
		authList.add("/wmore_back/user/getUserById");
		authList.add("/wmore_back/course/getCourseByName");
		authList.add("/wmore_back/role/getRoleAuthority");
		authList.add("/wmore_back/user/selectUserByMany");
		authList.add("/wmore_back/course/getCourseByCompany");
		authList.add("/wmore_back/course/getAllCourses");
		authList.add("/wmore_back/user/deleteExcel");
		authList.add("/wmore_back/user/updateMember");
		if (authList.contains(url)) {
			return true;
		}
		HttpSession session = request.getSession();
		Long id = (Long) session.getAttribute("id");
		session.setAttribute("id", id);
		session.setMaxInactiveInterval(30*60*1000);
		if(id==null) {
			response.sendRedirect("/admin/login");
			return false;
		}
		boolean r = redisService.exists(AuthKey.getKey, id.toString());
		List<String> lists = new ArrayList<>();
		if (r) {
			String json = redisService.get(AuthKey.getKey, id.toString(), String.class);
			lists = JSON.parseArray(json, String.class);
		}else {
			AdminRole adminRole = adminRoleMapper.selectByPrimaryKey(id);
			Long roleId = adminRole.getRoleId();
			lists = authorityMapper.getAuthorityByRole(roleId);	
			redisService.set(AuthKey.getKey, id.toString(),JSON.toJSONString(lists).toString());
		}
		
		Boolean result = lists.contains(url);
		if (result) {
			return true;
		}
		throw new HelperException(CodeMsg.NO_AUTHORITY_ERROR.getCode(), CodeMsg.NO_AUTHORITY_ERROR.getMsg());
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {
		// TODO Auto-generated method stub

	}

	@Override
	public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub

	}
}
