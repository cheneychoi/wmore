package com.cheney.modules.interceptor;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;


@Configuration
public class WebConfig extends WebMvcConfigurerAdapter {

	@Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(myInterceptor())
                .addPathPatterns("/**");    // 拦截所有请求，通过判断是否有 @LoginRequired 注解 决定是否需要登录
        super.addInterceptors(registry);
        
        InterceptorRegistration addInterceptor = registry.addInterceptor(myInterceptor());
        
        // 排除配置
        addInterceptor.excludePathPatterns("/admin/login");
        addInterceptor.excludePathPatterns("/wmore");
        // 拦截配置
        addInterceptor.addPathPatterns("/**");

    }

    @Bean
    public MyInterceptor myInterceptor() {
        return new MyInterceptor();
    }


}
