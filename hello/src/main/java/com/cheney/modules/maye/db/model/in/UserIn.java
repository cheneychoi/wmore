package com.cheney.modules.maye.db.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.User;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "用户-入")
public class UserIn {

	@ApiModelProperty(value = "用户姓名")
	private String username;
	
	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "手机号")
	private Integer mobile;

	@ApiModelProperty(value = "微信ID")
	private String wechatOpenid;

	@ApiModelProperty(value = "性别")
	private Integer sex;

	@ApiModelProperty(value = "生日")
	private Date birthday;

	@ApiModelProperty(value = "城市")
	private String city;

	@ApiModelProperty(value = "学历")
	private String education;

	@ApiModelProperty(value = "行业")
	private String industry;

	@ApiModelProperty(value = "爱好")
	private String hobby;

	@ApiModelProperty(value = "公司ID")
	private Long companyId;

	@ApiModelProperty(value = "是否是会员")
	private Integer isMember;

	@ApiModelProperty(value = "职位")
	private Integer position;

	@ApiModelProperty(value = "积分")
	private Integer points;

	@ApiModelProperty(value = "头像ID")
	private String profilePhoto;

	public User toUser() {
		User user = new User();
		BeanUtils.copyProperties(this, user);
		return user;
	}
	
}