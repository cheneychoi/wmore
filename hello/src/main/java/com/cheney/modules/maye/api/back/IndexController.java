package com.cheney.modules.maye.api.back;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {
	
	@RequestMapping("/admin/login")
	public String login() {
		return "login";
	}
	
	
	@RequestMapping("/admin/auth")
	public String auth() {
		return "NoAuthority";
	}
	
	@RequestMapping("/admin/index")
	public String index() {
		return "index";
	}
	
}
