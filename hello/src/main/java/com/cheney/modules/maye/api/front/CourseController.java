package com.cheney.modules.maye.api.front;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.db.model.out.CourseDetailOut;
import com.cheney.modules.maye.db.model.out.CourseUserOut;
import com.cheney.modules.maye.db.model.out.SignOut;
import com.cheney.modules.maye.mid.service.CourseService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "课程")
@Controller
@RequestMapping(value = "/wmore")
public class CourseController extends BaseController {
	
	Logger logger = LoggerFactory.getLogger("course");
	
	@Autowired
	CourseService courseService;

	@ApiOperation(value = "当前登陆账号可约课列表", notes = "返回数据" )
	@RequestMapping(value = "/course/getCourse", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCourseByCourseAuthority(@ApiParam(value = "用户id") @RequestParam("id") Long id,
			@ApiParam(value="日期") @RequestParam("month") String month) {
		logger.info("------------------进入当前登陆账号可约课列表--------------");
		logger.info("userId---------------"+id);
		logger.info("日期--------------------"+month);
		List<CourseUserOut> courseUserOuts = courseService.getCourseByCourseAuthority(id,month);
		logger.info("返回数据------------"+courseUserOuts.size()+":"+courseUserOuts);
		return success(courseUserOuts);
	}
	
	@ApiOperation(value = "查询图文详情和课程预约状态", notes = "返回数据" )
	@RequestMapping(value = "/course/getCourseDetail", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCourseDetail(@ApiParam(value = "用户id") @RequestParam("id") Long userId,
			@ApiParam(value="课程") @RequestParam("courseId") Long courseId) {
		logger.info("------------------查询图文详情和课程预约状态--------------");
		logger.info("userId---------------"+userId);
		logger.info("课程ID--------------------"+courseId);
		CourseDetailOut courseDetailOut = courseService.getCourseDetail(userId, courseId);
		logger.info("返回数据------------"+courseDetailOut);
		return success(courseDetailOut);
	}
	@ApiOperation(value = "获取签到所需信息", notes = "返回数据" )
	@RequestMapping(value = "/course/getSignInfo", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getSignInfo(
			@ApiParam(value="课程") @RequestParam("courseId") Long courseId) {
		logger.info("------------------获取签到所需信息--------------");
		logger.info("课程ID--------------------"+courseId);
		SignOut signOut = courseService.getSignInfo(courseId);
		logger.info("返回数据------------"+signOut.toString());
		return success(signOut);
	}
}
