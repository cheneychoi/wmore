package com.cheney.modules.maye.mid.service;

import com.cheney.modules.maye.db.model.Config;
import com.cheney.share.mid.prop.AliDayuProperties;
import com.cheney.share.mid.prop.AliOSSProperties;

/**
 * 后台配置模块
 * 
 * @ClassName: CourseService
 * @Description: TODO
 * @author: WangBo
 * @date: 2018年7月6日 上午11:14:21
 */
public interface ConfigService {

	/**
	 * 短信服务器配置
	 * 
	 * @param config
	 *            void
	 * @author WangBo
	 * @date 2018年7月12日上午11:58:23
	 */
	void updateSMSConfig(Long id,Integer type,String appid,String appkey,String gateway,String bucket);

	/**
	 * oss服务器配置
	 * 
	 * @param config
	 *            void
	 * @author WangBo
	 * @date 2018年7月12日上午11:58:31
	 */
	void updateOSSConfig(Long id, Integer type,String appid,String appkey,String sign,String product,String phoneRegisterTmid,String phoneBindTmid);

	/**
	 * 获取配置信息
	 * 
	 * @param id
	 * @return Config
	 * @author WangBo
	 * @date 2018年7月18日下午4:31:54
	 */
	Config getConfig(Long id);

	AliDayuProperties getSMSProps();

	AliOSSProperties getOSSProps();

}
