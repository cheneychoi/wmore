package com.cheney.modules.maye.api.back;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.db.model.out.ActionOut;
import com.cheney.modules.maye.mid.service.ActionService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/wmore")
public class ActionBackController extends BaseController {
	@Autowired
	ActionService actionService;

	@ApiOperation(value = "根据时间和名字查询会员的所有过往行为及积分等信息 ", notes = "返回数据", response = ActionOut.class)
	@RequestMapping(value = "/action/getActionByConditions", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getActionByConditions(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize,
			@ApiParam(value = "起始时间") @RequestParam(value = "startTime", required = false) String startTime,
			@ApiParam(value = "结束时间") @RequestParam(value = "endTime", required = false) String endTime,
			@ApiParam(value = "会员名称") @RequestParam(value = "userName", required = false) String userName) {
		return success(actionService.getActionByConditions(pageNo, pageSize, startTime, endTime, userName));

	}

	@ApiOperation(value = "查询所有会员的所有过往行为及积分等信息 ", notes = "返回数据", response = ActionOut.class)
	@RequestMapping(value = "/action/getAllActions", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAllActions() {
		return success(actionService.getAllActions());
	}
}
