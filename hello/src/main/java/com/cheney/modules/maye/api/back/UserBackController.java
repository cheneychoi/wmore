package com.cheney.modules.maye.api.back;

import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.alibaba.fastjson.JSONObject;
import com.cheney.modules.maye.db.model.User;
import com.cheney.modules.maye.db.model.in.AddUserIn;
import com.cheney.modules.maye.db.model.in.UserAddIn;
import com.cheney.modules.maye.db.model.in.UserUpdateIn;
import com.cheney.modules.maye.db.model.out.UcOut;
import com.cheney.modules.maye.db.model.out.UserOut;
import com.cheney.modules.maye.mid.service.UserService;
import com.cheney.share.api.controller.BaseController;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.redis.AuthKey;
import com.cheney.share.mid.redis.RedisService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "用户-后台")
@Controller
@RequestMapping("/wmore_back")
public class UserBackController extends BaseController {
	@Autowired
	UserService userService;
	
	@Autowired
	RedisService redisService;
	
	Logger logger = LoggerFactory.getLogger("user");

	@ApiOperation(value = "后台录入员工信息", notes = "用于后台录入员工信息以及绑定企业")
	@RequestMapping(value = "/user/addUser", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> addUser(@RequestBody AddUserIn in) {
		userService.addUser(in.toUser());
		return success();
	}

	@ApiOperation(value = "企业会员的查", notes = "", response = UserOut.class)
	@RequestMapping(value = "/user/getUserMember", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getUserMember(@ApiParam(value = "公司id") @RequestParam("companyId") Long companyId,
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		Pagination<User> user = userService.getUserMember(companyId, pageNo, pageSize);
		return success(user);
	}
	@ApiOperation(value = "企业会员的按企业筛选", notes = "")
	@RequestMapping(value = "/user/getCompanyById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCompanyById(@ApiParam(value = "公司id") @RequestParam(value="companyId",required=false) Long companyId
			) {
		return success(userService.getCompanyById(companyId));
	}
	
	@ApiOperation(value = "根据ID查询员工", notes = "")
	@RequestMapping(value = "/user/getUserById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getUserById(@ApiParam(value="员工id") @RequestParam("id") Long id){
		return success(userService.getUserById(id));
	}

	@ApiOperation(value = "企业会员的增", notes = "")
	@RequestMapping(value = "/user/insertUserMember", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> insertUserMember(UserAddIn in,HttpSession session) {
		Long userId = (Long) session.getAttribute("id");
		return success(userService.insertUserMember(userId,in));
	}

	@ApiOperation(value = "删除企业员工", notes = "")
	@RequestMapping(value = "/user/deleteMember", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> deleteByPrimaryKey(@ApiParam(value = "用户id") @RequestParam("id") Long id,HttpSession session) {
		Long userId = (Long) session.getAttribute("id");
		userService.deleteByPrimaryKey(userId,id);
		return success();
	}

	@ApiOperation(value = "编辑企业员工", notes = "")
	@RequestMapping(value = "/user/updateMember", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateByPrimaryKeySelective(UserUpdateIn in) {
		return success(userService.updateByPrimaryKeySelective(in.toUserUpdateIn()));
	}

	@ApiOperation(value = "冻结企业员工", notes = "修改企业员工是否冻结的状态")
	@RequestMapping(value = "/user/updateUserFrozen", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateUserFrozen(@ApiParam(value = "用户id") @RequestParam("id") Long id,
			@ApiParam(value="员工状态") @RequestParam("state") Integer state) {
		userService.updateUserFrozen(id,state);
		return success();
	}

	@ApiOperation(value = "平台会员详细列表", notes = "", response = UserOut.class)
	@RequestMapping(value = "/user/getUser", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getUser() {
		return success(userService.getUser());
	}
	
	@ApiOperation(value = "分页平台会员详细列表", notes = "")
	@RequestMapping(value = "/user/getAllUser", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAllUser(@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		long da= System.currentTimeMillis();
		logger.info("------------------------------进入该方法----------------------------");
		Pagination<UcOut> user = userService.getAllUser(pageNo, pageSize);
		long db=System.currentTimeMillis();
		long d=db-da;
		logger.info("时间================================="+d);
		return success(user);
	}

	@ApiOperation(value = "企业员工按真实姓名或手机号或者邮箱条件查询", notes = "可以通过真实姓名或手机号或者邮箱查询企业")
	@RequestMapping(value = "/user/selectUserByOne", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> selectUserByOne(
			@ApiParam(value = "姓名") @RequestParam(value = "name", required = false) String name,
			@ApiParam(value = "手机号") @RequestParam(value = "mobile", required = false) String mobile,
			@ApiParam(value = "邮箱") @RequestParam(value = "email", required = false) String email) {
		return success(userService.selectUserByOne(name, mobile, email));
	}

	@ApiOperation(value = "企业员工按时间或状态或公司名称或职位或是否是会员条件查询", notes = "可以通过时间或状态或公司名称或职位或是否是会员查询员工返回多条")
	@RequestMapping(value = "/user/selectUserByMany", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> selectUserByMany(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize,
			@ApiParam(value = "企业名称") @RequestParam(value = "companyName", required = false) String companyName,
			@ApiParam(value = "员工状态") @RequestParam(value = "state", required = false) Integer state,
			@ApiParam(value = "职位") @RequestParam(value = "position", required = false) Integer position,
			@ApiParam(value = "是否是会员") @RequestParam(value = "isMember", required = false) Integer isMember,
			@ApiParam(value = "姓名") @RequestParam(value = "name", required = false) String name,
			@ApiParam(value = "手机号") @RequestParam(value = "mobile", required = false) String mobile,
			@ApiParam(value = "邮箱") @RequestParam(value = "email", required = false) String email) {

		return success(userService.selectUserByMany(pageNo, pageSize, companyName, state, position, isMember,name,mobile,email));
	}
	
	@ApiOperation(value = "导出会员Excel", notes = "")
	@RequestMapping(value = "/user/exportExcel", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> exportExcel(@ApiParam(value = "企业Id") @RequestParam(value = "companyId", required = false)Long companyId){
		String path = userService.exportExcel(companyId);
		return success(path);
	}
	
	@ApiOperation(value = "导入会员Excel", notes = "")
	@RequestMapping(value = "/user/importExcel", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> importExcel(HttpServletRequest request){
		
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
		MultipartFile file = multipartHttpServletRequest.getFile("file");
		JSONObject jsonObject = userService.importExcel(file);
		return success(jsonObject);
	}
	
	@ApiOperation(value = "删除Excel", notes = "")
	@RequestMapping(value = "/user/deleteExcel", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> deleteExcel(@ApiParam(value = "路径") @RequestParam(value = "url")String url){
		userService.deleteExcel(url);
		return success();
	}
	
	@ApiOperation(value = "首页信息", notes = "")
	@RequestMapping(value = "/index/info", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getIndexInfo(){
		JSONObject jsonObject = userService.getInfo();
		return success(jsonObject);
	}
	@ApiOperation(value = "后台退出", notes = "")
	@RequestMapping(value = "/loginOut", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> LoginOut(HttpSession session,HttpServletRequest request,HttpServletResponse response){
		Long id=(Long) session.getAttribute("id");
		redisService.delete(AuthKey.getKey, id.toString());
		Cookie[] cookies = request.getCookies();
		if (cookies.length != 0) {
			for (Cookie cookie : cookies) {
				if ("username".equals(cookie.getName())) {
					cookie.setValue(null);  
                    cookie.setMaxAge(0);// 立即销毁cookie  
                    cookie.setPath("/");  
                    response.addCookie(cookie);  
                    break;  
				}
			}
		}
		session.removeAttribute("id");
		return success();
	}
}
