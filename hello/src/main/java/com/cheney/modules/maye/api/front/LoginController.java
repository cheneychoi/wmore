package com.cheney.modules.maye.api.front;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cheney.modules.maye.mid.service.NoticeService;
import com.cheney.modules.maye.mid.service.UserCourseService;
import com.cheney.modules.maye.mid.service.WeChatLoginService;
import com.cheney.share.api.controller.BaseController;
import com.cheney.share.mid.prop.WeChatProperties;
import com.cheney.share.mid.utils.HttpsUtil;
import com.cheney.share.mid.utils.WeChatCheckUtils;
import com.cheney.share.mid.wechat.JsapiTicketUtil;
import com.cheney.share.mid.wechat.Sign;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;

@Api(value = "登录")
@Controller
@RequestMapping(value = "/wmore")
public class LoginController extends BaseController {

	Logger logger = LoggerFactory.getLogger("login");

	@Resource
	WeChatProperties props;

	@Resource
	WeChatLoginService wechatLoginService;

	@Resource
	UserCourseService userCourseService;

	@Resource
	NoticeService noticeService;

	@ApiModelProperty(value = "微信服务器校验", notes = "用于验证服务器地址的有效性逻辑")
	@RequestMapping(value = "/wechat/check", method = { RequestMethod.GET })
	@ResponseBody
	public String check(@RequestParam(name = "signature") String signature,
			@RequestParam(name = "timestamp") String timestamp, @RequestParam(name = "nonce") String nonce,
			@RequestParam(name = "echostr") String echostr) {

		// 排序
		String sortString = WeChatCheckUtils.sort(props.getToken(), timestamp, nonce);
		// 加密
		String myString = WeChatCheckUtils.sha1(sortString);
		logger.info("myString" + myString);
		// 校验
		if (myString != null && myString != "" && myString.equals(signature)) {
			System.out.println("签名校验通过");
			// 如果检验成功原样返回echostr，微信服务器接收到此输出，才会确认检验完成。
			return echostr;
		} else {
			System.out.println("签名校验失败");// TODO
			return "";
		}
	}

	@ApiModelProperty(value = "调用授权url", notes = "用于后续微信登录")
	@RequestMapping(value = "/wechat/index", method = { RequestMethod.GET })
	@ResponseBody
	public String index(HttpServletResponse response) throws IOException {
		// snsapi_userinfo为获取用户信息 snsapi_base直接登录不需要授权
		String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + props.getAppId() + "&redirect_uri="
				+ URLEncoder.encode(props.getBackUrl(), "GBK") + "&response_type=code&scope=snsapi_userinfo&state=0#wechat_redirect";
		response.sendRedirect(url);
		return "登陆成功";
	}

	@ApiModelProperty(value = "微信登录/扫二维码签到", notes = "用于用户微信授权登录或者员工上课签到")
	@RequestMapping(value = "/wechat/getOpenId", method = { RequestMethod.GET })
	@ResponseBody
	public void getOpenId(@RequestParam(name = "code") String code, @RequestParam(name = "state") Long courseId,
			HttpServletResponse response,HttpSession session) throws IOException {
		logger.info("code--------------" + code);
		logger.info("state--------------" + courseId);

		String url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=" + props.getAppId() + "&secret="
				+ props.getAppSecret() + "&code=" + code + "&grant_type=authorization_code";
		String result = HttpsUtil.httpsRequestToString(url, "GET", null);
		JSONObject jsonObject = JSON.parseObject(result);
		String openid = jsonObject.getString("openid");
		String access_token = jsonObject.getString("access_token");
		logger.info("serviceOpenid--------------" + openid);
		logger.info("access_token--------------" + access_token);
		wechatLoginService.getWeChatUserInfo(access_token, openid);

		
		if (courseId != 0) {
			userCourseService.sign(courseId, openid);
			response.sendRedirect("http://wmore.wearewer.com/QRcode/QRcodeover.html?key="+courseId);
		} else {
			
			session.setAttribute("openId", openid);
			session.setMaxInactiveInterval(30*60*1000);
			String u = "http://wmore.wearewer.com/#/home";
			response.sendRedirect(u);
		}
	}

	@ApiModelProperty(value = "获取用户Id", notes = "用于后续微信登录")
	@RequestMapping(value = "/user/userId", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getUserId(@RequestParam(name = "key", required = false) String key,HttpSession session) {
		logger.info("getUserkey------------------" + key);
		logger.info("-----------进入获取用户Id的方法---------------");
		String openId=(String) session.getAttribute("openId");
		logger.info("-------------------------Session的openId为"+openId);
		String userId = wechatLoginService.getUserId(openId);
		logger.info("获取用户Id-userId---------------" + userId);
		logger.info("-----------结束获取用户Id的方法---------------");
		return success(userId);
	}

	@ApiModelProperty(value = "获取上传头像所需要的参数", notes = "用于后续上传头像")
	@RequestMapping(value = "/user/getParam", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getParam(@RequestParam(name = "url") String url) {
		logger.info("-----------进入获取上传头像所需要的参数的方法---------------");
		String appid = props.getAppId();
		String ticket = JsapiTicketUtil.getJSApiTicket(appid, props.getAppSecret());
		logger.info("ticket------------------" + ticket);
		// 注意 URL 一定要动态获取，不能 hardcode
//		String url = "http://wmore.wearewer.com:8080/#/home";// url是你请求的一个action或者controller地址，并且方法直接跳转到使用jsapi的jsp界面

		Map<String, String> ret = Sign.sign(ticket, url);
		ret.put("appid", appid);
		logger.info("-----------结束获取上传头像所需要的参数的方法---------------");
		return success(ret);
	}

}
