package com.cheney.modules.maye.db.model.in;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Notice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "通知-入")
public class NoticeIn {

	@ApiModelProperty(value = "用户ID")
	private String userId;

	@ApiModelProperty(value = "通知标题")
	private String title;

	@ApiModelProperty(value = "通知内容")
	private String content;

	@ApiModelProperty(value = "是否已读")
	private Integer is_read;

	public Notice toNotice() {
		Notice notice = new Notice();
		BeanUtils.copyProperties(this, notice);
		return notice;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public Integer getIs_read() {
		return is_read;
	}

	public void setIs_read(Integer is_read) {
		this.is_read = is_read;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}