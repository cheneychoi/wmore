package com.cheney.modules.maye.db.model.out;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "用户-出")
public class CompanyOut {
	@ApiModelProperty(value = "企业id")
	private Long id;
	@ApiModelProperty(value = "公司名称")
	private String companyName;
	@ApiModelProperty(value = "公司代号")
	private String companyCode;
	@ApiModelProperty(value = "公司联系人")
	private String companyPerson;
	@ApiModelProperty(value = "联系方式")
	private String companyMobile;
	@ApiModelProperty(value = "公司规模")
	private Integer companyNumber;
	@ApiModelProperty(value = "创建时间")
	private Date createDate;
	
	@ApiModelProperty(value = "企业状态：1：冻结，2：未冻结")
	private Integer companyState;

	public static CompanyOut from(Company company) {
		CompanyOut out = new CompanyOut();
		BeanUtils.copyProperties(company, out);
		return out;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public Integer getCompanyNumber() {
		return companyNumber;
	}

	public void setCompanyNumber(Integer companyNumber) {
		this.companyNumber = companyNumber;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getCompanyState() {
		return companyState;
	}

	public void setCompanyState(Integer companyState) {
		this.companyState = companyState;
	}

	public String getCompanyPerson() {
		return companyPerson;
	}

	public void setCompanyPerson(String companyPerson) {
		this.companyPerson = companyPerson;
	}

	public String getCompanyMobile() {
		return companyMobile;
	}

	public void setCompanyMobile(String companyMobile) {
		this.companyMobile = companyMobile;
	}


}
