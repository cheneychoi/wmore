package com.cheney.modules.maye.mid.impl;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.cheney.modules.maye.db.mapper.CompanyMapper;
import com.cheney.modules.maye.db.mapper.PictureMapper;
import com.cheney.modules.maye.db.model.Company;
import com.cheney.modules.maye.db.model.CompanyExample;
import com.cheney.modules.maye.db.model.Picture;
import com.cheney.modules.maye.mid.service.CompanyService;
import com.cheney.modules.maye.mid.service.LogService;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.prop.AliOSSProperties;
import com.cheney.share.mid.utils.AliOSSUtils;
import com.cheney.share.mid.utils.PictureUtils;
import com.cheney.share.mid.utils.ToolUtil;

@Service
public class CompanyServiceImpl implements CompanyService {
	@Autowired
	CompanyMapper companyMapper;

	@Autowired
	LogService logService;

	@Autowired
	PictureMapper pictureMapper;

	@Resource
	AliOSSProperties aliOSSProperties;

	@Override
	public Pagination<Company> getCompany(Integer pageNo, Integer pageSize) {
		Pagination<Company> page = new Pagination<>(pageNo, pageSize);
		CompanyExample cx = new CompanyExample();
		cx.createCriteria();
		cx.setOrderByClause("id desc");
		pageNo = (pageNo - 1) * pageSize;
		List<Company> list = companyMapper.getCompany(pageNo, pageSize);
		int count = (int) companyMapper.countByExample(cx);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	@Transactional
	public int deleteByPrimaryKey(Long userId, Long id) {
		Company company = companyMapper.selectByPrimaryKey(id);
		String companyName = company.getCompanyName();
		String msg = "删除企业名为" + companyName;
		logService.addLog(userId, msg, 2);
		if (companyMapper.deleteByPrimaryKey(id) == 0) {
			throw new HelperException("出错了");
		}
		Long picId = company.getPictureId();
		Picture picture = pictureMapper.selectByPrimaryKey(picId);
		if (picture != null) {
			AliOSSUtils.removeFromOSS(aliOSSProperties, picture.getOssId());
		}
		return companyMapper.deleteByPrimaryKey(id);
	}

	@Override
	@Transactional
	public boolean updateCompany(Long id, String companyName, Integer companyNumber, String companyPerson,
			String companyMobile, Long pictureId, List<MultipartFile> files) {
		Company c = new Company();
		c.setId(id);
		c.setCompanyName(companyName);
		c.setCompanyNumber(companyNumber);
		c.setCompanyPerson(companyPerson);
		c.setCompanyMobile(companyMobile);
		
		if (files.size() != 0) {
			Picture picture = pictureMapper.selectByPrimaryKey(pictureId);
			String ossId = picture.getOssId();
			AliOSSUtils.removeFromOSS(aliOSSProperties, ossId);
			MultipartFile file = files.get(0);
			String key = null;
			try {
				key = AliOSSUtils.uploadToOSS(aliOSSProperties, file.getInputStream());
			} catch (IOException e) {
				e.printStackTrace();
			}
			picture.setOssId(ossId);
			picture.setPictureUrl(aliOSSProperties.getBucket() + "." + aliOSSProperties.getGateway() + "/" + key);
			pictureMapper.updateByPrimaryKeySelective(picture);
			c.setPictureId(picture.getId());
		}
		int num = companyMapper.updateByPrimaryKeySelective(c);
		if (num == 0) {
			throw new HelperException(CodeMsg.COMPANY_ERROR.getCode(), CodeMsg.COMPANY_ERROR.getMsg());
		}
		return num == 1 ? true : false;
	}

	@Override
	@Transactional
	public boolean insertCompany(Long userId, String companyName, Integer companyNumber, String companyPerson,
			String companyMobile, MultipartFile file) {
		String code = getCode();
		Company c = new Company();
		if (companyMapper.getCode(code).size() > 0) {
			insertCompany(userId, companyName, companyNumber, companyPerson, companyMobile, file);
		} else {
			c.setCompanyCode(code);
		}
		c.setCompanyName(companyName);
		c.setCompanyNumber(companyNumber);
		c.setCompanyMobile(companyMobile);
		c.setCompanyPerson(companyPerson);
		c.setCreateDate(ToolUtil.getTime(0));

		String key = "";
		try {
			key = AliOSSUtils.uploadToOSS(aliOSSProperties, file.getInputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
		Picture picture = new Picture();
		String type = file.getContentType();
		String picUrl = aliOSSProperties.getBucket() + "." + aliOSSProperties.getGateway() + "/" + key;
		if (type.indexOf("image") != -1) {
			PictureUtils picUtils = new PictureUtils();
			picture = picUtils.getPictureModel(file);
			picture.setOssId(key);
			picture.setPictureType(6);
			picture.setPictureUrl(picUrl);
			pictureMapper.insertSelective(picture);
		} else {
			throw new HelperException(CodeMsg.FILE_NOT_PICTURE.getCode(), CodeMsg.FILE_NOT_PICTURE.getMsg());
		}

		c.setPictureId(picture.getId());

		int num = companyMapper.insertSelective(c);
		if (num == 0) {
			throw new HelperException(CodeMsg.COMPANY_ERROR.getCode(), CodeMsg.COMPANY_ERROR.getMsg());
		}
		String msg = "新增企业名为" + companyName;
		logService.addLog(userId, msg, 2);

		return num == 1 ? true : false;
	}

	public String getCode() {
		String code = String.valueOf((int) (Math.random() * 9000 + 1000));
		return code;
	}

	@Override
	public boolean updateCompanyFrozen(Long id) {
		Company c = new Company();
		c.setId(id);
		c.setCompanyState(1);
		int num = companyMapper.updateByPrimaryKeySelective(c);
		return num == 1 ? true : false;
	}

	@Override
	public void relieveCompany(Long id) {
		Company c = new Company();
		c.setId(id);
		c.setCompanyState(2);
		companyMapper.updateByPrimaryKeySelective(c);
	}

	/*
	 * 根据企业代码和名称查询某个企业
	 */
	@Override
	public Company selectCompanyByOne(String companyCode, String companyName) {
		if (companyMapper.selectCompanyByOne(companyCode, companyName) == null) {
			throw new HelperException(CodeMsg.NO_COMPANY_ERROR.getCode(), CodeMsg.NO_COMPANY_ERROR.getMsg());
		}
		return companyMapper.selectCompanyByOne(companyCode, companyName);
	}

	/*
	 * 根据时间或者状态查询企业 分页
	 */
	@Override
	public List<Company> selectCompanyByMany(Integer pageNo, Integer pageSize, String startTime, String endTime,
			Integer companyState) {
		pageNo = (pageNo - 1) * pageSize;
		List<Company> companies = companyMapper.selectCompanyByMany(pageNo, pageSize, startTime, endTime, companyState);
		return companies;
	}

	@Override
	public JSONObject getCompanyById(Long id) {

		Company company = companyMapper.selectByPrimaryKey(id);
		Long pictureId = company.getPictureId();
		Picture picture = pictureMapper.selectByPrimaryKey(pictureId);
		String url = picture.getPictureUrl();
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("company", company);
		jsonObject.put("url", url);
		return jsonObject;
	}

}
