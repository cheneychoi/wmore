package com.cheney.modules.maye.mid.service;

import javax.servlet.http.HttpServletRequest;

import com.cheney.modules.maye.db.model.Admin;
import com.cheney.modules.maye.db.model.out.AdminOut;
import com.cheney.share.db.model.Pagination;

public interface AdminService {
	/** 
	 * 获取验证码
	* @param request void
	* @author WangBo
	* @date 2018年7月18日下午7:15:57
	*/ 
	public void getCode(HttpServletRequest request);
	
	/** 登录
	* @param admin
	* @param codes
	* @param request void
	* @author WangBo
	* @date 2018年7月18日下午7:15:52
	* ,String codes,HttpServletRequest request
	*/ 
	Long login(String username,String password);
	
	/**
	 * 后台管理员添加
	 */
	boolean  insertAdmin(String nickname,String name,String username,String password,Long roleId);
	/**
	 * 分页查询所有管理员
	 */
	Pagination<AdminOut> getAllAdminByPage(Integer pageNo, Integer pageSize);
	/**
	 * 根据id看管理员
	 */
	Admin getAdminById(Long id);
	/**
	 * 根据id编辑管理员
	 */
	boolean updateAdmin(Long id,String nickname,String name,String username,String password,Long roleId);
	/**
	 * 根据id删除管理员
	 */
	boolean deleteAdmin(Long id);
	/**
	 * 批量删除管理员
	 */
	boolean deleteAdminBatch(String ids);
	  /**
     * 修改密码
     */
    boolean updatePsd(Long id,String password);
    
    /** 
     * 修改权限
    * @param adminId
    * @param roleId void
    * @author WangBo
    * @date 2018年8月2日下午7:19:31
    */ 
    void  updateRole(Long adminId,Long roleId);
    /**
     * 查询所有角色
     */
   /* List<AdminOut> getAllAdmin(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize);*/
    
}
