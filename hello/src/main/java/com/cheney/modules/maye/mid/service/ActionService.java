package com.cheney.modules.maye.mid.service;

import java.util.List;

import com.cheney.modules.maye.db.model.ActionWithBLOBs;
import com.cheney.modules.maye.db.model.out.ActionOut;

/**行为模块 
 * @ClassName: ActionService 
 * @Description: TODO
 * @author: WangBo
 * @date: 2018年7月6日 上午11:39:10  
 */
public interface ActionService {
	/**
	 * 点击可查询当前会员的所有过往行为及积分等信息
	 */
	List<ActionOut> getActionById(Long UserId);
	
	/**
	 * 根据时间和名字查询会员的所有过往行为及积分等信息
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @param startTime
	 * @param endTime
	 * @param userName
	 * @return List<ActionOut>
	 * @author WangBo
	 * @date 2018年7月18日上午11:48:19
	 */
	List<ActionOut> getActionByConditions(Integer pageNo, Integer pageSize, String startTime, String endTime,
			String userName);

	/**
	 * 
	 * @return List<ActionOut>
	 * @author WangBo
	 * @date 2018年7月18日上午11:48:48
	 */
	List<ActionOut> getAllActions();
	
	/** 
	 * 增加积分增加行为
	*  void
	* @author WangBo
	* @date 2018年7月19日下午2:25:30
	*/ 
	void addAction(ActionWithBLOBs actionWithBLOBs);

}
