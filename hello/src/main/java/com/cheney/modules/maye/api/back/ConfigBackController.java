package com.cheney.modules.maye.api.back;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.db.model.Config;
import com.cheney.modules.maye.db.model.out.OSSConfigOut;
import com.cheney.modules.maye.db.model.out.SMSConfigOut;
import com.cheney.modules.maye.mid.service.ConfigService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value="配置")
@Controller
@RequestMapping(value="/wmore_back")
public class ConfigBackController extends BaseController{
	
	@Autowired
	ConfigService configService;
	
	
	@ApiOperation(value = "更改短信配置信息", notes = "用于后台短信配置")
	@RequestMapping(value = "/config/updateSMSConfig", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateSMSConfig(@ApiParam(value="id") @RequestParam(value="id")Long id,@ApiParam(value="type") @RequestParam(value="type",required=false)Integer type,
			@ApiParam(value="appid") @RequestParam(value="appid",required=false)String appid,@ApiParam(value="appkey") @RequestParam(value="appkey",required=false)String appkey,
			@ApiParam(value="gateway") @RequestParam(value="gateway",required=false)String gateway,@ApiParam(value="bucket") @RequestParam(value="bucket",required=false)String bucket){
		System.out.println(id);
		configService.updateSMSConfig(id, type, appid, appkey, gateway, bucket);
		return success();
	}
	
	
	@ApiOperation(value = "更改OSS服务器配置信息", notes = "用于后台OSS服务器配置")
	@RequestMapping(value = "/config/updateOSSConfig", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateOSSConfig(@ApiParam("id") @RequestParam(value="id")Long id, @ApiParam("type") @RequestParam(value="type",required=false)Integer type,
			@ApiParam("appkey") @RequestParam(value="appkey",required=false)String appid,@ApiParam("appkey") @RequestParam(value="appkey",required=false)String appkey,
			@ApiParam("product") @RequestParam(value="product",required=false)String sign,@ApiParam("product") @RequestParam(value="product",required=false)String product,
			@ApiParam("phoneRegisterTmid") @RequestParam(value="phoneRegisterTmid",required=false)String phoneRegisterTmid,@ApiParam("phoneBindTmid") @RequestParam(value="phoneBindTmid",required=false)String phoneBindTmid){
		configService.updateOSSConfig(id, type, appid, appkey, sign, product, phoneRegisterTmid, phoneBindTmid);
		return success();
	}
	
	
	@ApiOperation(value = "查看配置信息", notes = "用于查看配置信息")
	@RequestMapping(value = "/config/getConfig", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getConfig(@ApiParam(value = "配置ID(1-短信 2-OSS)") @RequestParam("id") Long id){
		Config config = configService.getConfig(id);
		if (id == 1) {
			return success(SMSConfigOut.from(config));
		}else {
			return success(OSSConfigOut.from(config));
		}
		
	}
	
}
