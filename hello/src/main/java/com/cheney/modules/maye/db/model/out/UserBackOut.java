package com.cheney.modules.maye.db.model.out;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.User;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "用户-出")
public class UserBackOut {

	@ApiModelProperty(value = "id")
	private Long id;

	@ApiModelProperty(value = "用户姓名")
	private String name;
	@ApiModelProperty(value = "用户性别")
	private Integer sex;
	@ApiModelProperty(value = "行业")
	private String industry;
	@ApiModelProperty(value = "城市")
	private String city;
	@ApiModelProperty(value = "积分")
	private Integer points;
	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "公司名称")
	private String companyName;

	@ApiModelProperty(value = "是否是会员 1-是 2-不是")
	private Integer isMember;

	@ApiModelProperty(value = "职位 1-高管 2-普通员工")
	private Integer position;

	@ApiModelProperty(value = "状态 1：冻结，2：未冻结")
	private Integer state;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;

	public static UserBackOut from(User user) {
		UserBackOut uo = new UserBackOut();
		BeanUtils.copyProperties(user, uo);
		return uo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public Integer getIsMember() {
		return isMember;
	}

	public void setIsMember(Integer isMember) {
		this.isMember = isMember;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}
	
}