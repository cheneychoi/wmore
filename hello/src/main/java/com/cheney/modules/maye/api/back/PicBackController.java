package com.cheney.modules.maye.api.back;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.cheney.modules.maye.mid.service.PictureService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "图片-后台")
@Controller
@RequestMapping(value = "/wmore_back")
public class PicBackController extends BaseController {
	@Autowired
	PictureService pictureService;

	@ApiOperation(value = "上架或者下架Banner", notes = "")
	@RequestMapping(value = "/picture/updateBanner", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateBanner(@ApiParam(value = "图片ID") @RequestParam("pictureId") Long picId,
			@ApiParam(value = "是否是banner") @RequestParam("isBanner") Integer isBanner) {
		pictureService.isBanner(picId, isBanner);
		return success();
	}

	@ApiOperation(value = "获取所有banner图片", notes = "")
	@RequestMapping(value = "/picture/getBanner", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getBanner(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		return success(pictureService.getBanner(pageNo, pageSize));
	}

	@ApiOperation(value = "上传Banner图", notes = "上传Banner图")
	@RequestMapping(value = "/picture/uploadBanner", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> uploadBanner(HttpServletRequest request) {
		String url = request.getParameter("contentUrl");
		String type = request.getParameter("type");
		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		List<MultipartFile> files = multipartRequest.getFiles("file");
		MultipartFile file = files.get(0);
		pictureService.uploadBanner(file, url, Integer.valueOf(type));
		return success();
	}
	@ApiOperation(value = "删除图片", notes = "删除图片")
	@RequestMapping(value = "/picture/delPic", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> delPic(@ApiParam(value = "id") @RequestParam("id")Long id) {
		pictureService.delPic(id);
		return success();
	}
	
	@ApiOperation(value = "根据ID查询Banner图片", notes = "编辑Banner图片")
	@RequestMapping(value = "/picture/getBannerById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getBannerById(@ApiParam(value = "id") @RequestParam("id")Long id) {
		return success(pictureService.getBannerById(id));
	}
	
	@ApiOperation(value = "编辑Banner图片", notes = "编辑Banner图片")
	@RequestMapping(value = "/picture/editBanner", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> editBanner(@ApiParam(value = "id") @RequestParam("id")Long id,@ApiParam(value = "跳转链接") @RequestParam("contentUrl")String contentUrl,
			@ApiParam(value = "Banner类型") @RequestParam("type")Integer type) {
		pictureService.editBanner(id,contentUrl,type);
		return success();
	}

}
