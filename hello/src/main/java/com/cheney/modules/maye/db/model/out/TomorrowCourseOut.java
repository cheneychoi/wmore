package com.cheney.modules.maye.db.model.out;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "明天的课程信息-出")
public class TomorrowCourseOut {
	@ApiModelProperty(value = "用户姓名")
	private String name;
	@ApiModelProperty(value = "用户手机号")
	private String mobile;
	@ApiModelProperty(value = "课程名称")
	private String courseName;
	@ApiModelProperty(value = "课程地点")
	private String coursePlace;
	@ApiModelProperty(value = "上课时间")
	private Date courseDate;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCoursePlace() {
		return coursePlace;
	}

	public void setCoursePlace(String coursePlace) {
		this.coursePlace = coursePlace;
	}

	public Date getCourseDate() {
		return courseDate;
	}

	public void setCourseDate(Date courseDate) {
		this.courseDate = courseDate;
	}

}
