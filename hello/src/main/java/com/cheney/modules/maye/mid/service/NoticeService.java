package com.cheney.modules.maye.mid.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.cheney.modules.maye.db.model.Notice;
import com.cheney.modules.maye.db.model.out.NoticeOut;

/** 
 * 通知模块
 * @ClassName: NoticeService 
 * @Description: TODO
 * @author: WangBo
 * @date: 2018年7月13日 下午6:20:43  
 */
public interface NoticeService {
	
	/** 
	 * 获得每日通知
	* @param notice
	* @return List<NoticeOut>
	* @author WangBo
	* @date 2018年7月15日下午5:12:19
	*/ 
	List<NoticeOut> getNoticeByDay(Long id);
	
	/** 
	 * 添加登陆通知
	*  void
	* @author WangBo
	* @date 2018年7月15日下午4:25:20
	*/ 
	void addLoginNotice(HttpServletRequest request);
	
	
	/** 
	 * 添加通知
	*  void
	* @author WangBo
	* @date 2018年7月15日下午5:22:04
	*/ 
	void addNotice(Notice notice);
	
	/** 
	 * 获取未读的通知
	* @return List<Notice>
	* @author WangBo
	* @date 2018年7月15日下午5:38:59
	*/ 
	List<Notice> getUnreadNotice(Long userId);
	
	/** 
	 * 获取已读的通知
	* @return List<Notice>
	* @author WangBo
	* @date 2018年7月15日下午5:39:12
	*/ 
	List<Notice> getReadNotice(Long userId);
	
	/** 
	 * 将未读通知变成已读的通知
	*  void
	* @author WangBo
	* @date 2018年7月15日下午5:40:00
	*/ 
	Notice toReadNotice(Long noticeId);
}
