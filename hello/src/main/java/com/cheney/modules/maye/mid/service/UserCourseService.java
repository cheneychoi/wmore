package com.cheney.modules.maye.mid.service;

import java.util.List;

import com.cheney.modules.maye.db.model.Notice;
import com.cheney.modules.maye.db.model.UserCourseWithBLOBs;
import com.cheney.modules.maye.db.model.in.EvaluateIn;
import com.cheney.modules.maye.db.model.out.CourseEvaluateOut;
import com.cheney.modules.maye.db.model.out.EvaluateOut;

public interface UserCourseService {
	/**
	 * 选择课程，确认预约，产生预约订单
	 */
	Notice insertUserCourse(UserCourseWithBLOBs ucb);

	/** 
	 * 签到
	* @return boolean
	* @author WangBo
	* @date 2018年7月13日下午2:09:13
	*/ 
	void sign(Long courseId,String openId);
	
	/** 
	 * 课程评分
	* @param courseId
	* @param userId
	* @return Notice
	* @author WangBo
	* @date 2018年7月15日下午7:08:05
	*/ 
	Notice setScore(EvaluateIn in);
	
	/**
	 * 获取未评价的课程
	 * 
	 * @param userId
	 * @return UserCourse
	 * @author WangBo
	 * @date 2018年7月19日上午10:08:11
	 */
	List<EvaluateOut> getIsEvaluateCourse(Long userId);
	
	
	/** 
	 * 用于后台查询课程评价
	* @return List<CourseEvaluateOut>
	* @author WangBo
	* @date 2018年7月26日下午5:11:24
	*/ 
	List<CourseEvaluateOut> getEvaluate(Long courseId);
}
