package com.cheney.modules.maye.mid.service;

import com.cheney.modules.maye.db.model.Authority;
import com.cheney.share.db.model.Pagination;

public interface AuthorityService {
	/**
	 * 添加权限
	 */
	boolean addAuthority(String url,String menuClass,String menuName,String parentMenucode,Integer menuType);
	/**
	 * 根据ID查询权限
	 */
	Authority getAuthorityById(Long id);
	/**
	 * 根据ID编辑权限
	 */
	boolean updateAuthority(Long id,String url,String menuClass,String menuName,String parentMenucode,Integer menuType);
	/**
	 * 查询所有权限 分页
	 */
	Pagination<Authority> getAuthorityByPage(Integer pageNo, Integer pageSize);
	/**
	 * 删除权限
	 */
	boolean deleteAuthority(Long id);
	/**
	 * 批量删除权限
	 */
	boolean deleteAuthorityBatch(String ids);
}
