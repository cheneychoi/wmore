package com.cheney.modules.maye.db.model.out;

import java.util.Date;

public class CourseDetailOut {
	private String courseName;

	private String courseNameen;

	private String coursePlace;

	private Date courseDate;
	
	private String introduction;
	
	private String crowd;
	
	private String trainingPreparation;
	
	private String otherBusiness;
	
	private String trainingEffect;
	
	private Integer state;
	
	private Integer courseState;
	
	private String pictureUrl;

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseNameen() {
		return courseNameen;
	}

	public void setCourseNameen(String courseNameen) {
		this.courseNameen = courseNameen;
	}

	public String getCoursePlace() {
		return coursePlace;
	}

	public void setCoursePlace(String coursePlace) {
		this.coursePlace = coursePlace;
	}

	public Date getCourseDate() {
		return courseDate;
	}

	public void setCourseDate(Date courseDate) {
		this.courseDate = courseDate;
	}

	public String getTrainingEffect() {
		return trainingEffect;
	}

	public void setTrainingEffect(String trainingEffect) {
		this.trainingEffect = trainingEffect;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}

	public String getIntroduction() {
		return introduction;
	}

	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}

	public String getCrowd() {
		return crowd;
	}

	public void setCrowd(String crowd) {
		this.crowd = crowd;
	}

	public String getTrainingPreparation() {
		return trainingPreparation;
	}

	public void setTrainingPreparation(String trainingPreparation) {
		this.trainingPreparation = trainingPreparation;
	}

	public String getOtherBusiness() {
		return otherBusiness;
	}

	public void setOtherBusiness(String otherBusiness) {
		this.otherBusiness = otherBusiness;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public Integer getCourseState() {
		return courseState;
	}

	public void setCourseState(Integer courseState) {
		this.courseState = courseState;
	}

}
