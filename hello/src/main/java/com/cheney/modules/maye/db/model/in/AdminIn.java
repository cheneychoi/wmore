package com.cheney.modules.maye.db.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Admin;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="管理员登陆-输入")
public class AdminIn {
	@ApiModelProperty(value="用户名")
	 private String username;
	@ApiModelProperty(value="密码")
	 private String password;
	@ApiModelProperty(value="登陆时间")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	 private Date loginDate;
	@ApiModelProperty(value="验证码")
	private String codes;
	public Admin toAdminIn() {
		Admin a = new Admin();
		BeanUtils.copyProperties(this, a);
		return a;
	}
	
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getLoginDate() {
		return loginDate;
	}
	public void setLoginDate(Date loginDate) {
		this.loginDate = loginDate;
	}

	public String getCodes() {
		return codes;
	}
	public void setCodes(String codes) {
		this.codes = codes;
	}
}