package com.cheney.modules.maye.api.front;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.db.model.out.ActionOut;
import com.cheney.modules.maye.mid.service.ActionService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/wmore")
public class ActionController extends BaseController {
	
	Logger logger = LoggerFactory.getLogger("action");
	
	@Autowired
	ActionService actionService;

	@ApiOperation(value = "查询当前会员的所有过往行为及积分等信息 ", notes = "返回数据", response = ActionOut.class)
	@RequestMapping(value = "/action/getActionById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getActionById(@ApiParam(value = "用户id") @RequestParam("id") Long userId) {
		
		logger.info("----------------进入查询当前会员的所有过往行为及积分等信息------------------------");
		logger.info("userId-----------------------------"+userId);
		List<ActionOut> actionOuts =  actionService.getActionById(userId);
		logger.info("actionOuts-----------------------------"+actionOuts);
		return success(actionOuts);
	}
}
