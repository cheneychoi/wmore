package com.cheney.modules.maye.api.back;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.mid.service.UserCourseService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "用户课程-后台")
@Controller
@RequestMapping("/wmore_back")
public class UserCourseBackController extends BaseController{
	
	@Autowired
	UserCourseService userCourseService;
	
	@ApiOperation(value = "根据课程查看评价信息", notes = "根据课程查看评价信息")
	@RequestMapping(value = "/userCourse/getEvaluate", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getEvaluate(@ApiParam(value = "课程id") @RequestParam("courseId") Long courseId) {
		return success(userCourseService.getEvaluate(courseId));
	}
}
