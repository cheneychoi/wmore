package com.cheney.modules.maye.mid.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cheney.modules.maye.db.mapper.AdminRoleMapper;
import com.cheney.modules.maye.db.mapper.AuthorityMapper;
import com.cheney.modules.maye.db.mapper.RoleAuthorityMapper;
import com.cheney.modules.maye.db.mapper.RoleMapper;
import com.cheney.modules.maye.db.model.AdminRole;
import com.cheney.modules.maye.db.model.AdminRoleExample;
import com.cheney.modules.maye.db.model.Authority;
import com.cheney.modules.maye.db.model.Role;
import com.cheney.modules.maye.db.model.RoleAuthority;
import com.cheney.modules.maye.db.model.RoleAuthorityExample;
import com.cheney.modules.maye.mid.service.DistributionService;
import com.cheney.share.mid.utils.ToolUtil;

@Service
public class DistributionServiceImpl implements DistributionService {

	@Resource
	AdminRoleMapper adminRoleMapper;

	@Resource
	RoleMapper roleMapper;

	@Resource
	RoleAuthorityMapper roleAuthorityMapper;

	@Resource
	AuthorityMapper authorityMapper;

	@Override
	public void addRole(Long adminId, Long roleId) {
		AdminRoleExample are = new AdminRoleExample();
		are.or().andRoleIdEqualTo(roleId).andAdminIdEqualTo(adminId);

		AdminRole adminRole = new AdminRole();
		adminRole.setAdminId(adminId);
		adminRole.setRoleId(roleId);

		List<AdminRole> adminRoles = adminRoleMapper.selectByExample(are);
		if (adminRoles.size() == 0) {
			adminRoleMapper.insert(adminRole);
		} else {
			adminRoleMapper.updateByExample(adminRole, are);
		}
	}

	@Override
	public Role getRoleByAdminId(Long adminId) {
		return roleMapper.getRoleByAdminId(adminId);
	}

	@Override
	public void addAuthority(Long roleId, String authIds) {

		List<Long> ids = ToolUtil.toList(authIds);

		for (Long authId : ids) {
			RoleAuthorityExample rae = new RoleAuthorityExample();
			rae.or().andRoleIdEqualTo(roleId).andMenuIdEqualTo(authId);

			RoleAuthority ra = new RoleAuthority();
			ra.setMenuId(authId);
			ra.setRoleId(roleId);

			List<RoleAuthority> roleAuthorities = roleAuthorityMapper.selectByExample(rae);
			if (roleAuthorities.size() == 0) {
				roleAuthorityMapper.insertSelective(ra);
			} else {
				roleAuthorityMapper.updateByExampleSelective(ra, rae);
			}

		}

	}

	@Override
	public List<Authority> getAuthorityByRoleId(Long roleId) {

		return authorityMapper.getAuthorityByRoleId(roleId);
	}

	@Override
	public boolean deleteUserRole(Long adminId, Long roleId) {
		int num=adminRoleMapper.deleteUserRole(adminId, roleId);
		return num>=1 ? true : false;
	}

	@Override
	public boolean deleteRoleAut(Long roleId, Long menuId) {
		int num=roleAuthorityMapper.deleteRoleAut(roleId, menuId);
		return num >=1 ? true : false;
	}

}
