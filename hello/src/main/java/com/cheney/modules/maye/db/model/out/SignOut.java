package com.cheney.modules.maye.db.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value = "签到课程信息-出")
public class SignOut {
	@ApiModelProperty(value = "课程名称")
	private String courseName;
	
	@ApiModelProperty(value = "课程名称英语")
	private String courseNameEN;
	
	@ApiModelProperty(value = "课程二维码")
	private String codeUrl;
	
	@ApiModelProperty(value = "课程所属企业名称")
	private String companyName;
	
	@ApiModelProperty(value = "企业背景图")
	private String pictureUrl;
	
	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseNameEN() {
		return courseNameEN;
	}

	public void setCourseNameEN(String courseNameEN) {
		this.courseNameEN = courseNameEN;
	}

	public String getCodeUrl() {
		return codeUrl;
	}

	public void setCodeUrl(String codeUrl) {
		this.codeUrl = codeUrl;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	
	
}
