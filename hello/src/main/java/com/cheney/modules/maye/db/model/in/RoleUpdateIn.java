package com.cheney.modules.maye.db.model.in;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Role;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="根据id角色名修改-输入")
public class RoleUpdateIn {
	@ApiModelProperty(value="id")
	private Long id;
	@ApiModelProperty(value="描述")
	private String description;
	@ApiModelProperty(value="角色名")
	private String name;
	public Role toRoleUpdateIn() {
		Role r = new Role();
		BeanUtils.copyProperties(this, r);
		return r;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
