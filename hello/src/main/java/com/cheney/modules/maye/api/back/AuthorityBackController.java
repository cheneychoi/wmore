package com.cheney.modules.maye.api.back;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.db.model.Authority;
import com.cheney.modules.maye.mid.service.AuthorityService;
import com.cheney.share.api.controller.BaseController;
import com.cheney.share.db.model.Pagination;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value = "/wmore_back")
public class AuthorityBackController extends BaseController{
	@Autowired
	AuthorityService authorityService;
	
	@ApiOperation(value = "添加权限", notes = "")
	@RequestMapping(value = "/admin/addAuthority", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> addAuthority(@ApiParam(value="路径") @RequestParam("url") String url,
			@ApiParam(value="菜单样式") @RequestParam("menuClass") String menuClass,@ApiParam(value="菜单名称") @RequestParam("menuName") String menuName,
			@ApiParam(value="上级菜单编码样式") @RequestParam("parentMenucode") String parentMenucode,@ApiParam(value="菜单类型（1-左导航栏 2-按钮）") @RequestParam("menuType") Integer menuType){
		return success(authorityService.addAuthority(url, menuClass, menuName, parentMenucode, menuType));
	}
	@ApiOperation(value = "修改权限", notes = "")
	@RequestMapping(value = "/admin/updateAuthority", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateAuthority(@ApiParam(value="id") @RequestParam(value="id")Long id,@ApiParam(value="路径") @RequestParam(value="url",required=false) String url,
			@ApiParam(value="菜单样式") @RequestParam(value="menuClass",required=false) String menuClass,@ApiParam(value="菜单名称") @RequestParam(value="menuName",required=false) String menuName,
			@ApiParam(value="上级菜单编码样式") @RequestParam(value="parentMenucode",required=false) String parentMenucode,@ApiParam(value="菜单类型（1-左导航栏 2-按钮）") @RequestParam(value="menuType",required=false) Integer menuType){
		return success(authorityService.updateAuthority(id, url, menuClass, menuName, parentMenucode, menuType));
	}
	@ApiOperation(value = "点击id查看权限", notes = "")
	@RequestMapping(value = "/admin/getAuthorityById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAuthorityById(@ApiParam(value="权限id") @RequestParam("id") Long id){
		return success(authorityService.getAuthorityById(id));
	}
	@ApiOperation(value = "点击id删除权限", notes = "")
	@RequestMapping(value = "/admin/deleteAuthority/{id}", method = { RequestMethod.DELETE })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> deleteAuthority(@ApiParam(value="权限id") @PathVariable("id") Long id){
		return success(authorityService.deleteAuthority(id));
	}
	@ApiOperation(value = "分页查询所有权限", notes = "")
	@RequestMapping(value = "/admin/getAuthorityByPage", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAuthorityByPage(@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize){
		Pagination<Authority> authority = authorityService.getAuthorityByPage(pageNo, pageSize);
		return success(authority);
	}
	@ApiOperation(value = "批量删除权限", notes = "")
	@RequestMapping(value = "/admin/deleteAuthorityBatch/{ids}", method = { RequestMethod.DELETE })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> deleteAuthorityBatch(@ApiParam(value="权限id") @PathVariable("ids") String ids){
		return success(authorityService.deleteAuthorityBatch(ids));
	}
}
