package com.cheney.modules.maye.mid.service;

import java.util.List;

import com.cheney.modules.maye.db.model.Point;
import com.cheney.modules.maye.db.model.PointWithBLOBs;
import com.cheney.share.db.model.Pagination;

/** 
 * 积分模块
 * @ClassName: PointService 
 * @Description: TODO
 * @author: WangBo
 * @date: 2018年7月6日 上午11:23:59  
 */
public interface PointService {
	
	/** 获取所有的积分规则
	* @return List<Point>
	* @author WangBo
	* @date 2018年7月18日下午2:12:39
	*/ 
	Pagination<PointWithBLOBs> getAllPoints(Integer pageNo,Integer pageSize);
	
	/** 
	 * 根据ID获取积分规则
	* @return Point
	* @author WangBo
	* @date 2018年7月18日下午2:26:12
	*/ 
	PointWithBLOBs getPointById(Long pointId);
	
	/** 
	 * 根据条件查询积分规则  分页
	* @return List<Point>
	* @author WangBo
	* @date 2018年7月18日下午2:26:22
	*/ 
	List<Point> getPointByCondition(Integer pageNo,Integer pageSize,String startTime,String endTime);
	
	
	/** 增加积分
	*  void
	* @author WangBo
	* @date 2018年7月19日下午1:49:57
	*/ 
	void plusPoint(Integer type,Long userId,Integer times);
	
	/** 
	 *是否启用积分规则
	* @param id
	* @param isStart void
	* @author WangBo
	* @date 2018年7月19日下午2:01:45
	*/ 
	void isStart(Long id,Integer isStart);
	
	/** 
	 * 添加积分
	* @param name
	* @param introduce
	* @param num
	* @param type void
	* @author WangBo
	* @date 2018年8月2日下午1:34:49
	*/ 
	void addPointRule(String name,String introduce,Integer num,Integer type);
	
	
	/** 
	 * 修改积分
	* @param name
	* @param introduce
	* @param num
	* @param type void
	* @author WangBo
	* @date 2018年8月2日下午1:34:49
	*/ 
	void updatePointRule(Long id, String name,String introduce,Integer num,Integer type,String rule);
	
}
