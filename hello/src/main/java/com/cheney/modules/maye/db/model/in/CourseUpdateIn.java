package com.cheney.modules.maye.db.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.CourseWithBLOBs;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="课程的修改-入")
public class CourseUpdateIn {
	@ApiModelProperty(value = "课程id")
	private Long id;
	@ApiModelProperty(value = "课程名")
	private String courseName;
	@ApiModelProperty(value = "课程名称英文")
	private String courseNameen;
	@ApiModelProperty(value = "课程地点")
	 private String coursePlace;
	@ApiModelProperty(value = "课程时间")
	 @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	 private Date courseDate;
	@ApiModelProperty(value = "课程二维码地址")
	 private String codeUrl;
	@ApiModelProperty(value = "课程人数数量")
	 private Integer courseNum;
	@ApiModelProperty(value = "训练效果")
	 private String trainingEffect;
	@ApiModelProperty(value = "课程简介")
	 private String introduction;
	@ApiModelProperty(value = "适合人群")
	 private String crowd;
	 @ApiModelProperty(value = "训练准备")
	 private String trainingPreparation;
	 @ApiModelProperty(value = "其他事项")
	 private String otherBusiness;
	 @ApiModelProperty(value = "课程的权限 1-高管 2-员工 3-全部")
	 private Integer courseAuthority;
	 @ApiModelProperty(value = "预约状态： 1 预约满 2 预约未满")
	 private Integer score;
	 @ApiModelProperty(value = "总评分")
	 private Integer type;
	 @ApiModelProperty(value = "上课老师")
	 private String teacher;
	 @ApiModelProperty(value = "企业ID")
	 private Long companyId;
	 @ApiModelProperty(value = "图片的id")
	 private Long pictureId;
	 @ApiModelProperty(value = "修改时间")
	 @JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	 private Date updateDate;
	 public CourseWithBLOBs toCourseUpdateIn() {
		 CourseWithBLOBs c = new CourseWithBLOBs();
		 BeanUtils.copyProperties(this, c);
		return c;
	 }
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getCourseName() {
		return courseName;
	}
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}
	public String getCourseNameen() {
		return courseNameen;
	}
	public void setCourseNameen(String courseNameen) {
		this.courseNameen = courseNameen;
	}
	public String getCoursePlace() {
		return coursePlace;
	}
	public void setCoursePlace(String coursePlace) {
		this.coursePlace = coursePlace;
	}
	public Date getCourseDate() {
		return courseDate;
	}
	public void setCourseDate(Date courseDate) {
		this.courseDate = courseDate;
	}
	public String getCodeUrl() {
		return codeUrl;
	}
	public void setCodeUrl(String codeUrl) {
		this.codeUrl = codeUrl;
	}
	public Integer getCourseNum() {
		return courseNum;
	}
	public void setCourseNum(Integer courseNum) {
		this.courseNum = courseNum;
	}
	public String getTrainingEffect() {
		return trainingEffect;
	}
	public void setTrainingEffect(String trainingEffect) {
		this.trainingEffect = trainingEffect;
	}
	public String getIntroduction() {
		return introduction;
	}
	public void setIntroduction(String introduction) {
		this.introduction = introduction;
	}
	public String getCrowd() {
		return crowd;
	}
	public void setCrowd(String crowd) {
		this.crowd = crowd;
	}
	public String getTrainingPreparation() {
		return trainingPreparation;
	}
	public void setTrainingPreparation(String trainingPreparation) {
		this.trainingPreparation = trainingPreparation;
	}
	public String getOtherBusiness() {
		return otherBusiness;
	}
	public void setOtherBusiness(String otherBusiness) {
		this.otherBusiness = otherBusiness;
	}
	public Integer getCourseAuthority() {
		return courseAuthority;
	}
	public void setCourseAuthority(Integer courseAuthority) {
		this.courseAuthority = courseAuthority;
	}
	public Integer getScore() {
		return score;
	}
	public void setScore(Integer score) {
		this.score = score;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getTeacher() {
		return teacher;
	}
	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}
	public Long getCompanyId() {
		return companyId;
	}
	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}
	public Long getPictureId() {
		return pictureId;
	}
	public void setPictureId(Long pictureId) {
		this.pictureId = pictureId;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
}
