package com.cheney.modules.maye.mid.impl;

import java.util.List;

import javax.annotation.Resource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheney.modules.maye.db.mapper.PointMapper;
import com.cheney.modules.maye.db.mapper.UserMapper;
import com.cheney.modules.maye.db.model.ActionWithBLOBs;
import com.cheney.modules.maye.db.model.Point;
import com.cheney.modules.maye.db.model.PointExample;
import com.cheney.modules.maye.db.model.PointWithBLOBs;
import com.cheney.modules.maye.db.model.User;
import com.cheney.modules.maye.mid.service.ActionService;
import com.cheney.modules.maye.mid.service.PointService;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.utils.ToolUtil;

@Service
public class PointServiceImpl implements PointService {
	
	Logger logger = LoggerFactory.getLogger("point");

	@Resource
	PointMapper pointMapper;

	@Resource
	UserMapper userMapper;

	@Resource
	ActionService actionService;

	@Override
	public Pagination<PointWithBLOBs> getAllPoints(Integer pageNo, Integer pageSize) {
		Pagination<PointWithBLOBs> page = new Pagination<>(pageNo, pageSize);
		PointExample pe = new PointExample();
//		PointWithBLOBs pw = new PointWithBLOBs();
		pe.createCriteria();
		List<PointWithBLOBs> list = pointMapper.selectByExampleWithBLOBs(pe);
		int count = (int) pointMapper.countByExample(pe);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public PointWithBLOBs getPointById(Long pointId) {
		return pointMapper.selectByPrimaryKey(pointId);
	}

	@Override
	public List<Point> getPointByCondition(Integer pageNo, Integer pageSize, String startTime, String endTime) {
		pageNo = (pageNo - 1) * pageSize;
		return pointMapper.getPointByCondition(pageNo, pageSize, startTime, endTime);
	}

	/*
	 * 增加积分 times:倍数
	 */
	@Transactional
	public void plusPoint(Integer type, Long userId, Integer times) {
		PointExample pe = new PointExample();
		pe.or().andPointTypeEqualTo(type).andIsStartEqualTo(1);
		List<PointWithBLOBs> points = pointMapper.selectByExampleWithBLOBs(pe);
		logger.info("====================类型"+type);
		logger.info("====================积分"+points.size());
		if (points.size() > 1) {
			throw new HelperException(CodeMsg.POINT_RULE_ERROR.getCode(), CodeMsg.POINT_RULE_ERROR.getMsg());
		} else if (points.size() == 0) {
			throw new HelperException(CodeMsg.NO_POINT_ERROR.getCode(), CodeMsg.NO_POINT_ERROR.getMsg());
		}
		PointWithBLOBs point = points.get(0);
		
		logger.info("====================积分"+point.toString());
		Integer pointNum = (point.getPointNum()) * times;
		logger.info("=====================积分数目"+pointNum);
//		userMapper.increasePoint(pointNum, userId);
		
		
		// if (num == 0) {
		// throw new HelperException(CodeMsg.INCREASE_POINT_ERROR.getCode(),
		// CodeMsg.INCREASE_POINT_ERROR.getMsg());
		// }
		User user = userMapper.selectByPrimaryKey(userId);
		Integer userPoint = user.getPoints();
		
		User uPoint = new User();
		uPoint.setId(userId);
		uPoint.setPoints(userPoint+pointNum);
		
		userMapper.updateByPrimaryKeySelective(uPoint);
		
		ActionWithBLOBs actionWithBLOBs = new ActionWithBLOBs();
		actionWithBLOBs.setUserId(userId);
		actionWithBLOBs.setActionInformation((point.getPointInformation()).replaceAll("%", pointNum.toString()));
		actionWithBLOBs.setActionPoint(uPoint.getPoints());
		actionWithBLOBs.setActionType(2);
		actionWithBLOBs.setCreateDate(ToolUtil.getTime(0));
		actionService.addAction(actionWithBLOBs);

	}

	@Override
	public void isStart(Long id, Integer isStart) {
		PointWithBLOBs pointWithBLOBs = new PointWithBLOBs();
		pointWithBLOBs.setId(id);
		pointWithBLOBs.setIsStart(isStart);
		pointMapper.updateByPrimaryKeySelective(pointWithBLOBs);
	}

	@Override
	public void addPointRule(String name, String introduce, Integer num, Integer type) {
		PointWithBLOBs point = new PointWithBLOBs();
		point.setCreateDate(ToolUtil.getTime(0));
		point.setPointName(name);
		point.setPointNum(num);
		point.setPointType(type);
		point.setPointInformation(introduce);

		pointMapper.insertSelective(point);

	}

	@Override
	public void updatePointRule(Long id, String name, String introduce, Integer num, Integer type,String rule) {
		PointWithBLOBs point = new PointWithBLOBs();
		point.setId(id);
		point.setPointName(name);
		point.setPointNum(num);
		point.setPointType(type);
		point.setPointInformation(introduce);
		point.setPointRule(rule);
		point.setUpdateDate(ToolUtil.getTime(0));
		
		pointMapper.updateByPrimaryKeySelective(point);
	}

}
