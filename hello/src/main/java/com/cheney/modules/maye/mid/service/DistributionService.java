package com.cheney.modules.maye.mid.service;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.cheney.modules.maye.db.model.Authority;
import com.cheney.modules.maye.db.model.Role;

public interface DistributionService {
	
	/** 
	 * 添加角色
	*  void
	* @author WangBo
	* @date 2018年7月20日下午2:33:02
	*/ 
	void addRole(Long adminId,Long roleId);
	
	/** 
	 * 通过用户ID获取角色
	* @param adminId
	* @return Role
	* @author WangBo
	* @date 2018年7月20日下午3:01:45
	*/ 
	Role getRoleByAdminId(Long adminId);
	
	
	/** 
	 * 添加权限
	* @param roleId void
	* @author WangBo
	* @date 2018年7月20日下午3:53:53
	*/ 
	void addAuthority(Long roleId,String authIds);
	
	
	/** 根据角色ID获取权限
	* @param roleId
	* @return List<Authority>
	* @author WangBo
	* @date 2018年7月20日下午4:12:11
	*/ 
	List<Authority> getAuthorityByRoleId(Long roleId);
	/**
     * 给用户移除角色
     */
    boolean deleteUserRole(@Param("adminId") Long adminId,@Param("roleId") Long roleId);
    /**
     * 给角色移除权限
     */
    boolean deleteRoleAut(@Param("roleId")Long roleId,@Param("menuId")Long menuId);
}
