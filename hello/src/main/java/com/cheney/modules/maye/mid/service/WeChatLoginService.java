package com.cheney.modules.maye.mid.service;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;

/** 
 * 微信登录模块
 * @ClassName: WeChatLoginService 
 * @Description: TODO
 * @author: WangBo
 * @date: 2018年7月12日 下午5:35:58  
 */
public interface WeChatLoginService {
	
	/** 
	 * 获取微信用户信息
	*  void
	* @author WangBo
	* @date 2018年7月12日下午6:14:37
	*/ 
	void getWeChatUserInfo(String access_token,String openid);
	
	/** 
	 * 获取微信用户openId
	* @param code
	* @return
	* @throws ClientProtocolException
	* @throws IOException String
	* @author WangBo
	* @date 2018年7月12日下午6:27:17
	*/ 
	String getUserOpenId(String code);
	
	/** 
	 * 获取用户ID
	* @param code
	* @param request
	* @return String
	* @author WangBo
	* @date 2018年7月24日下午2:58:19
	*/ 
	String getUserId(String key);
	
}
