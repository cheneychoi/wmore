package com.cheney.modules.maye.mid.impl;

import java.io.IOException;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.cheney.modules.maye.db.mapper.PictureMapper;
import com.cheney.modules.maye.db.model.Picture;
import com.cheney.modules.maye.db.model.PictureExample;
import com.cheney.modules.maye.mid.service.PictureService;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.prop.AliOSSProperties;
import com.cheney.share.mid.utils.AliOSSUtils;
import com.cheney.share.mid.utils.PictureUtils;
import com.cheney.share.mid.utils.ToolUtil;

@Service
public class PictureServiceImpl implements PictureService {

	@Resource
	PictureMapper pictureMapper;
	
	@Resource
	AliOSSProperties aliOSSProperties;
	

	// 查询大Banner图片
	@Override
	public List<Picture> getBigBannerPicture(int type) {
		PictureExample pe = new PictureExample();
		pe.or().andIsBannerEqualTo(1).andPictureTypeEqualTo(type);
		return pictureMapper.selectByExample(pe);
	}

	// 查询小Banner图片
	@Override
	public List<Picture> getSmallBanner(int type) {
		PictureExample pe = new PictureExample();
		pe.or().andIsBannerEqualTo(1).andPictureTypeEqualTo(type);
		return pictureMapper.selectByExample(pe);
	}

	@Override
	public List<Picture> getPicture(Long id) {
		return pictureMapper.getPicture(id);
	}

	@Override
	public void isBanner(Long pictureId, Integer isBanner) {
		Picture picture = new Picture();
		picture.setId(pictureId);
		picture.setIsBanner(isBanner);
		int num = pictureMapper.updateByPrimaryKeySelective(picture);
		if (num == 0) {
			throw new HelperException(CodeMsg.UPDATE_PICTURE_ERROR.getCode(), CodeMsg.UPDATE_PICTURE_ERROR.getMsg());
		}
	}

	@Override
	public Pagination<Picture> getBanner(Integer pageNo, Integer pageSize) {
		Pagination<Picture> page = new Pagination<>(pageNo, pageSize);
		PictureExample example = new PictureExample();
		pageNo = (pageNo - 1) * pageSize;
		List<Picture> list = pictureMapper.getBanner(pageNo, pageSize);
		example.or().andIsBannerEqualTo(1);
		pictureMapper.countByExample(example);
		page.setItems(list);
		page.setRecords(list.size());
		return page;
	}

	@Override
	public void uploadBanner(MultipartFile file,String url,Integer type) {
		uploadPicture(file, 0L, type, url,0L,1);
	}

	@Override
	public String uploadPicture(MultipartFile file,Long pictureId,Integer picType,String url,Long courseId,Integer isBanner) {
		int picNum = 0;
		String key = null;
		try {
			if (pictureId == 0) {
				key = AliOSSUtils.uploadToOSS(aliOSSProperties, file.getInputStream());
			} else {
				Picture pic = pictureMapper.selectByPrimaryKey(pictureId);
				key = AliOSSUtils.uploadToOSS(aliOSSProperties, file.getInputStream(), pic.getOssId());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		String type = file.getContentType();
		String picUrl = aliOSSProperties.getBucket() + "." + aliOSSProperties.getGateway() + "/" + key;
		if (type.indexOf("image") != -1) {
			PictureUtils picUtils = new PictureUtils();
			Picture picture = picUtils.getPictureModel(file);
			picture.setOssId(key);
			picture.setContentUrl(url);
			picture.setPictureType(picType);
			picture.setCourseId(courseId);
			picture.setIsBanner(isBanner);
			picture.setPictureUrl(picUrl);
			picNum = pictureMapper.insertSelective(picture);
		} else {
			throw new HelperException(CodeMsg.FILE_NOT_PICTURE.getCode(), CodeMsg.FILE_NOT_PICTURE.getMsg());
		}
		if (picNum == 0) {
			AliOSSUtils.removeFromOSS(aliOSSProperties, key);
			throw new HelperException(CodeMsg.PICTURE_SAVE_ERROR.getCode(), CodeMsg.PICTURE_SAVE_ERROR.getMsg());
		}
		return picUrl;
	}

	@Override
	public List<Picture> getBigBanner(int type) {
		PictureExample pictureExample = new PictureExample();
		pictureExample.or().andPictureTypeEqualTo(type);
		return pictureMapper.selectByExample(pictureExample);
	}

	@Override
	@Transactional
	public boolean delPic(Long id) {
		Picture picture = pictureMapper.selectByPrimaryKey(id);
		if (picture!=null) {
			AliOSSUtils.removeFromOSS(aliOSSProperties, picture.getOssId());
		}
		int num=pictureMapper.deleteByPrimaryKey(id);
		return num>0 ? true : false;
	}

	@Override
	public Picture getBannerById(Long id) {
		return pictureMapper.selectByPrimaryKey(id);
	}

	@Override
	public void editBanner(Long id, String contentUrl, Integer type) {
		Picture picture = new Picture();
		picture.setId(id);
		picture.setContentUrl(contentUrl);
		picture.setPictureType(type);
		picture.setUpdateDate(ToolUtil.getTime(0));
		pictureMapper.updateByPrimaryKeySelective(picture);
	}
}
