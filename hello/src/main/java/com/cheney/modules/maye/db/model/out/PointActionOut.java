package com.cheney.modules.maye.db.model.out;

import java.util.List;

import com.cheney.modules.maye.db.model.Action;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "积分行为-出")
public class PointActionOut {

	@ApiModelProperty(value = "用户积分")
	private String userPoint;
	
	@ApiModelProperty(value = "积分行为")
	private List<Action> pointAction;

	public String getUserPoint() {
		return userPoint;
	}

	public void setUserPoint(String userPoint) {
		this.userPoint = userPoint;
	}

	public List<Action> getPointAction() {
		return pointAction;
	}

	public void setPointAction(List<Action> pointAction) {
		this.pointAction = pointAction;
	}


}