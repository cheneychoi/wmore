package com.cheney.modules.maye.mid.impl;

import java.util.LinkedList;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.cheney.modules.maye.db.mapper.NoticeMapper;
import com.cheney.modules.maye.db.mapper.NoticeMasterplateMapper;
import com.cheney.modules.maye.db.mapper.UserMapper;
import com.cheney.modules.maye.db.model.Notice;
import com.cheney.modules.maye.db.model.NoticeExample;
import com.cheney.modules.maye.db.model.User;
import com.cheney.modules.maye.db.model.UserExample;
import com.cheney.modules.maye.db.model.out.NoticeOut;
import com.cheney.modules.maye.mid.service.NoticeService;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.utils.ToolUtil;

@Service
public class NoticeServiceImpl implements NoticeService {

	Logger logger = LoggerFactory.getLogger("notice");
	
	@Resource
	NoticeMapper noticeMapper;

	@Resource
	UserMapper userMapper;

	@Resource
	NoticeMasterplateMapper noticeMasterplateMapper;

	/*
	 * 获取今日通知
	 */
	public List<NoticeOut> getNoticeByDay(Long userId) {
		logger.info("通知方法的userID是-------------"+userId);

		List<Notice> notices = noticeMapper.getTodayNotice(userId);
		
		logger.info("获取通知的个数----------"+notices.size());
		
		Notice no = null;
		List<NoticeOut> noticeOuts = new LinkedList<>();
		int size = notices.size();
		if (size > 0) {
			for (int i = 0; i < size; i++) {
				no = notices.get(i);
				no.setIsFirst(2);
				noticeMapper.updateByPrimaryKeySelective(no);
				noticeOuts.add(NoticeOut.from(no));
			}
		}
		
		logger.info("获取通知before的个数----------"+noticeOuts.size());
		return noticeOuts;
	}

	/*
	 * 添加登陆通知
	 */
	public void addLoginNotice(HttpServletRequest request) {

		HttpSession session = request.getSession();
		String openid = (String) session.getAttribute("openid");

		UserExample ue = new UserExample();
		ue.or().andWechatOpenidEqualTo(openid);

		List<User> users = userMapper.selectByExample(ue);
		int size = users.size();
		if (size > 0) {
			Notice notice = new Notice();
			notice.setUserId(users.get(0).getId());
			notice.setTitle("签到成功！");
			notice.setContent("签到成功");
			notice.setIsFirst(1);
			notice.setCreateDate(ToolUtil.getTime(0));
			notice.setNoticeDate(ToolUtil.getTime(1));
			noticeMapper.insertSelective(notice);
		}

	}

	/*
	 * 添加通知
	 */
	public void addNotice(Notice notice) {
		int num = noticeMapper.insertSelective(notice);
		if (num != 1) {
			throw new HelperException(CodeMsg.ADD_NOTICE_ERROR.getCode(), CodeMsg.ADD_NOTICE_ERROR.getMsg());
		}
	}

	/**
	 * 获取未读的通知
	 * 
	 * @return List<Notice>
	 * @author WangBo
	 * @date 2018年7月15日下午5:42:07
	 */
	public List<Notice> getUnreadNotice(Long userId) {
		NoticeExample ne = new NoticeExample();
		ne.or().andIsReadEqualTo(1).andUserIdEqualTo(userId).andIsFirstEqualTo(2);
		List<Notice> notices = noticeMapper.selectByExample(ne);
		return notices;
	}

	/**
	 * 获取已读的通知
	 * 
	 * @return List<Notice>
	 * @author WangBo
	 * @date 2018年7月15日下午5:42:17
	 */
	public List<Notice> getReadNotice(Long userId) {
		NoticeExample ne = new NoticeExample();
		ne.or().andIsReadEqualTo(2).andUserIdEqualTo(userId).andIsFirstEqualTo(2);
		List<Notice> notices = noticeMapper.selectByExample(ne);
		return notices;
	}

	/**
	 * 将未读通知变为已读通知 void
	 * 
	 * @author WangBo
	 * @date 2018年7月15日下午5:42:33
	 */
	public Notice toReadNotice(Long noticeId) {
		Notice notice = new Notice();
		notice.setId(noticeId);
		notice.setIsRead(2);
		noticeMapper.updateByPrimaryKeySelective(notice);
		return noticeMapper.selectByPrimaryKey(noticeId);
	}

}
