package com.cheney.modules.maye.api.back;

import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.db.model.out.AdminOut;
import com.cheney.modules.maye.mid.service.AdminService;
import com.cheney.share.api.controller.BaseController;
import com.cheney.share.db.model.Pagination;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value = "/wmore_back")
public class AdminBackController extends BaseController {
	@Autowired
	AdminService adminService;

	@ApiOperation(value = "管理员登陆匹配", notes = "")
	@RequestMapping(value = "/admin/matchAdmin", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> matchAdmin(@ApiParam("用户名") @RequestParam(value = "username") String username,
			@ApiParam("密码") @RequestParam(value = "password") String password, HttpSession session,HttpServletResponse response) {
		Long id = adminService.login(username, password);
		session.setAttribute("id", id);
		session.setMaxInactiveInterval(30*60*1000);
		Cookie cookie = new Cookie("username", username);
		cookie.setMaxAge(30*60*1000);
		cookie.setPath("/");
		response.addCookie(cookie);
		return success();
	}

	@ApiOperation(value = "管理员登陆验证码", notes = "")
	@RequestMapping(value = "/admin/getCode", method = { RequestMethod.GET })
	@CrossOrigin
	public void getCode(HttpServletRequest request) {
		adminService.getCode(request);
	}

	@ApiOperation(value = "管理员添加", notes = "")
	@RequestMapping(value = "/admin/insertAdmin", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> insertAdmin(@ApiParam(value = "昵称") @RequestParam("nickname") String nickname,
			@ApiParam(value = "姓名") @RequestParam("name") String name,
			@ApiParam(value = "用户名") @RequestParam("username") String username,
			@ApiParam(value = "密码") @RequestParam("password") String password,
			@ApiParam(value = "角色Id") @RequestParam("roleId") Long roleId) {
		return success(adminService.insertAdmin(nickname, name, username, password,roleId));
	}

	@ApiOperation(value = "根据id查看管理员", notes = "")
	@RequestMapping(value = "/admin/getAdminById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAdminById(@ApiParam(value = "管理员id") @RequestParam("id") Long id) {
		return success(adminService.getAdminById(id));
	}

	@ApiOperation(value = "根据id编辑管理员", notes = "")
	@RequestMapping(value = "/admin/updateAdmin", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateAdmin(@ApiParam(value = "id") @RequestParam(value = "id") Long id,
			@ApiParam(value = "昵称") @RequestParam(value = "nickname", required = false) String nickname,
			@ApiParam(value = "姓名") @RequestParam(value = "name", required = false) String name,
			@ApiParam(value = "用户名") @RequestParam(value = "username", required = false) String username,
			@ApiParam(value = "密码") @RequestParam(value = "password", required = false) String password,
			@ApiParam(value = "角色id") @RequestParam(value = "roleId", required = false) Long roleId) {
		return success(adminService.updateAdmin(id, nickname, name, username, password,roleId));
	}

	@ApiOperation(value = "根据id删除管理员", notes = "")
	@RequestMapping(value = "/admin/deleteAdmin", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> deleteAdmin(@ApiParam(value = "管理员id") @RequestParam("id") Long id) {
		return success(adminService.deleteAdmin(id));
	}

	/*@ApiOperation(value = "分页查询所有管理员列表", notes = "")
	@RequestMapping(value = "/admin/getAllAdminByPage", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAllAdminByPage(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		Pagination<AdminOut> admin = adminService.getAllAdminByPage(pageNo, pageSize);
		return success(admin);
	}*/
	@ApiOperation(value = "分页查询所有管理员列表", notes = "")
	@RequestMapping(value = "/admin/getAllAdminByPage", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAllAdminByPage(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		Pagination<AdminOut> admin =adminService.getAllAdminByPage(pageNo, pageSize);
		return success(admin);
	}

	@ApiOperation(value = "批量删除", notes = "")
	@RequestMapping(value = "/admin/deleteAdminBatch", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> deleteAdminBatch(@ApiParam("管理员id") @RequestParam("ids") String ids) {
		return success(adminService.deleteAdminBatch(ids));
	}

	@ApiOperation(value = "修改密码", notes = "")
	@RequestMapping(value = "/admin/updatePsd", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updatePsd(@RequestParam("password") String password,HttpSession session) {
		Long id=(Long) session.getAttribute("id");
		return success(adminService.updatePsd(id, password));
	}

	@ApiOperation(value = "修改角色", notes = "")
	@RequestMapping(value = "/admin/updateRole", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateRole(@ApiParam("管理员ID") @RequestParam("adminId") Long adminId,
			@ApiParam("角色ID") @RequestParam("roleId") Long roleId) {
		adminService.updateRole(adminId, roleId);
		return success();
	}

}
