package com.cheney.modules.maye.mid.service;

import com.cheney.modules.maye.db.model.out.LogOut;
import com.cheney.share.db.model.Pagination;

/** 
 * 日志模块
 * @ClassName: LogService 
 * @Description: TODO
 * @author: WangBo
 * @date: 2018年7月12日 下午1:22:57  
 */
public interface LogService {

	/**
	 * 添加日志 
	* @param log void
	* @author WangBo
	* @date 2018年7月12日下午1:31:11
	*/ 
	void addLog(Long userId,String msg,Integer type);
	
	/**
	 * 倒序分页日志
	 */
	Pagination<LogOut> getLog(Integer pageNo,Integer pageSize);
	
	/** 
	 * 根据条件获取日志
	* @param pageNo
	* @param pageSize
	* @param startTime
	* @param endTime
	* @param username
	* @return List<Log>
	* @author WangBo
	* @date 2018年7月18日下午5:18:33
	*/ 
	Pagination<LogOut> getLogByCondition(Integer pageNo,Integer pageSize,String name,String msg);
	
}
