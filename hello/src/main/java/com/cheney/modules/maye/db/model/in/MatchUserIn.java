package com.cheney.modules.maye.db.model.in;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "匹配用户-入")
public class MatchUserIn {

	@ApiModelProperty(value = "用户姓名")
	private String name;
	
	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "手机号")
	private String mobile;
	
	@ApiModelProperty(value = "验证码")
	private String verCode;

	@ApiModelProperty(value = "公司编号")
	private String code;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getVerCode() {
		return verCode;
	}

	public void setVerCode(String verCode) {
		this.verCode = verCode;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "MatchUserIn [name=" + name + ", email=" + email + ", mobile=" + mobile + ", verCode=" + verCode
				+ ", code=" + code + "]";
	}

}