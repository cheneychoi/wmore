package com.cheney.modules.maye.api.back;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.cheney.modules.maye.db.model.Company;
import com.cheney.modules.maye.db.model.out.CompanyOut;
import com.cheney.modules.maye.mid.service.CompanyService;
import com.cheney.share.api.controller.BaseController;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.exception.HelperException;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/wmore_back")
public class CompanyBackController extends BaseController {
	@Autowired
	CompanyService companyService;

	@ApiOperation(value = "查询企业列表", notes = "", response = CompanyOut.class)
	@RequestMapping(value = "/company/getCompany", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCompany(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		Pagination<Company> company = companyService.getCompany(pageNo, pageSize);
		return success(company);
	}
	
	@ApiOperation(value = "根据ID查询企业", notes = "")
	@RequestMapping(value = "/company/getCompanyById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCompanyById(@ApiParam(value="企业id") @RequestParam("id") Long id){
		return success(companyService.getCompanyById(id));
	}

	@ApiOperation(value = "删除企业", notes = "")
	@RequestMapping(value = "/company/deleteCompany", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> deleteByPrimaryKey(@ApiParam(value = "企业id") @RequestParam("id") String id,HttpSession session)
			throws HelperException {
		Long companyId=Long.parseLong(id);
		Long userId = (Long) session.getAttribute("id");
		companyService.deleteByPrimaryKey(userId,companyId);
		return success();
	}

	@ApiOperation(value="冻结企业",notes="修改企业是否冻结的状态")
	@RequestMapping(value="/company/updateCompanyFrozen",method= {RequestMethod.POST})
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateCompanyFrozen(@ApiParam("企业id") @RequestParam("id") Long id){
		return success(companyService.updateCompanyFrozen(id));
	}
	
	@ApiOperation(value="冻结企业",notes="修改企业是否冻结的状态")
	@RequestMapping(value="/company/relieveCompany",method= {RequestMethod.POST})
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> relieveCompany(@ApiParam("企业id") @RequestParam("id") Long id){
		companyService.relieveCompany(id);
		return success();
	}
	
	@ApiOperation(value="编辑企业",notes="")
	@RequestMapping(value="/company/updateCompany",method= {RequestMethod.POST})
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateCompany(HttpServletRequest httpServletRequest){
		
		String id = httpServletRequest.getParameter("id");
		String companyName = httpServletRequest.getParameter("companyName");
		String companyNumber = httpServletRequest.getParameter("companyNumber");
		String companyPerson = httpServletRequest.getParameter("companyPerson");
		String companyMobile = httpServletRequest.getParameter("companyMobile");
		String pictureId = httpServletRequest.getParameter("pictureId");
		MultipartHttpServletRequest request = (MultipartHttpServletRequest) httpServletRequest;
		List<MultipartFile> files = request.getFiles("file");
		System.out.println(files.size());
		return success(companyService.updateCompany(Long.valueOf(id),companyName, Integer.valueOf(companyNumber),companyPerson,companyMobile,Long.valueOf(pictureId),files));
	}
	@ApiOperation(value="添加企业",notes="",response=CompanyOut.class)
	@RequestMapping(value="/company/insertCompany",method= {RequestMethod.POST})
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> insertCompany(HttpServletRequest request,HttpSession session) throws HelperException{
		Long userId = (Long) session.getAttribute("id");
		String companyName = request.getParameter("companyName");
		String companyNumber = request.getParameter("companyNumber");
		String companyPerson = request.getParameter("companyPerson");
		String companyMobile = request.getParameter("companyMobile");
		MultipartHttpServletRequest multipartHttpServletRequest = (MultipartHttpServletRequest) request;
		MultipartFile file = multipartHttpServletRequest.getFile("file");
		return success(companyService.insertCompany(userId,companyName, Integer.valueOf(companyNumber),companyPerson,companyMobile,file));
	}

	@ApiOperation(value = "企业按代码或名称条件查询", notes = "可以通过代码或名称查询企业")
	@RequestMapping(value = "/company/selectCompanyByOne", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> selectCompanyByOne(
			@ApiParam(value = "企业名称") @RequestParam(value = "companyName", required = false) String companyName,
			@ApiParam(value = "企业代码") @RequestParam(value = "companyCode", required = false) String companyCode) {

		Company company = companyService.selectCompanyByOne(companyCode, companyName);
		return success(CompanyOut.from(company));
	}

	@ApiOperation(value = "企业按时间或状态条件查询", notes = "可以通过时间或状态查询企业返回多条")
	@RequestMapping(value = "/company/selectCompanyByMany", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> selectCompanyByMany(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize,
			@ApiParam(value = "起始时间") @RequestParam(value = "startTime", required = false) String startTime,
			@ApiParam(value = "结束时间") @RequestParam(value = "endTime", required = false) String endTime,
			@ApiParam(value = "企业状态") @RequestParam(value = "companyState", required = false) Integer companyState) {

		List<Company> companies = companyService.selectCompanyByMany(pageNo, pageSize, startTime, endTime,
				companyState);
		return success(companies);
	}

}
