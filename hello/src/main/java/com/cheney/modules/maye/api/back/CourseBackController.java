package com.cheney.modules.maye.api.back;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.cheney.modules.maye.db.model.Course;
import com.cheney.modules.maye.db.model.out.CourseBackOut;
import com.cheney.modules.maye.db.model.out.CourseOut;
import com.cheney.modules.maye.db.model.out.MakeCourseOut;
import com.cheney.modules.maye.mid.service.CourseService;
import com.cheney.share.api.controller.BaseController;
import com.cheney.share.db.model.Pagination;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "课程-后台")
@Controller
@RequestMapping(value = "/wmore_back")
public class CourseBackController extends BaseController {
	@Autowired
	CourseService courseService;

	@ApiOperation(value = "添加课程信息", notes = "用于后台添加课程")
	@RequestMapping(value = "/course/addCourse", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> addCourse(HttpServletRequest request) {
		String courseName = request.getParameter("courseName");
		String courseNameEN = request.getParameter("courseNameEN");
		String coursePlace = request.getParameter("coursePlace");
		String date = request.getParameter("courseDate");
		String task = request.getParameter("task");
		String num = request.getParameter("courseNum");
		String introduction = request.getParameter("introduction");
		String crowd = request.getParameter("crowd");
		String trainingEffect = request.getParameter("trainingEffect");
		String trainingPreparation = request.getParameter("trainingPreparation");
		String otherBusiness = request.getParameter("otherBusiness");
		String courseAuthority = request.getParameter("courseAuthority");
		String type = request.getParameter("type");
		String companyName = request.getParameter("companyName");

		if (StringUtils.isEmpty(num)) {
			num = "0";
		}
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date courseDate = null;
		try {
			courseDate = sdf.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;

		// 获得文件：
		List<MultipartFile> files = multipartRequest.getFiles("file[]");

		HttpSession session = request.getSession();
		Long userId = (Long) session.getAttribute("id");

		courseService.addCourse(userId, courseName, courseNameEN, coursePlace, courseDate, Integer.valueOf(task),
				Integer.valueOf(num), introduction, crowd, trainingEffect, trainingPreparation, otherBusiness,
				Integer.valueOf(courseAuthority), Integer.valueOf(type), files, companyName);
		return success();
	}

	@ApiOperation(value = "后台查当日预约课程详情 ", notes = "返回数据", response = CourseOut.class)
	@RequestMapping(value = "/course/getCourseByToDay", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCourseByToDay(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		System.out.println(pageNo+"------"+pageSize);
		Pagination<MakeCourseOut> out = courseService.getCourseByToDay(pageNo, pageSize);
		return success(out);
	}

	@ApiOperation(value = "取消课程列表 ", notes = "返回数据", response = CourseOut.class)
	@RequestMapping(value = "/course/getCourseByCancel", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCourseByCancel(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		Pagination<Course> course = courseService.getCourseByCancel(pageNo, pageSize);
		return success(course);
	}

	@ApiOperation(value = "一周预约详情 ", notes = "返回数据", response = CourseOut.class)
	@RequestMapping(value = "/course/getCourseByWeek", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCourseByWeek(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		Pagination<MakeCourseOut> course = courseService.getCourseByWeek(pageNo, pageSize);
		return success(course);
	}

	@ApiOperation(value = "生成二维码 ", notes = "生成课程二维码供员工签到")
	@RequestMapping(value = "/course/createQRCode", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> createQRCode(@ApiParam(value = "课程的Id") @RequestParam("courseId") Long courseId) {
		return success(courseService.createQRCode(courseId));
	}

	@ApiOperation(value = "查询所有课程", notes = "用于后台查询所有的课程列表")
	@RequestMapping(value = "/course/getAllCourses", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAllCourses(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		return success(courseService.getAllCourses(pageNo, pageSize));
	}

	@ApiOperation(value = "查询所有课程根据企业筛选", notes = "用于后台查询所有的课程列表")
	@RequestMapping(value = "/course/getCourseByCompany", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCourseByCompany(@RequestParam("companyId") Long companyId) {
		return success(courseService.getCourseByCompany(companyId));
	}

	@ApiOperation(value = "根据课程名称或者日期查询课程", notes = "用于后台根据课程名称或者日期查询课程列表", response = CourseBackOut.class)
	@RequestMapping(value = "/course/getCourseByCondition", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCourseByCondition(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize,
			@ApiParam(value = "起始时间") @RequestParam(value = "startTime", required = false) String startTime,
			@ApiParam(value = "结束时间") @RequestParam(value = "endTime", required = false) String endTime,
			@ApiParam(value = "课程名称") @RequestParam(value = "courseName", required = false) String courseName) {
		return success(courseService.getCourseByCondition(pageNo, pageSize, startTime, endTime, courseName));
	}

	@ApiOperation(value = "课程名的模糊查询", notes = "")
	@RequestMapping(value = "/course/getCourseByFuzzy", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCourseByFuzzy(
			@ApiParam(value = "课程名") @RequestParam(value = "courseName", required = false) String courseName,
			@ApiParam(value = "公司名") @RequestParam(value = "companyName", required = false) String companyName,
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		Pagination<Course> course = courseService.getCourseByFuzzy(courseName, companyName, pageNo, pageSize);
		return success(course);
	}

	@ApiOperation(value = "根据课程名称查询课程", notes = "")
	@RequestMapping(value = "/course/getCourseByName", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCourseByName(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize,
			@ApiParam(value = "课程名") @RequestParam(value = "courseName", required = false) String courseName,
			@ApiParam(value = "起始时间") @RequestParam(value = "startTime", required = false) String startTime,
			@ApiParam(value = "结束时间") @RequestParam(value = "endTime", required = false) String endTime) {
		return success(courseService.getCourseByName(pageNo, pageSize, courseName, startTime, endTime));
	}

	@ApiOperation(value = "当日预约按企业筛选", notes = "")
	@RequestMapping(value = "/course/getCompanyByName", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCompanyByName(@ApiParam(value = "公司id") @RequestParam("companyId") Long companyId) {
		return success(courseService.getCompanyByName(companyId));
	}

	@ApiOperation(value = "一周预约按企业筛选", notes = "")
	@RequestMapping(value = "/course/getCompanyByName1", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCompanyByName1(@ApiParam(value = "公司id") @RequestParam("companyId") Long companyId) {
		return success(courseService.getCompanyByName1(companyId));
	}

	@ApiOperation(value = "根据课程名称查询一周模糊课程", notes = "")
	@RequestMapping(value = "/course/getCourseByName1", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCourseByName1(
			@ApiParam(value = "课程名") @RequestParam(value = "courseName", required = false) String courseName,
			@ApiParam(value = "起始时间") @RequestParam(value = "startTime", required = false) String startTime,
			@ApiParam(value = "结束时间") @RequestParam(value = "endTime", required = false) String endTime) {
		return success(courseService.getCourseByName1(courseName, startTime, endTime));
	}

	@ApiOperation(value = "后台根据ID查课程", notes = "")
	@RequestMapping(value = "/course/getCourseById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCourseById(@ApiParam(value = "课程id") @RequestParam("id") Long id) {
		return success(courseService.getCourseById(id));
	}

	@ApiOperation(value = "后台根据ID编辑课程", notes = "")
	@RequestMapping(value = "/course/updateCourse", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateCourse(HttpServletRequest request) throws ParseException {
		String id = request.getParameter("id");
		String courseName = request.getParameter("courseName");
		String courseNameEN = request.getParameter("courseNameEN");
		String coursePlace = request.getParameter("coursePlace");
		String date = request.getParameter("courseDate");
		String task = request.getParameter("task");
		String num = request.getParameter("courseNum");
		String introduction = request.getParameter("introduction");
		String crowd = request.getParameter("crowd");
		String trainingEffect = request.getParameter("trainingEffect");
		String trainingPreparation = request.getParameter("trainingPreparation");
		String otherBusiness = request.getParameter("otherBusiness");
		String courseAuthority = request.getParameter("courseAuthority");
		String type = request.getParameter("type");
		String companyName = request.getParameter("companyName");

		if (StringUtils.isEmpty(num)) {
			num = "0";
		}

		Long d = Long.valueOf(date);

		Date courseDate = new Date(d);

		MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) request;
		// 获得文件：
		List<MultipartFile> files = multipartRequest.getFiles("file[]");
		courseService.updateCourse(Long.valueOf(id), courseName, courseNameEN, coursePlace, courseDate,
				Integer.valueOf(task), Integer.valueOf(num), introduction, crowd, trainingEffect, trainingPreparation,
				otherBusiness, Integer.valueOf(courseAuthority), Integer.valueOf(type), files, companyName);
		return success();
	}

	@ApiOperation(value = "删除课程", notes = "返回数据")
	@RequestMapping(value = "/course/delCourse", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> delCourse(@ApiParam(value = "id") @RequestParam("id") Long id, HttpSession session) {
		Long userId = (Long) session.getAttribute("id");
		courseService.delCourse(userId, id);
		return success();
	}

	@ApiOperation(value = "导出Excel", notes = "返回数据")
	@RequestMapping(value = "/course/exportCourse", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> exportCourse(
			@ApiParam(value = "课程名") @RequestParam(value = "courseName", required = false) String courseName,
			@ApiParam(value = "起始时间") @RequestParam(value = "startTime", required = false) String startTime,
			@ApiParam(value = "结束时间") @RequestParam(value = "endTime", required = false) String endTime,
			@ApiParam(value = "公司ID") @RequestParam("companyId") Long companyId) {
		String path = courseService.exportCourse(courseName, startTime, endTime, companyId);
		System.out.println(path);
		return success(path);
	}

}
