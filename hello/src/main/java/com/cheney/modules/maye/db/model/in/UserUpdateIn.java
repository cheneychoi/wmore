package com.cheney.modules.maye.db.model.in;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.User;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="修改用户--入")
public class UserUpdateIn {
	@ApiModelProperty(value="用户id")
	private Long id;
	
	@ApiModelProperty(value = "真实姓名")
	private String name;


	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "手机号")
	private String mobile;
	@ApiModelProperty(value = "性别 男:1 女:2")
	private Integer sex;


	@ApiModelProperty(value = "职位 高管-1 普通员工-2")
	private int position;


	public User toUserUpdateIn() {
		User user = new User();
		BeanUtils.copyProperties(this, user);
		return user;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	
}
