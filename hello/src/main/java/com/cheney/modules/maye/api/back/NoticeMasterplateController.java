package com.cheney.modules.maye.api.back;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.db.model.NoticeMasterplate;
import com.cheney.modules.maye.mid.service.NoticeMasterplateService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "通知模板")
@Controller
@RequestMapping(value = "/wmore_back")
public class NoticeMasterplateController extends BaseController {

	@Autowired
	NoticeMasterplateService noticeMasterplateService;

	@ApiOperation(value = "添加日志模板", notes = "用于后台添加日志模板")
	@RequestMapping(value = "/noticeMasterplate/addNoticeMasterplate", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> addNoticeMasterplate(
			@ApiParam("标题") @RequestParam(value = "title", required = false) String title,
			@ApiParam("正文") @RequestParam(value = "content") String content,
			@ApiParam("通知类型") @RequestParam(value = "type", required = false) Integer type,
			@ApiParam("是否停用") @RequestParam(value = "isStop", required = false) Integer isStop) {
		NoticeMasterplate noticeMasterplate = new NoticeMasterplate();
		noticeMasterplate.setTitle(title);
		noticeMasterplate.setContent(content);
		noticeMasterplate.setIsStop(isStop);
		noticeMasterplate.setType(type);
		noticeMasterplateService.addNoticeMasterplate(noticeMasterplate);
		return success();
	}

	@ApiOperation(value = "根据ID获取模板", notes = "用于后台根据ID获取模板")
	@RequestMapping(value = "/noticeMasterplate/getNoticeMasterplateById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getNoticeMasterplateById(@ApiParam(value = "模板ID") @RequestParam("id") Long id) {
		return success(noticeMasterplateService.getNoticeMasterplateById(id));
	}

	@ApiOperation(value = "根据ID修改模板", notes = "用于后台根据ID修改模板")
	@RequestMapping(value = "/noticeMasterplate/updateNoticeMasterplateById", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateNoticeMasterplateById(@ApiParam("标题") @RequestParam(value = "title", required = false) String title,
			@ApiParam("正文") @RequestParam(value = "content") String content,
			@ApiParam("id") @RequestParam(value = "id") Long id,
			@ApiParam("通知类型") @RequestParam(value = "type", required = false) Integer type,
			@ApiParam("是否停用") @RequestParam(value = "isStop", required = false) Integer isStop) {
		NoticeMasterplate noticeMasterplate = new NoticeMasterplate();
		noticeMasterplate.setId(id);
		noticeMasterplate.setTitle(title);
		noticeMasterplate.setContent(content);
		noticeMasterplate.setIsStop(isStop);
		noticeMasterplate.setType(type);
		noticeMasterplateService.updateNoticeMasterplateById(noticeMasterplate);
		return success();
	}

	@ApiOperation(value = "获取所有的通知模板", notes = "用于后台获取所有的通知模板")
	@RequestMapping(value = "/noticeMasterplate/getAllNoticeMasterplate", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAllNoticeMasterplate(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		return success(noticeMasterplateService.getAllNoticeMasterplate(pageNo, pageSize));
	}

	@ApiOperation(value = "根据模板类型获取所有的通知模板", notes = "用于后台根据模板类型获取所有的通知模板")
	@RequestMapping(value = "/noticeMasterplate/updateNoticeMasterplateByType", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateNoticeMasterplateByType(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize,
			@ApiParam(value = "模板类型") @RequestParam("type") Integer type) {
		return success(noticeMasterplateService.selectNoticeMasterplateByType(pageNo, pageSize, type));
	}

	@ApiOperation(value = "是否停用", notes = "用于后台是否停用模板")
	@RequestMapping(value = "/noticeMasterplate/isStopNoticeMasterplate", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> isStopNoticeMasterplate(@ApiParam(value = "模板id") @RequestParam("id") Long id,
			@ApiParam(value = "是否停用") @RequestParam("isStop") Integer isStop) {
		noticeMasterplateService.isStopNoticeMasterplate(id, isStop);
		return success();
	}
}
