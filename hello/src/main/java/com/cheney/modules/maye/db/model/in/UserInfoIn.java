package com.cheney.modules.maye.db.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.User;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "用户-入")
public class UserInfoIn {

	@ApiModelProperty(value = "用户ID")
	private Long id;
	
	@ApiModelProperty(value = "性别（1-男 2-女）")
	private Integer sex;

	@ApiModelProperty(value = "生日")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	private Date birthday;

	@ApiModelProperty(value = "城市")
	private String city;

	@ApiModelProperty(value = "学历")
	private String education;

	@ApiModelProperty(value = "行业")
	private String industry;

	@ApiModelProperty(value = "爱好")
	private String hobby;
	
	@ApiModelProperty(value = "薪资范围")
	private String incomeRange;
	
	@ApiModelProperty(value = "感兴趣的内容")
	private String interestingContent;
	
	@ApiModelProperty(value = "头像ID")
	private String profixId;

	public User toUser() {
		User user = new User();
		BeanUtils.copyProperties(this, user);
		return user;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getProfixId() {
		return profixId;
	}

	public void setProfixId(String profixId) {
		this.profixId = profixId;
	}

	public String getIncomeRange() {
		return incomeRange;
	}

	public void setIncomeRange(String incomeRange) {
		this.incomeRange = incomeRange;
	}

	public String getInterestingContent() {
		return interestingContent;
	}

	public void setInterestingContent(String interestingContent) {
		this.interestingContent = interestingContent;
	}
	
	
}