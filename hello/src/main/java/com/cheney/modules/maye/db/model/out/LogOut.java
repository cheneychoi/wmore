package com.cheney.modules.maye.db.model.out;


import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Log;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="日志--出")
public class LogOut {
	
	@ApiModelProperty(value="id")
   private String id;
	
	@ApiModelProperty(value="用户名")
	   private Long userId;
	
	@ApiModelProperty(value="用户名")
   private String name;

	@ApiModelProperty(value="日志信息")
   private String logMsg;
	
	@ApiModelProperty(value="创建时间")
   private Date createDate;
	
	public static LogOut from(Log log) {
		LogOut logOut = new LogOut();
        BeanUtils.copyProperties(log, logOut);
        return logOut;
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}


	public String getLogMsg() {
		return logMsg;
	}

	public void setLogMsg(String logMsg) {
		this.logMsg = logMsg;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
