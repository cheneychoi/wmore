package com.cheney.modules.maye.db.model.out;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.UserCourse;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel(value = "用户-出")
public class UserCourseOut {
	@ApiModelProperty(value = "id")
	private Long id;
	@ApiModelProperty(value = "用户id")
	private String userId;
	@ApiModelProperty(value = "课程的id")
	private String courseId;
	@ApiModelProperty(value = "预约的状态 1-已预约 2-已完成 3-已取消")
	private Integer state;
	@ApiModelProperty(value = "评论")
	private String evaluate;
	@ApiModelProperty(value = "评分")
	private Integer score;
	@ApiModelProperty(value = "预约的时间")
	private Date courseDate;
	@ApiModelProperty(value = "创建时间")
	private Date createDate;
	@ApiModelProperty(value = "修改时间")
	private Date updateDate;

	public static UserCourseOut from(UserCourse uc) {
		UserCourseOut out = new UserCourseOut();
		BeanUtils.copyProperties(uc, out);
		return out;
	}
}
