package com.cheney.modules.maye.mid.service;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.cheney.modules.maye.db.model.Notice;
import com.cheney.modules.maye.db.model.User;
import com.cheney.modules.maye.db.model.in.ExcelIn;
import com.cheney.modules.maye.db.model.in.MatchUserIn;
import com.cheney.modules.maye.db.model.in.UserAddIn;
import com.cheney.modules.maye.db.model.out.CourseOut;
import com.cheney.modules.maye.db.model.out.UcOut;
import com.cheney.modules.maye.db.model.out.UserBackOut;
import com.cheney.modules.maye.db.model.out.UserInfoOut;
import com.cheney.modules.maye.db.model.out.UserPictureOut;
import com.cheney.share.db.model.Pagination;

/**
 * 用户模块
 * 
 * @ClassName: UserService
 * @Description: TODO
 * @author: WangBo
 * @date: 2018年7月6日 上午10:59:16
 */
public interface UserService {

	/**
	 * 修改用户基本信息
	 * 
	 * @return boolean
	 * @author WangBo
	 * @date 2018年7月6日上午10:59:36
	 */
	void editUserInfo(User user);
	/**
	 * 用户基本信息上传头像
	 */
	String uploadHead(String url,Long pictureId);
	
	/**
	 * 编辑头像
	 */
	String updateHead(String accessUrl, String mediaId, Long pictureId);
	
	/**
	 * 添加用户
	 * 
	 * @return boolean
	 * @author WangBo
	 * @date 2018年7月6日下午3:51:23
	 */
	void addUser(User user);

	/**
	 * 获取用户基本信息
	 * 
	 * @return User
	 * @author WangBo
	 * @date 2018年7月6日上午11:27:32
	 */
	UserInfoOut getUserInfo(Long userId);

	/**
	 * 查询用户课程信息与状态 (已预约)
	 * 
	 * @return UserCourse
	 * @author WangBo
	 * @date 2018年7月6日上午11:03:28
	 */
	List<CourseOut> getReservedCourse(Long userId);

	/**
	 * 查询用户课程信息与状态 (已完成)
	 * 
	 * @return UserCourse
	 * @author WangBo
	 * @date 2018年7月6日上午11:03:28
	 */
	List<CourseOut> getFinishedCourse(Long userId);

	/**
	 * 积分获取列表和用户积分
	 * 
	 * @return List
	 * @author WangBo
	 * @date 2018年7月6日上午11:06:24
	 */
	JSONObject getPoint(Long userId);

	/**
	 * 匹配用户
	 * 
	 * @param name
	 * @param moblie
	 * @param email
	 * @param code
	 * @return boolean
	 * @author WangBo
	 * @date 2018年7月9日下午1:52:53
	 */
	Notice matchUser(MatchUserIn in,HttpSession session);

	/**
	 * 发送验证码
	 * 
	 * @param mobile
	 *            void
	 * @author WangBo
	 * @date 2018年7月9日下午4:59:49
	 */
	void sendSMS(String mobile);
	
	/**
	 * 发送预约成功短信
	 * 
	 * @param mobile
	 *            void
	 * @author WangBo
	 * @date 2018年7月9日下午4:59:49
	 */
	void sendOrderSMS(String mobile,String name,String time,String location);
	
	/**
	 * 发送课前通知短信
	 * 
	 * @param mobile
	 *            void
	 * @author WangBo
	 * @date 2018年7月9日下午4:59:49
	 */
	void sendNoticeSMS(String mobile,String name,String time,String location);

	/**
	 * 匹配原密码
	 * 
	 * @param userId
	 * @param password
	 *            void
	 * @author WangBo
	 * @date 2018年7月9日下午6:38:53
	 */
	void matchPassword(Long userId, String password);

	/**
	 * 设置新密码
	 * 
	 * @param userId
	 * @param password
	 * @return boolean
	 * @author WangBo
	 * @date 2018年7月10日上午10:08:16
	 */
	void updatePassword(Long userId, String password);

	/**
	 * 企业下级员工账号的查
	 */
	Pagination<User> getUserMember(Long companyId, Integer pageNo, Integer pageSize);

	/**
	 * 企业会员的增
	 */
	boolean insertUserMember(Long userId,UserAddIn in);

	/**
	 * 企业下级员工账号的删
	 */
	int deleteByPrimaryKey(Long userId,Long id);

	/**
	 * 企业下级员工账号的改
	 */
	boolean updateByPrimaryKeySelective(User user);

	/**
	 * 企业下级员工账号的冻结账号
	 */
	void updateUserFrozen(Long id,Integer state);

	/**
	 * 平台会员详细列表
	 */
	List<User> getUser();

	/**
	 * 企业员工按真实姓名或手机号或者邮箱条件查询
	 * 
	 * @param name
	 * @param mobile
	 * @param email
	 * @return User
	 * @author WangBo
	 * @date 2018年7月18日上午10:41:23
	 */
	UserBackOut selectUserByOne(String name, String mobile, String email);

	/** 
	 * 企业员工按时间或状态或公司名称或职位或是否是会员条件查询
	* @param pageNo
	* @param pageSize
	* @param startTime
	* @param endTime
	* @param companyName
	* @param state
	* @param position
	* @param isMember
	* @return List<User>
	* @author WangBo
	* @date 2018年7月18日上午10:42:49
	*/ 
	List<UserBackOut> selectUserByMany(Integer pageNo, Integer pageSize, String companyName,
			Integer state, Integer position, Integer isMember,String name,String mobile,String email);
	
	/**
	 * 点击可查询当前会员的所有过往行为及积分等信息
	 */
	UserPictureOut getActionById(Long UserId);
	
	/** 
	 * 导出excel
	* @return String
	* @author WangBo
	* @date 2018年8月1日下午5:55:58
	*/ 
	String exportExcel(Long companyId);
	
	/** 
	 * 根据ID获取用户信息
	* @param id
	* @return UcOut
	* @author WangBo
	* @date 2018年8月2日下午3:39:04
	*/ 
	UcOut getUserById(Long id);
	
	/**
	 * 后台分页查询所有会员
	 */
	Pagination<UcOut> getAllUser(Integer pageNo, Integer pageSize);
	
	
	/** 
	 *获取首页信息
	* @return JSONObject
	* @author WangBo
	* @date 2018年8月2日下午3:39:57
	*/ 
	JSONObject getInfo();
	
	
	/**
	 * 根据企业筛选用户
	 */
	List<UcOut> getCompanyById(@Param("companyId") Long companyId);
	
	/** 
	 * 导入员工信息
	* @param file void
	* @author WangBo
	* @date 2018年8月23日上午11:37:14
	*/ 
	JSONObject importExcel(MultipartFile file);
	
	/** 
	 * 批量导入用户
	* @param in void
	* @author WangBo
	* @date 2018年8月23日下午3:23:48
	*/ 
	void addUserByExcel(ExcelIn in);
	
	
	/** 
	 * 删除Excel
	* @param url void
	* @author WangBo
	* @date 2018年8月27日下午1:33:59
	*/ 
	void deleteExcel(String url);
	
}
