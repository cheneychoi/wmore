package com.cheney.modules.maye.api.back;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.mid.service.LogService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value="日志")
@Controller
@RequestMapping(value="/wmore_back")
public class LogBackController extends BaseController{
	
	@Autowired
	LogService logService;
	
	/*@ApiOperation(value = "获取所有日志", notes = "用于后台获取 管理员登陆日志")
	@RequestMapping(value = "/log/getAllLog", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAllLog(@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		return success(logService.getAllLog(pageNo, pageSize));
	}*/
	@ApiOperation(value = "获取所有日志", notes = "用于后台获取 管理员登陆日志")
	@RequestMapping(value = "/log/getAllLog", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAllLog(@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		return success(logService.getLog(pageNo, pageSize));
	}
	
	
	@ApiOperation(value = "根据时间和用户查看日志", notes = "用于后台条件查询用户登陆日志")
	@RequestMapping(value = "/log/getLogByCondition", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getLogByCondition(@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize,
			@ApiParam(value = "管理员") @RequestParam(value = "name", required = false) String name,
			@ApiParam(value = "日志记录") @RequestParam(value = "msg", required = false) String msg) {
		System.out.println(name);
		System.out.println(msg);
		return success(logService.getLogByCondition(pageNo, pageSize, name,msg));
	}
	
}
