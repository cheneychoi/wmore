package com.cheney.modules.maye.db.model.in;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Admin;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("根据id修改管理员-入")
public class AdminUpdateIn {
	@ApiModelProperty(value="管理员id")
	private Long id;
	@ApiModelProperty(value="昵称")
	private String nickname;
	@ApiModelProperty(value="管理员姓名")
	private String name;
	@ApiModelProperty(value="用户名")
	private String username;
	public Admin toAdminUpdateIn() {
		Admin a = new Admin();
		BeanUtils.copyProperties(this, a);
		return a;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
}
