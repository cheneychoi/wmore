package com.cheney.modules.maye.mid.service;

import java.util.List;

import com.cheney.modules.maye.db.model.Role;
import com.cheney.modules.maye.db.model.RoleAuthority;
import com.cheney.share.db.model.Pagination;

public interface RoleService {
	/**
	 * 分页查询所有的角色
	 */
	Pagination<Role> getAllRoleByPage(Integer pageNo, Integer pageSize);
	/**
	 * 添加角色
	 */
	boolean insertRole(String name, String description,String authorityId);
	/**
	 * 编辑角色
	 */
	boolean updateRole(Long id,String name, String description,String authorityIds);
	/**
	 * 删除角色
	 */
	boolean deleteRole(Long id);
	/**
	 * 批量删除角色
	 */
	boolean deleteRoleBatch(String ids);
	
	/** 
	 * 根据id查询角色
	* @param id
	* @return Role
	* @author WangBo
	* @date 2018年8月2日上午11:40:12
	*/ 
	Role getRoleById(Long id);
	
	/** 
	 * 跟新角色权限
	* @param roleId
	* @param authorityId void
	* @author WangBo
	* @date 2018年8月2日下午7:34:03
	*/ 
	void updateAuthority(Long roleId,List<Long> authorityIds);
	 /**
     * 根据角色id获取权限列表
     */
    List<RoleAuthority> getRoleAuthority(Long roleId);
}
