package com.cheney.modules.maye.mid.service;

import com.cheney.modules.maye.db.model.NoticeMasterplate;
import com.cheney.share.db.model.Pagination;

public interface NoticeMasterplateService {
	
	/** 
	 * 添加通知模板
	* @param noticeMasterplateService void
	* @author WangBo
	* @date 2018年7月18日下午6:08:30
	*/ 
	void addNoticeMasterplate(NoticeMasterplate noticeMasterplate);
	
	
	/** 
	 * 根据ID获取模板
	* @param id
	* @return NoticeMasterplate
	* @author WangBo
	* @date 2018年7月18日下午6:15:06
	*/ 
	NoticeMasterplate getNoticeMasterplateById(Long id);
	
	/** 
	 * 获取所有的通知模板
	* @return List<NoticeMasterplate>
	* @author WangBo
	* @date 2018年7月18日下午6:15:49
	*/ 
	Pagination<NoticeMasterplate> getAllNoticeMasterplate(Integer pageNo,Integer pageSize);
	
	/** 
	 * 根据ID修改模板
	* @param id void
	* @author WangBo
	* @date 2018年7月18日下午6:16:32
	*/ 
	void updateNoticeMasterplateById(NoticeMasterplate noticeMasterplate);
	
	/** 
	 * 根据模板类型查询模板
	* @param type
	* @return List<NoticeMasterplate>
	* @author WangBo
	* @date 2018年7月18日下午6:40:13
	*/ 
	Pagination<NoticeMasterplate> selectNoticeMasterplateByType(Integer pageNo,Integer pageSize,Integer type);
	
	
	/** 
	 * 是否停用
	* @param isStop void
	* @author WangBo
	* @date 2018年7月18日下午6:41:01
	*/ 
	void isStopNoticeMasterplate(Long id,Integer isStop);
	
	
	/** 
	 * 根据模版类型获取通知模版
	* @param type
	* @return NoticeMasterplate
	* @author WangBo
	* @date 2018年7月19日下午4:10:23
	*/ 
	NoticeMasterplate getNoticeMasterplateByType(Integer type);
}
