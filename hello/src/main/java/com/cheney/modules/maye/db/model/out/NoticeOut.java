package com.cheney.modules.maye.db.model.out;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Notice;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="系统通知--出")
public class NoticeOut {
	
	@ApiModelProperty(value="通知标题")
   private String title;
	
	@ApiModelProperty(value="通知内容")
   private Long userId;
	@ApiModelProperty(value="时间")
	private Date noticeDate;
	
	public static NoticeOut from(Notice notice) {
		NoticeOut noticeOut = new NoticeOut();
        BeanUtils.copyProperties(notice, noticeOut);
        return noticeOut;
    }

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Date getNoticeDate() {
		return noticeDate;
	}

	public void setNoticeDate(Date noticeDate) {
		this.noticeDate = noticeDate;
	}
	
}
