package com.cheney.modules.maye.db.model.in;

import io.swagger.annotations.ApiModelProperty;
public class UserAddIn {

	@ApiModelProperty(value = "真实姓名")
	private String name;

	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "手机号")
	private String mobile;
	@ApiModelProperty(value = "性别 男:1 女:2")
	private Integer sex;

	@ApiModelProperty(value = "城市")
	private String city;
	
	@ApiModelProperty(value = "行业")
	private String industry;

	@ApiModelProperty(value = "所属公司")
	private String companyName;

	@ApiModelProperty(value = "职位 高管-1 普通员工-2")
	private int position;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}

	
	
}
