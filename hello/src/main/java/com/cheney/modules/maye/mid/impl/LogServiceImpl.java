package com.cheney.modules.maye.mid.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cheney.modules.maye.db.mapper.LogMapper;
import com.cheney.modules.maye.db.model.Log;
import com.cheney.modules.maye.db.model.out.LogOut;
import com.cheney.modules.maye.mid.service.LogService;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.utils.ToolUtil;

@Service
public class LogServiceImpl implements LogService {

	@Resource
	LogMapper logMapper;

	public void addLog(Long userId, String msg, Integer type) {
		Log log = new Log();
		log.setCreateDate(ToolUtil.getTime(0));
		log.setUserId(userId);
		log.setLogMsg(msg);
		log.setType(type);
		int num = logMapper.insertSelective(log);
		if (num != 1) {
			throw new HelperException(CodeMsg.ADD_LOGIN_ERROR.getCode(), CodeMsg.ADD_LOGIN_ERROR.getMsg());
		}
	}

	@Override
	public Pagination<LogOut> getLog(Integer pageNo, Integer pageSize) {
		Pagination<LogOut> page = new Pagination<>(pageNo, pageSize);
		pageNo = (pageNo - 1) * pageSize;
		List<LogOut> list = logMapper.getLog(pageNo, pageSize);
		// LogExample le = new LogExample();
		int count = logMapper.count();
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public Pagination<LogOut> getLogByCondition(Integer pageNo, Integer pageSize, String name, String msg) {
		Pagination<LogOut> page = new Pagination<>(pageNo, pageSize);
		pageNo = (pageNo - 1) * pageSize;
		List<LogOut> list = logMapper.getLogByCondition(pageNo, pageSize,name,msg);
		int count = logMapper.countByCondition(name,msg);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

}
