package com.cheney.modules.maye.api.front;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.db.model.Notice;
import com.cheney.modules.maye.db.model.in.EvaluateIn;
import com.cheney.modules.maye.db.model.in.UserCourseIn;
import com.cheney.modules.maye.db.model.out.EvaluateOut;
import com.cheney.modules.maye.mid.service.UserCourseService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/wmore")
public class UserCourseController extends BaseController {
	
	Logger logger = LoggerFactory.getLogger("login");
	
	@Autowired
	private UserCourseService userCourseService;

	@RequestMapping(value = "/userCourse/insertUC", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	@ApiOperation(value = "选择课程，确认预约，产生预约订单", notes = "添加数据", response = UserCourseIn.class)
	public Map<String, Object> insertUserCourse(@RequestBody UserCourseIn in) {
		logger.info("-----------进入选择课程，确认预约，产生预约订单的方法---------------");
		logger.info("传入参数----------------" + in.toString());
		Notice notice = userCourseService.insertUserCourse(in.toUserCourseIn());
		logger.info("返回的数据----------------" + notice);
		return success(notice);
	}
	
	
	@RequestMapping(value = "/userCourse/setScore", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	@ApiOperation(value = "课程评分", notes = "员工评价课程")
	public Map<String, Object> setScore(@RequestBody EvaluateIn in) {
		logger.info("-----------进入课程评分的方法---------------");
		logger.info("传入参数----------------" + in.toString());
		Notice notice = userCourseService.setScore(in);
		logger.info("返回的数据----------------" + notice);
		return success(notice);
	}
	
	@RequestMapping(value = "/userCourse/getIsEvaluateCourse", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	@ApiOperation(value = "获取未评价课程", notes = "获取未评价课程")
	public Map<String, Object> getIsEvaluateCourse(@ApiParam(value = "用户Id") @RequestParam(name="userId",required=false) Long userId
			) {
		logger.info("-----------进入获取未评价课程的方法---------------");
		logger.info("userId----------------" + userId);
		List<EvaluateOut> evaluateOuts = userCourseService.getIsEvaluateCourse(userId);
		logger.info("返回的数据----------------" + evaluateOuts);
		return success(evaluateOuts);
	}
	
	

}