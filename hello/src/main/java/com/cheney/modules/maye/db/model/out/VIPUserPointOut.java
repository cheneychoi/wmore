package com.cheney.modules.maye.db.model.out;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.User;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "VIP用户积分-出")
public class VIPUserPointOut {

	@ApiModelProperty(value = "用户id")
	private Long userId;

	@ApiModelProperty(value = "用户姓名")
	private String name;
	
	@ApiModelProperty(value = "积分")
	private int point;
	
	public static VIPUserPointOut from(User user) {
		VIPUserPointOut upo = new VIPUserPointOut();
		BeanUtils.copyProperties(user, upo);
		return upo;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}
}