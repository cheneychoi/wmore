package com.cheney.modules.maye.db.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Picture;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="BANNER的图片更换")
public class PictureUpdateIn {
	@ApiModelProperty(value="图片的ID")
	private Long id;
	@ApiModelProperty(value="修改日期")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	private Date updateDate;
/*	@ApiModelProperty(value="日期")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	private Date createDate;*/
	public Picture toPictureUpdateIn() {
		Picture picture = new Picture();
		BeanUtils.copyProperties(this, picture);
		return picture;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Date getUpdateDate() {
		return updateDate;
	}
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}
	
}