package com.cheney.modules.maye.mid.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.cheney.modules.maye.db.mapper.ConfigMapper;
import com.cheney.modules.maye.db.model.Config;
import com.cheney.modules.maye.db.model.ConfigExample;
import com.cheney.modules.maye.mid.service.ConfigService;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.prop.AliDayuProperties;
import com.cheney.share.mid.prop.AliOSSProperties;
import com.cheney.share.mid.redis.OssKey;
import com.cheney.share.mid.redis.RedisService;
import com.cheney.share.mid.redis.SmsKey;

@Service
public class ConfigServiceImpl implements ConfigService {

	@Resource
	ConfigMapper configMapper;

	@Resource
	RedisService redisService;

	/*
	 * 短信服务器配置
	 */
	public void updateSMSConfig(Long id,Integer type,String appid,String appkey,String gateway,String bucket) {
		Config c = new Config();
		c.setId(id);
		c.setType(type);
		c.setAppid(appid);
		c.setAppkey(appkey);
		c.setGateway(gateway);
		c.setBucket(bucket);
		int num = configMapper.updateByPrimaryKeySelective(c);
		if (num != 1) {
			throw new HelperException(CodeMsg.SMS_CONFIG_ERROR.getCode(), CodeMsg.SMS_CONFIG_ERROR.getMsg());
		}
		 c = configMapper.selectByPrimaryKey(c.getId());
		redisService.set(SmsKey.withExpire(0), "", JSON.toJSONString(c));
	}

	/*
	 * oss服务器配置
	 */
	public void updateOSSConfig(Long id, Integer type,String appid,String appkey,String sign,String product,String phoneRegisterTmid,String phoneBindTmid) {
		Config c = new Config();
		c.setId(id);
		c.setType(type);
		c.setAppid(appid);
		c.setAppkey(appkey);
		c.setSign(sign);
		c.setProduct(product);
		c.setPhoneBindTmid(phoneBindTmid);
		c.setPhoneRegisterTmid(phoneRegisterTmid);
		int num = configMapper.updateByPrimaryKeySelective(c);
		if (num != 1) {
			throw new HelperException(CodeMsg.OSS_CONFIG_ERROR.getCode(), CodeMsg.OSS_CONFIG_ERROR.getMsg());
		}
		 c = configMapper.selectByPrimaryKey(c.getId());
		redisService.set(OssKey.withExpire(0), "", JSON.toJSONString(c));
	}

	@Override
	public Config getConfig(Long id) {
		return configMapper.selectByPrimaryKey(id);
	}

	/**
	 * 获取短信服务器信息
	 * 
	 * @return AliDayuProperties
	 * @author WangBo
	 * @date 2018年7月12日下午2:45:41
	 */
	public AliDayuProperties getSMSProps() {
		AliDayuProperties props = new AliDayuProperties();
		String prop = redisService.get(SmsKey.withExpire(0), "", String.class);
		Config config = null;
		if (prop == null) {
			ConfigExample ce = new ConfigExample();
			ce.or().andTypeEqualTo(1);
			config = configMapper.selectByExample(ce).get(0);
			redisService.set(SmsKey.withExpire(0), "", JSON.toJSONString(config));
		} else {
			config = JSON.parseObject(prop, Config.class);
		}
		if (config == null) {
			throw new HelperException(CodeMsg.GET_SMS_ERROR.getCode(), CodeMsg.GET_OSS_ERROR.getMsg());
		}
		props.setAppId(config.getAppid());
		props.setAppKey(config.getAppkey());
		props.setSign(config.getSign());
		props.setProduct(config.getProduct());
		props.setPhoneBindTMID(config.getPhoneBindTmid());
		props.setPhoneRegisterTMID(config.getPhoneRegisterTmid());
		return props;
	};

	/**
	 * 获取OSS服务器信息
	 * 
	 * @return AliDayuProperties
	 * @author WangBo
	 * @date 2018年7月12日下午2:45:55
	 */
	public AliOSSProperties getOSSProps() {
		AliOSSProperties props = new AliOSSProperties();
		String prop = redisService.get(OssKey.withExpire(0), "", String.class);
		Config config = null;
		if (prop == null) {
			ConfigExample ce = new ConfigExample();
			ce.or().andTypeEqualTo(2);
			config = configMapper.selectByExample(ce).get(0);
			redisService.set(OssKey.withExpire(0), "", JSON.toJSONString(config));
		} else {
			config = JSON.parseObject(prop, Config.class);
		}
		if (config == null) {
			throw new HelperException(CodeMsg.GET_OSS_ERROR.getCode(), CodeMsg.GET_OSS_ERROR.getMsg());
		}
		props.setAppId(config.getAppid());
		props.setAppKey(config.getAppkey());
		props.setBucket(config.getBucket());
		props.setGateway(config.getGateway());
		return props;
	};

}
