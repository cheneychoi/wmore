package com.cheney.modules.maye.db.model.out;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "课程-出")
public class CourseBackOut {
	
	@ApiModelProperty(value = "课程id")
	private String courseId;

	@ApiModelProperty(value = "课程名称")
	private String courseName;
	
	@ApiModelProperty(value = "课程名称英语")
	private String courseNameEN;

	@ApiModelProperty(value = "课程地点")
	private String coursePlace;

	@ApiModelProperty(value = "课程时间")
	private Date courseDate;
	
	@ApiModelProperty(value = "课程二维码")
	private String codeUrl;
	
	@ApiModelProperty(value = "课程权限 1-高管 2-员工 3-所有")
	private Integer courseAuthority;

	@ApiModelProperty(value = "课程类型 1-员工课 2-户外课")
	 private Integer type;
	
	@ApiModelProperty(value = "创建时间")
	 private Date createDate;

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseNameEN() {
		return courseNameEN;
	}

	public void setCourseNameEN(String courseNameEN) {
		this.courseNameEN = courseNameEN;
	}

	public String getCoursePlace() {
		return coursePlace;
	}

	public void setCoursePlace(String coursePlace) {
		this.coursePlace = coursePlace;
	}

	public Date getCourseDate() {
		return courseDate;
	}

	public void setCourseDate(Date courseDate) {
		this.courseDate = courseDate;
	}

	public String getCodeUrl() {
		return codeUrl;
	}

	public void setCodeUrl(String codeUrl) {
		this.codeUrl = codeUrl;
	}

	public Integer getCourseAuthority() {
		return courseAuthority;
	}

	public void setCourseAuthority(Integer courseAuthority) {
		this.courseAuthority = courseAuthority;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	

	
}