package com.cheney.modules.maye.db.model.out;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Config;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "OSS服务器配置-出")
public class SMSConfigOut {
	
	@ApiModelProperty(value = "id")
	private Long id;

	@ApiModelProperty(value = "配置类型(1-短信接口配置 2-oss服务器配置)")
	private Integer type;

	@ApiModelProperty(value = "appId")
	private String appid;

	@ApiModelProperty(value = "appKey")
	private String appkey;

	@ApiModelProperty(value = "oss服务器地址")
	private String gateway;

	@ApiModelProperty(value = "oss服务器bucket")
	private String bucket;

	public static SMSConfigOut from(Config config) {
		SMSConfigOut out = new SMSConfigOut();
		BeanUtils.copyProperties(config, out);
		return out;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getAppkey() {
		return appkey;
	}

	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}

	public String getGateway() {
		return gateway;
	}

	public void setGateway(String gateway) {
		this.gateway = gateway;
	}

	public String getBucket() {
		return bucket;
	}

	public void setBucket(String bucket) {
		this.bucket = bucket;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	

}