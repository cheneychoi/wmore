package com.cheney.modules.maye.mid.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cheney.modules.maye.db.mapper.AuthorityMapper;
import com.cheney.modules.maye.db.model.Authority;
import com.cheney.modules.maye.db.model.AuthorityExample;
import com.cheney.modules.maye.db.model.AuthorityExample.Criteria;
import com.cheney.modules.maye.mid.service.AuthorityService;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.utils.ToolUtil;
@Service
public class AuthorityServiceImpl implements AuthorityService{
	
	@Autowired
	AuthorityMapper authorityMapper;
	
	@Override
	public boolean addAuthority(String url,String menuClass,String menuName,String parentMenucode,Integer menuType) {
		Authority a = new Authority();
		a.setCreateTime(ToolUtil.getTime(0));
		a.setUrl(url);
		a.setMenuClass(menuClass);
		a.setMenuName(menuName);
		a.setParentMenucode(parentMenucode);
		a.setMenuType(menuType);
		int num=authorityMapper.insertSelective(a);
		if(num==0) {
			throw new HelperException(CodeMsg.ADD_AUTHORITY_ERROR.getCode(), CodeMsg.ADD_AUTHORITY_ERROR.getMsg());
		}
		return num >=1 ? true : false;
	}

	@Override
	public Authority getAuthorityById(Long id) {
		return authorityMapper.selectByPrimaryKey(id);
	}

	@Override
	public boolean updateAuthority(Long id,String url,String menuClass,String menuName,String parentMenucode,Integer menuType) {
		Authority a = new Authority();
		a.setId(id);
		a.setUrl(url);
		a.setMenuClass(menuClass);
		a.setMenuName(menuName);
		a.setParentMenucode(parentMenucode);
		a.setMenuType(menuType);
		int num=authorityMapper.updateByPrimaryKeySelective(a);
		if(num==0) {
			throw new HelperException(CodeMsg.UPDATE_AUTHORITY_ERROR.getCode(), CodeMsg.UPDATE_AUTHORITY_ERROR.getMsg());
		}
		return num >=1 ? true : false;
	}


	@Override
	public Pagination<Authority> getAuthorityByPage(Integer pageNo, Integer pageSize) {
		Pagination<Authority> page=new Pagination<>(pageNo, pageSize);
		AuthorityExample ax = new AuthorityExample();
		List<Authority> list = authorityMapper.selectByExample(ax);
		int count = (int) authorityMapper.countByExample(ax);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public boolean deleteAuthority(Long id) {
		int num=authorityMapper.deleteByPrimaryKey(id);
		if(num == 0) {
			throw new HelperException(CodeMsg.DELETE_AUTHORITY_ERROR.getCode(), CodeMsg.DELETE_AUTHORITY_ERROR.getMsg());
		}
		return num == 1 ? true : false;
	}
	@Override
	public boolean deleteAuthorityBatch(String ids) {
		AuthorityExample example = new AuthorityExample();
		Criteria criteria = example.createCriteria();
		criteria.andIdIn(ToolUtil.toList(ids));
		int num=authorityMapper.deleteByExample(example);
		return num>=1 ? true : false;
	}

}
