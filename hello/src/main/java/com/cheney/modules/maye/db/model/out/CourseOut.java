package com.cheney.modules.maye.db.model.out;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Course;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "课程-出")
public class CourseOut {
	
	@ApiModelProperty(value = "课程id")
	private String courseId;

	@ApiModelProperty(value = "课程名称")
	private String courseName;
	
	@ApiModelProperty(value = "课程名称英语")
	private String courseNameEN;

	@ApiModelProperty(value = "课程地点")
	private String coursePlace;

	@ApiModelProperty(value = "课程时间")
	private Date courseDate;

	@ApiModelProperty(value = "课程类型 1-员工课 2-户外课")
	 private Integer type;

	public static CourseOut from(Course course) {
		CourseOut uco = new CourseOut();
        BeanUtils.copyProperties(course, uco);
        return uco;
    }

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseNameEN() {
		return courseNameEN;
	}

	public void setCourseNameEN(String courseNameEN) {
		this.courseNameEN = courseNameEN;
	}

	public String getCoursePlace() {
		return coursePlace;
	}

	public void setCoursePlace(String coursePlace) {
		this.coursePlace = coursePlace;
	}

	public Date getCourseDate() {
		return courseDate;
	}

	public void setCourseDate(Date courseDate) {
		this.courseDate = courseDate;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getCourseId() {
		return courseId;
	}

	public void setCourseId(String courseId) {
		this.courseId = courseId;
	}

	
}