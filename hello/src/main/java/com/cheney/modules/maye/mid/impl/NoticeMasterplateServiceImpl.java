package com.cheney.modules.maye.mid.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.cheney.modules.maye.db.mapper.NoticeMasterplateMapper;
import com.cheney.modules.maye.db.model.NoticeMasterplate;
import com.cheney.modules.maye.db.model.NoticeMasterplateExample;
import com.cheney.modules.maye.mid.service.NoticeMasterplateService;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.utils.ToolUtil;

@Service
public class NoticeMasterplateServiceImpl implements NoticeMasterplateService {

	@Resource
	NoticeMasterplateMapper noticeMasterplateMapper;

	@Override
	public void addNoticeMasterplate(NoticeMasterplate noticeMasterplate) {
		noticeMasterplate.setCreateDate(ToolUtil.getTime(0));
		int num = noticeMasterplateMapper.insert(noticeMasterplate);
		if (num == 0) {
			throw new HelperException(CodeMsg.ADD_NOTICEMASTERPLATE_ERROR.getCode(),
					CodeMsg.ADD_NOTICEMASTERPLATE_ERROR.getMsg());
		}
	}

	@Override
	public NoticeMasterplate getNoticeMasterplateById(Long id) {
		return noticeMasterplateMapper.selectByPrimaryKey(id);
	}

	@Override
	public Pagination<NoticeMasterplate> getAllNoticeMasterplate(Integer pageNo, Integer pageSize) {
		Pagination<NoticeMasterplate> page = new Pagination<>(pageNo, pageSize);
		NoticeMasterplateExample nme = new NoticeMasterplateExample();
		nme.createCriteria();
		List<NoticeMasterplate> list = noticeMasterplateMapper.selectByExampleWithBLOBs(nme);
		int count = (int) noticeMasterplateMapper.countByExample(nme);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public void updateNoticeMasterplateById(NoticeMasterplate noticeMasterplate) {
		noticeMasterplateMapper.updateByPrimaryKeySelective(noticeMasterplate);
	}

	@Override
	public Pagination<NoticeMasterplate> selectNoticeMasterplateByType(Integer pageNo, Integer pageSize, Integer type) {
		Pagination<NoticeMasterplate> page = new Pagination<>(pageNo, pageSize);
		NoticeMasterplateExample nme = new NoticeMasterplateExample();
		nme.createCriteria().andTypeEqualTo(type);
		List<NoticeMasterplate> list = noticeMasterplateMapper.selectByExample(nme);
		int count = (int) noticeMasterplateMapper.countByExample(nme);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public void isStopNoticeMasterplate(Long id, Integer isStop) {
		NoticeMasterplate noticeMasterplate = new NoticeMasterplate();
		noticeMasterplate.setId(id);
		noticeMasterplate.setIsStop(isStop);
		noticeMasterplateMapper.updateByPrimaryKeySelective(noticeMasterplate);
	}

	@Override
	public NoticeMasterplate getNoticeMasterplateByType(Integer type) {
		NoticeMasterplateExample noticeMasterplateExample = new NoticeMasterplateExample();
		noticeMasterplateExample.or().andTypeEqualTo(type).andIsStopEqualTo(2);
		List<NoticeMasterplate> list = noticeMasterplateMapper.selectByExampleWithBLOBs(noticeMasterplateExample);
		if (list.size()>1) {
			throw new HelperException(CodeMsg.NOTICE_ERROR.getCode(), CodeMsg.NOTICE_ERROR.getMsg());
		}else if (list.size()==0) {
			throw new HelperException(CodeMsg.NO_NOTICE_ERROR.getCode(), CodeMsg.NO_NOTICE_ERROR.getMsg());
		}
		return list.get(0);
	}

}
