package com.cheney.modules.maye.db.model.out;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "用户行为--出")
public class ActionOut {
	@ApiModelProperty(value = "行为的id")
	private Long actionId;

	@ApiModelProperty(value = "用户的id")
	private Long userId;

	@ApiModelProperty(value = "用户姓名")
	private String name;

	@ApiModelProperty(value = "用户总积分")
	private Integer points;
	
	@ApiModelProperty(value = "剩余积分")
	private Integer actionPoint;

	@ApiModelProperty(value = "行为内容")
	private String actionInformation;

	@ApiModelProperty(value = "行为类型 1-动作日志 2-积分变化日志")
	private Integer actionType;

	@ApiModelProperty(value = "创建时间")
	private Date createDate;

	public Long getActionId() {
		return actionId;
	}

	public void setActionId(Long actionId) {
		this.actionId = actionId;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	public String getActionInformation() {
		return actionInformation;
	}

	public void setActionInformation(String actionInformation) {
		this.actionInformation = actionInformation;
	}

	public Integer getActionType() {
		return actionType;
	}

	public void setActionType(Integer actionType) {
		this.actionType = actionType;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Integer getActionPoint() {
		return actionPoint;
	}

	public void setActionPoint(Integer actionPoint) {
		this.actionPoint = actionPoint;
	}

	
}
