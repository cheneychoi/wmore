package com.cheney.modules.maye.db.model.out;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Config;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "短信配置-出")
public class OSSConfigOut {

	@ApiModelProperty(value = "id")
	private Long id;

	@ApiModelProperty(value = "配置类型(1-短信接口配置 2-oss服务器配置)")
	private Integer type;

	@ApiModelProperty(value = "appId")
	private String appid;

	@ApiModelProperty(value = "appKey")
	private String appkey;

	@ApiModelProperty(value = "短信接口签名")
	private String sign;

	@ApiModelProperty(value = "短信接口product")
	private String product;

	@ApiModelProperty(value = "注册短信模板")
	private String phoneRegisterTmid;

	@ApiModelProperty(value = "验证码短信模板")
	private String phoneBindTmid;

	public static OSSConfigOut from(Config config) {
		OSSConfigOut out = new OSSConfigOut();
		BeanUtils.copyProperties(config, out);
		return out;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getAppid() {
		return appid;
	}

	public void setAppid(String appid) {
		this.appid = appid;
	}

	public String getAppkey() {
		return appkey;
	}

	public void setAppkey(String appkey) {
		this.appkey = appkey;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getPhoneRegisterTmid() {
		return phoneRegisterTmid;
	}

	public void setPhoneRegisterTmid(String phoneRegisterTmid) {
		this.phoneRegisterTmid = phoneRegisterTmid;
	}

	public String getPhoneBindTmid() {
		return phoneBindTmid;
	}

	public void setPhoneBindTmid(String phoneBindTmid) {
		this.phoneBindTmid = phoneBindTmid;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}