package com.cheney.modules.maye.mid.service;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.cheney.modules.maye.db.model.Course;
import com.cheney.modules.maye.db.model.out.CourseBackOut;
import com.cheney.modules.maye.db.model.out.CourseDetailOut;
import com.cheney.modules.maye.db.model.out.CourseUserOut;
import com.cheney.modules.maye.db.model.out.MakeCourseOut;
import com.cheney.modules.maye.db.model.out.SignOut;
import com.cheney.share.db.model.Pagination;

/**
 * 课程模块
 * 
 * @ClassName: CourseService
 * @Description: TODO
 * @author: WangBo
 * @date: 2018年7月6日 上午11:14:21
 */
public interface CourseService {

	/**
	 * 新增课程
	 * 
	 * @return boolean
	 * @author WangBo
	 * @date 2018年7月6日上午11:15:26
	 */
	void addCourse(Long userId,String courseName, String courseNameEN, String coursePlace, Date courseDate, Integer task,
			Integer courseNum, String introduction, String crowd, String trainingEffect, String trainingPreparation,
			String otherBusiness, Integer courseAuthority, Integer type, List<MultipartFile> files, String companyName);

	/**
	 * 查可约课列表
	 */
	List<CourseUserOut> getCourseByCourseAuthority(Long id, String month);

	/**
	 * 后台查当日预约课程详情
	 */
	Pagination<MakeCourseOut> getCourseByToDay(Integer pageNo, Integer pageSize);

	/**
	 * 后台查看当日会员取消课程列表
	 */
	Pagination<Course> getCourseByCancel(Integer pageNo, Integer pageSize);

	/**
	 * 一周预约详情
	 */
	Pagination<MakeCourseOut> getCourseByWeek(Integer pageNo, Integer pageSize);

	/**
	 * 生成课程二维码
	 * 
	 * @param courseId
	 * @return String
	 * @author WangBo
	 * @throws Exception
	 * @date 2018年7月13日上午11:00:37
	 */
	String createQRCode(Long courseId);

	/**
	 * 查询所有课程
	 * 
	 * @return List<Course>
	 * @author WangBo
	 * @date 2018年7月18日下午12:09:18
	 */
	Pagination<Course> getAllCourses(Integer pageNo, Integer pageSize);

	/**
	 * 根据课程名称或者日期查询课程
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @param startTime
	 * @param endTime
	 * @param courseName
	 * @return List<CourseBackOut>
	 * @author WangBo
	 * @date 2018年7月18日下午12:17:04
	 */
	List<CourseBackOut> getCourseByCondition(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize,
			@Param("startTime") String startTime, @Param("endTime") String endTime,
			@Param("courseName") String courseName);

	/**
	 * 后台模糊查询课程名称
	 */
	Pagination<Course> getCourseByFuzzy(String courseName, String companyName, Integer pageNo, Integer pageSize);

	/**
	 * 后台根据ID查课程
	 */
	JSONObject getCourseById(Long id);

	/**
	 * 后台根据名称查课程
	 */
	Pagination<MakeCourseOut> getCourseByName(@Param("pageNo") Integer pageNo, @Param("pageSize") Integer pageSize,String courseName,String startTime,String endTime);

	/**
	 * 后台根据id编辑课程
	 */
	void updateCourse(Long id, String courseName, String courseNameEN, String coursePlace, Date courseDate,Integer task,
			Integer courseNum, String introduction, String crowd, String trainingEffect, String trainingPreparation,
			String otherBusiness, Integer courseAuthority, Integer type, List<MultipartFile> files, String companyName);

	/**
	 * 查询图文详情和课程预约状态
	 */
	CourseDetailOut getCourseDetail(Long userId, Long courseId);

	/**
	 * 获取签到课程信息
	 * 
	 * @param courseId
	 * @return SignOut
	 * @author WangBo
	 * @date 2018年8月9日上午9:36:52
	 */
	SignOut getSignInfo(Long courseId);

	/**
	 * 删除课程
	 */
	boolean delCourse(Long userId,Long id);

	/**
	 * 一周的模糊
	 */
	List<MakeCourseOut> getCourseByName1(String courseName,String startTime,String endTime);

	/**
	 * 搜索企业筛选
	 */
	List<MakeCourseOut> getCompanyByName(@Param("companyId") Long companyId);

	/**
	 * 一周预约按企业筛选
	 */
	List<MakeCourseOut> getCompanyByName1(@Param("companyId") Long companyId);

	/**
	 * 课程列表根据企业筛选
	 */
	List<Course> getCourseByCompany(@Param("companyId") Long companyId);
	

	/** 
	 * 导出Excel
	* @param courseName
	* @param startTime
	* @param endTime
	* @param companyId
	* @return String
	* @author WangBo
	* @date 2018年8月27日下午4:47:27
	*/ 
	String exportCourse(String courseName, String startTime, String endTime, Long companyId);
}
