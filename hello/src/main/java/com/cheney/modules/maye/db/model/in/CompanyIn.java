package com.cheney.modules.maye.db.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Company;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("添加企业--入")
public class CompanyIn {
	@ApiModelProperty(value = "公司名称")
   private String companyName;
	@ApiModelProperty(value = "公司代号")
   private String companyCode;
	@ApiModelProperty(value = "公司人数")
   private Integer companyNumber;
	@ApiModelProperty(value = "企业状态：1：冻结，2：未冻结")
   private Integer companyState;
	@ApiModelProperty(value="创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date createDate;
	public Company toCompanyIn() {
		Company c= new Company();
		BeanUtils.copyProperties(this, c);
		return c;
	}
	public String getCompanyName() {
		return companyName;
	}
	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}
	public String getCompanyCode() {
		return companyCode;
	}
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	public Integer getCompanyNumber() {
		return companyNumber;
	}
	public void setCompanyNumber(Integer companyNumber) {
		this.companyNumber = companyNumber;
	}
	public Integer getCompanyState() {
		return companyState;
	}
	public void setCompanyState(Integer companyState) {
		this.companyState = companyState;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}
