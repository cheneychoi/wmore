package com.cheney.modules.maye.db.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.UserCourseWithBLOBs;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "预约课程-入")
public class UserCourseIn {
	@ApiModelProperty(value = "用户id")
	private Long userId;
	@ApiModelProperty(value = "课程的id")
	private Long courseId;
	@ApiModelProperty(value = "预约的时间")
	private Date courseDate;
	/*
	 * @ApiModelProperty(value = "预约的状态 1-已预约 2-已完成 3-已取消") private Integer state;
	 * 
	 * @ApiModelProperty(value = "签到状态 1-已签到 2-未签到") private Integer sign;
	 */
	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date createDate;

	public UserCourseWithBLOBs toUserCourseIn() {
		UserCourseWithBLOBs ucb = new UserCourseWithBLOBs();
		BeanUtils.copyProperties(this, ucb);
		return ucb;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public Date getCourseDate() {
		return courseDate;
	}

	public void setCourseDate(Date courseDate) {
		this.courseDate = courseDate;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	@Override
	public String toString() {
		return "UserCourseIn [userId=" + userId + ", courseId=" + courseId + ", courseDate=" + courseDate
				+ ", createDate=" + createDate + "]";
	}

}