package com.cheney.modules.maye.db.model.out;

import java.util.Date;

public class AdminOut {
	private Long id;
	private String nickname;
	private String name;
	private String username;
	private String roleName;
	private Date createDate;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getRoleName() {
		return roleName;
	}
	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	@Override
	public String toString() {
		return "AdminOut [id=" + id + ", nickname=" + nickname + ", name=" + name + ", username=" + username
				+ ", roleName=" + roleName + ", createDate=" + createDate + "]";
	}
	
}
