package com.cheney.modules.maye.api.front;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.alibaba.fastjson.JSONObject;
import com.cheney.modules.maye.db.model.in.MatchUserIn;
import com.cheney.modules.maye.db.model.in.UserInfoIn;
import com.cheney.modules.maye.db.model.out.ActionOut;
import com.cheney.modules.maye.db.model.out.CourseOut;
import com.cheney.modules.maye.db.model.out.UserInfoOut;
import com.cheney.modules.maye.db.model.out.UserPictureOut;
import com.cheney.modules.maye.mid.service.UserService;
import com.cheney.share.api.controller.BaseController;
import com.cheney.share.mid.prop.WeChatProperties;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "用户")
@Controller
@RequestMapping("/wmore")
public class UserController extends BaseController {

	Logger logger = LoggerFactory.getLogger("user");

	@InitBinder
	public void init(WebDataBinder binder) {
		binder.registerCustomEditor(Date.class, new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd"), true));
	}

	@Autowired
	UserService userService;

	@Resource
	WeChatProperties props;

	@ApiOperation(value = "获取用户信息", notes = "返回用户信息", response = UserInfoOut.class)
	@RequestMapping(value = "/user/getUserById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getUserById(@ApiParam(value = "用户Id") @RequestParam("userId") Long userId) {
		logger.info("-----------进入获取用户信息的方法---------------");
		logger.info("userid----------------" + userId);
		UserInfoOut userInfoOut = userService.getUserInfo(userId);
		logger.info("返回的数据----------------" + userInfoOut);
		return success(userInfoOut);
	}

	@ApiOperation(value = "匹配用户", notes = "用于登陆界面上匹配用户以及其公司")
	@RequestMapping(value = "/user/matchUser", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> matchUser(@RequestBody MatchUserIn in,HttpSession session) {
		logger.info("--------------进入匹配用户的方法-------------");
		return success(userService.matchUser(in,session));
	}

	@ApiOperation(value = "发送验证码", notes = "用于验证手机以及修改密码")
	@RequestMapping(value = "/user/sendSMS", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> sendSMS(@ApiParam(value = "手机号") @RequestParam("mobile") String mobile) {
		userService.sendSMS(mobile);
		return success();
	}

	@ApiOperation(value = "匹配原密码", notes = "用于修改密码时先输入原密码 若为找回密码则直接调用修改密码接口")
	@RequestMapping(value = "/user/matchPassword", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> matchPassword(@ApiParam(value = "用户的Id") @RequestParam("userId") Long userId,
			@ApiParam(value = "原密码") @RequestParam("password") String password) {
		userService.matchPassword(userId, password);
		return success();
	}

	@ApiOperation(value = "设置新密码", notes = "用于修改密码或者忘记密码时设置新密码")
	@RequestMapping(value = "/user/updatePassword", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updatePassword(@ApiParam(value = "用户的Id") @RequestParam("userId") Long userId,
			@ApiParam(value = "新密码") @RequestParam("password") String password) {
		userService.updatePassword(userId, password);
		return success();
	}

	@ApiOperation(value = "完善用户信息", notes = "编辑个人中心信息")
	@RequestMapping(value = "/user/editUser", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> editUser(@RequestBody UserInfoIn in) {
		logger.info("-----------进入完善用户信息的方法---------------");
		logger.info("生日"+in.getBirthday());
		userService.editUserInfo(in.toUser());
		return success();
	}

	@ApiOperation(value = "上传头像", notes = "上传头像")
	@RequestMapping(value = "/user/uploadHead", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> uploadHead(@ApiParam(value = "微信图片ID") @RequestParam(value = "mediaId") String mediaId,
			@ApiParam(value = "id") @RequestParam(value = "pictureId") Long pictureId) {
		logger.info("--------------进入上传头像的方法-------------");
		logger.info("上传头像--------------"+mediaId);
		logger.info("pictureId-----------------"+pictureId);
		String accessUrl = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid="
				+ props.getAppId() + "&secret=" + props.getAppSecret();
		return success(userService.updateHead(accessUrl, mediaId, pictureId));
	}

	@ApiOperation(value = "获取已预约课程列表", notes = "", response = CourseOut.class)
	@RequestMapping(value = "/user/getReservedCourse", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getReservedCourse(@ApiParam(value = "用户的Id") @RequestParam("userId") Long userId) {
		return success(userService.getReservedCourse(userId));
	}

	@ApiOperation(value = "获取已完成课程列表", notes = "", response = CourseOut.class)
	@RequestMapping(value = "/user/getFinishedCourse", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getFinishedCourse(@ApiParam(value = "用户的Id") @RequestParam("userId") Long userId) {
		return success(userService.getFinishedCourse(userId));
	}

	@ApiOperation(value = "获取用户积分和积分列表", notes = "用于查询用户的积分和积分的获得情况")
	@RequestMapping(value = "/user/getPoint", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getPoint(@ApiParam(value = "用户的Id") @RequestParam("userId") Long userId) {
		logger.info("-----------进入获取用户积分和积分列表的方法---------------");
		logger.info("userid----------------" + userId);
		JSONObject jsonObject = userService.getPoint(userId);
		logger.info("返回的数据----------------" + jsonObject);
		return success(jsonObject);
	}

	@ApiOperation(value = "查询当前会员的所有过往行为及积分等信息 ", notes = "返回数据", response = ActionOut.class)
	@RequestMapping(value = "/user/getActionById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getActionById(@ApiParam(value = "用户id") @RequestParam("id") Long userId) {
		logger.info("-----------进入获取用户信息的方法---------------");
		logger.info("userid----------------" + userId);
		UserPictureOut userPictureOut = userService.getActionById(userId);
		logger.info("返回的数据----------------" + userPictureOut);
		return success(userPictureOut);
	}
	
}
