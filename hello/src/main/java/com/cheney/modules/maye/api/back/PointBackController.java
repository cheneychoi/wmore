package com.cheney.modules.maye.api.back;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.mid.service.PointService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "积分")
@Controller
@RequestMapping("/wmore_back")
public class PointBackController extends BaseController {

	@Autowired
	PointService pointService;

	@ApiOperation(value = "查询所有积分规则 ", notes = "返回数据")
	@RequestMapping(value = "/point/getAllPoints", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAllPoints(@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		return success(pointService.getAllPoints(pageNo,pageSize));
	}
	
	@ApiOperation(value = "根据ID查询积分规则 ", notes = "返回数据")
	@RequestMapping(value = "/point/getPointById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getPointById(@ApiParam(value = "积分规则的ID") @RequestParam("id") Long pointId) {
		return success(pointService.getPointById(pointId));
	}
	
	@ApiOperation(value = "根据时间查询积分规则 ", notes = "返回数据")
	@RequestMapping(value = "/point/getPointByCondition", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getPointByCondition(@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize,
			@ApiParam(value = "起始时间") @RequestParam(value = "startTime", required = false) String startTime,
			@ApiParam(value = "结束时间") @RequestParam(value = "endTime", required = false) String endTime) {
		return success();
	}
	
	@ApiOperation(value = "是否启用积分规则 ", notes = "返回数据")
	@RequestMapping(value = "/point/isStartPoint", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> isStartPoint(@ApiParam(value = "id") @RequestParam(value = "id") Long id,
			@ApiParam(value = "是否启用积分规则") @RequestParam(value = "isStart") Integer isStart) {
		pointService.isStart(id, isStart);
		return success();
	}
	
	@ApiOperation(value = "添加积分规则 ", notes = "返回数据")
	@RequestMapping(value = "/point/addPointRule", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> addPointRule(@ApiParam(value = "积分名称") @RequestParam(value = "name") String name,
			@ApiParam(value = "积分介绍") @RequestParam(value = "introduce") String introduce,
			@ApiParam(value = "积分数目") @RequestParam(value = "num") Integer num,
			@ApiParam(value = "积分类型") @RequestParam(value = "type") Integer type) {
		pointService.addPointRule(name, introduce, num, type);
		return success();
	}
	
	
	@ApiOperation(value = "更新积分规则 ", notes = "返回数据")
	@RequestMapping(value = "/point/updatePointRule",method=RequestMethod.POST)
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updatePointRule(
			@ApiParam(value = "积分id") @RequestParam(value = "id") Long id,
			@ApiParam(value = "积分名称") @RequestParam(value = "name") String name,
			@ApiParam(value = "积分介绍") @RequestParam(value = "introduce") String introduce,
			@ApiParam(value = "积分数目") @RequestParam(value = "num") Integer num,
			@ApiParam(value = "积分类型") @RequestParam(value = "type") Integer type,
			@ApiParam(value = "积分规则") @RequestParam(value = "rule") String rule) {
		pointService.updatePointRule(id, name, introduce, num, type,rule);
		return success();
	}
	
	
}
