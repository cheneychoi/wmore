package com.cheney.modules.maye.db.model.out;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Picture;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "图片-出")
public class BannerPictureOut {

	@ApiModelProperty(value = "图片ID")
	private Long id;

	@ApiModelProperty(value = "图片路径")
	private String pictureUrl;
	
	@ApiModelProperty(value="课程ID")
	private Long courseId;
	
	public static BannerPictureOut from(Picture picture) {
		BannerPictureOut po = new BannerPictureOut();
		BeanUtils.copyProperties(picture, po);
		return po;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}
	

}