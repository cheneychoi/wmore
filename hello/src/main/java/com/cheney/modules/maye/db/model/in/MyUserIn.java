package com.cheney.modules.maye.db.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.MyUser;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@ApiModel(value = "用户-入")
public class MyUserIn {
	
	@ApiModelProperty(value = "id")
	private Long id;

    @ApiModelProperty(value = "昵称")
    private String name;

    @ApiModelProperty(value = "年龄")
    private Integer age;

    @ApiModelProperty(value = "爱好")
    private String lover;

    @ApiModelProperty(value = "性别")
    private Integer gender;

    @ApiModelProperty(value = "创建时间")
    private Date createDate;

    @ApiModelProperty(value = "更新时间")
    private Date updateDate;

	public MyUser toMyUser() {
		MyUser mu = new MyUser();
		BeanUtils.copyProperties(this, mu);
		return mu;
	}
}
