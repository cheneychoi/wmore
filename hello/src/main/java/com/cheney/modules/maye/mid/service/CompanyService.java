package com.cheney.modules.maye.mid.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.cheney.modules.maye.db.model.Company;
import com.cheney.share.db.model.Pagination;

public interface CompanyService {
	/**
	 * 企业会员的查
	 */
	Pagination<Company> getCompany(Integer pageNo, Integer pageSize);
	/**
	 * 企业的删
	 */
	int deleteByPrimaryKey(Long userId,Long id);
	/**
	 * 冻结企业
	 */
	boolean updateCompanyFrozen(Long id);
	/**
	 * 解除冻结
	 */
	public void relieveCompany(Long id);
	/**
	 * 企业的编辑
	 */
	boolean updateCompany(Long id,String companyName,Integer companyNumber,String companyPerson,String companyMobile,Long pictureId,List<MultipartFile> files);
	/**
	 * 企业的添加
	 */
	boolean insertCompany(Long userId,String companyName,Integer companyNumber,String companyPerson,String companyMobile,MultipartFile file);
	
	/** 
	 * 根据企业代码和名称查询某个企业
	* @param companyCode
	* @param companyName
	* @return Company
	* @author WangBo
	* @date 2018年7月18日上午9:41:53
	*/ 
	Company selectCompanyByOne(String companyCode,String companyName);
	
	
	/** 
	 * 根据时间或者状态查询企业 分页
	* @param pageNo
	* @param pageSize
	* @param startTime
	* @param endTime
	* @param companyState
	* @return List<Company>
	* @author WangBo
	* @date 2018年7月18日上午9:56:18
	*/ 
	List<Company> selectCompanyByMany(Integer pageNo,Integer pageSize,String startTime,String endTime,Integer companyState);

	/** 
	 * 根据ID查询企业
	* @param id
	* @return Company
	* @author WangBo
	* @date 2018年8月2日上午11:43:18
	*/ 
	JSONObject getCompanyById(Long id);


}