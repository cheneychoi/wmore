package com.cheney.modules.maye.mid.impl;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cheney.modules.maye.db.mapper.ActionMapper;
import com.cheney.modules.maye.db.mapper.CompanyMapper;
import com.cheney.modules.maye.db.mapper.CourseMapper;
import com.cheney.modules.maye.db.mapper.PictureMapper;
import com.cheney.modules.maye.db.mapper.UserCourseMapper;
import com.cheney.modules.maye.db.mapper.UserMapper;
import com.cheney.modules.maye.db.model.Action;
import com.cheney.modules.maye.db.model.ActionExample;
import com.cheney.modules.maye.db.model.Company;
import com.cheney.modules.maye.db.model.CompanyExample;
import com.cheney.modules.maye.db.model.Notice;
import com.cheney.modules.maye.db.model.NoticeMasterplate;
import com.cheney.modules.maye.db.model.Picture;
import com.cheney.modules.maye.db.model.User;
import com.cheney.modules.maye.db.model.UserCourseExample;
import com.cheney.modules.maye.db.model.UserExample;
import com.cheney.modules.maye.db.model.in.ExcelIn;
import com.cheney.modules.maye.db.model.in.MatchUserIn;
import com.cheney.modules.maye.db.model.in.UserAddIn;
import com.cheney.modules.maye.db.model.out.CourseOut;
import com.cheney.modules.maye.db.model.out.MatchUserOut;
import com.cheney.modules.maye.db.model.out.UcOut;
import com.cheney.modules.maye.db.model.out.UserBackOut;
import com.cheney.modules.maye.db.model.out.UserExcelOut;
import com.cheney.modules.maye.db.model.out.UserInfoOut;
import com.cheney.modules.maye.db.model.out.UserPictureOut;
import com.cheney.modules.maye.mid.service.LogService;
import com.cheney.modules.maye.mid.service.NoticeMasterplateService;
import com.cheney.modules.maye.mid.service.NoticeService;
import com.cheney.modules.maye.mid.service.PictureService;
import com.cheney.modules.maye.mid.service.PointService;
import com.cheney.modules.maye.mid.service.UserService;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.excel.ExportExcel;
import com.cheney.share.mid.excel.ImportExcel;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.prop.AliDayuProperties;
import com.cheney.share.mid.prop.AliOSSProperties;
import com.cheney.share.mid.redis.RedisService;
import com.cheney.share.mid.redis.SmsKey;
import com.cheney.share.mid.redis.UrlKey;
import com.cheney.share.mid.utils.AliDayuUtils;
import com.cheney.share.mid.utils.AliOSSUtils;
import com.cheney.share.mid.utils.HttpsUtil;
import com.cheney.share.mid.utils.MD5Util;
import com.cheney.share.mid.utils.ToolUtil;
import com.cheney.share.mid.wechat.DownloadHeader;

@Service
public class UserServiceImpl implements UserService {

	Logger logger = LoggerFactory.getLogger("user");

	@Resource
	RedisService redisService;

	@Resource
	UserMapper userMapper;

	@Resource
	UserCourseMapper userCourseMapper;

	@Resource
	PointService pointService;

	@Resource
	CompanyMapper companyMapper;

	@Resource
	CourseMapper courseMapper;

	@Resource
	AliOSSProperties aliOSSProperties;

	@Resource
	AliDayuProperties aliDayuProperties;

	@Resource
	ActionMapper actionMapper;

	@Resource
	PictureMapper pictureMapper;

	@Resource
	NoticeService noticeService;

	@Resource
	PictureService pictureService;

	@Resource
	LogService logService;

	@Resource
	NoticeMasterplateService noticeMasterplateService;

	/*
	 * 编辑用户信息
	 */
	public void editUserInfo(User user) {
		Long id = user.getId();
		Date birthday = user.getBirthday();
		String city = user.getCity();
		String education = user.getEducation();
		String industry = user.getIndustry();
		String hobby = user.getHobby();

		User u = userMapper.selectByPrimaryKey(id);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = "";
		if (u.getBirthday() != null) {
			dateString = formatter.format(u.getBirthday());
		}
		String birthdayDate = formatter.format(birthday);
		if (StringUtils.isEmpty(dateString) && !StringUtils.isEmpty(birthdayDate)) {
			pointService.plusPoint(3, id, 1);
		}
		if (StringUtils.isEmpty(u.getCity()) && !StringUtils.isEmpty(city)) {
			pointService.plusPoint(3, id, 1);
		}
		if (StringUtils.isEmpty(u.getEducation()) && !StringUtils.isEmpty(education)) {
			pointService.plusPoint(3, id, 1);
		}
		if (StringUtils.isEmpty(u.getIndustry()) && !StringUtils.isEmpty(industry)) {
			pointService.plusPoint(3, id, 1);
		}
		if (StringUtils.isEmpty(u.getHobby()) && !StringUtils.isEmpty(hobby)) {
			pointService.plusPoint(3, id, 1);
		}

		userMapper.updateByPrimaryKeySelective(user);

	}

	/**
	 * 上传头像
	 */
	@Override
	public String uploadHead(String url, Long pictureId) {
		logger.info("uploadHead:url--------------------" + url);
		InputStream inputStream = DownloadHeader.getInputStream(url);
		Picture picture = pictureMapper.selectByPrimaryKey(pictureId);
		String ossId = picture.getOssId();
		String key = AliOSSUtils.uploadToOSS(aliOSSProperties, inputStream, ossId);
		String picUrl = aliOSSProperties.getBucket() + "." + aliOSSProperties.getGateway() + "/" + key;
		picture.setOssId(key);
		picture.setPictureUrl(picUrl);
		logger.info("uploadHead:picUrl---------------------" + picUrl);
		pictureMapper.updateByPrimaryKeySelective(picture);
		return picUrl;
	}

	/**
	 * 先删除再上传
	 * 
	 * @param url
	 * @param pictureId
	 * @return
	 */
	@Transactional
	public String uploadHead1(String url, Long pictureId) {
		logger.info("uploadHead:url--------------------" + url);
		InputStream inputStream = DownloadHeader.getInputStream(url);
		Picture picture = pictureMapper.selectByPrimaryKey(pictureId);
		String ossId = picture.getOssId();
		AliOSSUtils.removeFromOSS(aliOSSProperties, ossId);
		String key = AliOSSUtils.uploadToOSS(aliOSSProperties, inputStream);
		String picUrl = aliOSSProperties.getBucket() + "." + aliOSSProperties.getGateway() + "/" + key;
		picture.setOssId(key);
		picture.setPictureUrl(picUrl);
		logger.info("uploadHead:picUrl---------------------" + picUrl);
		pictureMapper.updateByPrimaryKeySelective(picture);
		return picUrl;
	}

	public String updateHead(String accessUrl, String mediaId, Long pictureId) {
		String result = HttpsUtil.httpsRequestToString(accessUrl, "GET", null);
		JSONObject jsonObject = JSON.parseObject(result);
		String access_token = jsonObject.getString("access_token");
		logger.info("updateHead:access_token-----------------------" + access_token);
		String url = "http://file.api.weixin.qq.com/cgi-bin/media/get?access_token=" + access_token + "&media_id="
				+ mediaId;
		return uploadHead1(url, pictureId);

	}

	@Override
	public void addUser(User user) {
		int num = userMapper.insertSelective(user);
		if (num != 1) {
			throw new HelperException(CodeMsg.ADD_USER_ERROR.getCode(), CodeMsg.ADD_USER_ERROR.getMsg());
		}
	}

	/*
	 * 获取用户个人信息
	 */
	public UserInfoOut getUserInfo(Long userId) {
		return userMapper.getUserById(userId);
	}

	/*
	 * 获取用户已预约课程信息
	 * 
	 */
	public List<CourseOut> getReservedCourse(Long userId) {
		return userMapper.getReservedCourse(userId);
	}

	/*
	 * 获取用户已完成课程信息
	 * 
	 */
	public List<CourseOut> getFinishedCourse(Long userId) {
		return userMapper.getFinishedCourse(userId);
	}

	/*
	 * 积分获取列表和用户积分
	 */
	@Transactional
	public JSONObject getPoint(Long userId) {
		User user = userMapper.selectByPrimaryKey(userId);
		int point = user.getPoints();

		ActionExample ae = new ActionExample();
		ae.or().andUserIdEqualTo(userId);
		List<Action> actions = actionMapper.selectByExample(ae);

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("userPoint", point);
		JSONArray jsonArray = JSONArray.parseArray(JSON.toJSONString(actions));
		jsonObject.put("pointAction", jsonArray);
		return jsonObject;
	}

	/*
	 * 匹配用户
	 */
	@Transactional
	public Notice matchUser(MatchUserIn in,HttpSession session) {
		logger.info("传入的参数是-----" + in.toString());
		int num = 1;
		User user = new User();
		MatchUserOut matchUserOut = null;
		String oldCode = redisService.get(SmsKey.withExpire(300 * 1000), in.getMobile(), String.class);
		logger.info("验证码为-------------" + oldCode);
		if (!oldCode.equals(in.getVerCode())) {
			throw new HelperException(CodeMsg.CODE_ERROR.getCode(), CodeMsg.CODE_ERROR.getMsg());
		} else {
			HashMap<String, String> map = new HashMap<>();
			map.put("name", in.getName());
			map.put("mobile", in.getMobile());
			map.put("email", in.getEmail());
			map.put("code", in.getCode());
			matchUserOut = userMapper.matchUser(map);
		}
		if (matchUserOut == null) {
			throw new HelperException(CodeMsg.MATCH_USER_ERROR.getCode(), CodeMsg.MATCH_USER_ERROR.getMsg());
		}
		String openid = (String) session.getAttribute("openId");
		String url = redisService.get(UrlKey.withExpire(0), openid, String.class);
		InputStream inputStream = DownloadHeader.getInputStream(url);
		int picNum = 0;
		String key = AliOSSUtils.uploadToOSS(aliOSSProperties, inputStream);
		Picture picture = new Picture();
		picture.setPictureName(openid);
		picture.setOssId(key);
		picture.setContentUrl("");
		picture.setPictureType(3);
		picture.setCourseId(0L);
		picture.setCreateDate(ToolUtil.getTime(0));
		picture.setPictureUrl(aliOSSProperties.getBucket() + "." + aliOSSProperties.getGateway() + "/" + key);
		picNum = pictureMapper.insertSelective(picture);
		if (picNum == 0) {
			AliOSSUtils.removeFromOSS(aliOSSProperties, key);
			throw new HelperException(CodeMsg.PICTURE_SAVE_ERROR.getCode(), CodeMsg.PICTURE_SAVE_ERROR.getMsg());
		}
		logger.info("头像图片ID-------------" + picture.getId());
		user.setPictureId(picture.getId());
		user.setId(matchUserOut.getId());
		user.setWechatOpenid(openid);
		user.setIsBing(1);
		user.setUpdateDate(ToolUtil.getTime(0));
		num = userMapper.updateByPrimaryKeySelective(user);
		logger.info("绑定---------------" + num);
		if (num == 0) {
			throw new HelperException(CodeMsg.BIND_OPENID_ERROR.getCode(), CodeMsg.BIND_OPENID_ERROR.getMsg());
		}
		pointService.plusPoint(6, matchUserOut.getId(), 1);

		NoticeMasterplate noticeMasterplate = noticeMasterplateService.getNoticeMasterplateByType(5);
		String title = noticeMasterplate.getTitle();
		String content = noticeMasterplate.getContent();

		Notice notice = new Notice();
		notice.setUserId(user.getId());
		notice.setTitle(title);
		notice.setContent(content);
		notice.setCreateDate(ToolUtil.getTime(0));
		noticeService.addNotice(notice);
		return notice;
	}

	/*
	 * 发送验证码
	 */
	public void sendSMS(String mobile) {
		Random rd = new Random();
		String code = String.valueOf(rd.nextInt(899999) + 100000);
		JSONObject param = new JSONObject();
		param.put("code", code);
		redisService.set(SmsKey.withExpire(300 * 1000), mobile, code);
		AliDayuUtils.sendSmsV2(aliDayuProperties, mobile, aliDayuProperties.getPhoneBindTMID(), param, "");
	}

	/*
	 * 匹配原密码
	 */
	public void matchPassword(Long userId, String password) {
		// 加盐加密
		String saltPassword = MD5Util.inputPassToFormPass(password);
		UserExample ue = new UserExample();
		ue.or().andIdEqualTo(userId).andPasswordEqualTo(saltPassword);
		List<User> users = userMapper.selectByExample(ue);
		if (users.size() == 0) {
			throw new HelperException(CodeMsg.OLD_PASSWORD_ERROR.getCode(), CodeMsg.OLD_PASSWORD_ERROR.getMsg());
		}
	}

	/*
	 * 设置新密码
	 */
	public void updatePassword(Long userId, String password) {
		String saltPassword = MD5Util.inputPassToFormPass(password);
		User user = new User();
		user.setId(userId);
		user.setPassword(saltPassword);
		user.setUpdateDate(ToolUtil.getTime(0));
		int num = userMapper.updateByPrimaryKeySelective(user);
		if (num == 0) {
			throw new HelperException(CodeMsg.NEW_PASSWORD_ERROR.getCode(), CodeMsg.NEW_PASSWORD_ERROR.getMsg());
		}
	}

	/**
	 * 企业下级员工账号的查
	 */
	@Override
	public Pagination<User> getUserMember(Long companyId, Integer pageNo, Integer pageSize) {
		Pagination<User> page = new Pagination<>(pageNo, pageSize);
		UserExample ue = new UserExample();
		ue.or().andIdEqualTo(companyId);
		List<User> list = userMapper.getUserMember(companyId);
		int count = (int) userMapper.countByExample(ue);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	/**
	 * 企业会员的增
	 */
	public boolean insertUserMember(Long userId, UserAddIn in) {
		/*
		 * String companyName=u.getcom companyMapper.getCompanyId(companyName);
		 */
		CompanyExample ce = new CompanyExample();
		ce.or().andCompanyNameEqualTo(in.getCompanyName());
		List<Company> companies = companyMapper.selectByExample(ce);
		if (companies.size() != 1) {
			throw new HelperException(CodeMsg.SELECT_COMPANY_ERROR.getCode(), CodeMsg.SELECT_COMPANY_ERROR.getMsg());
		}
		UserExample ue = new UserExample();
		ue.or().andMobileEqualTo(in.getMobile());
		List<User> users = userMapper.selectByExample(ue);
		if (users.size() != 0) {
			throw new HelperException(CodeMsg.MOBILE_ERROR.getCode(), CodeMsg.MOBILE_ERROR.getMsg());
		}
		ue.or().andEmailEqualTo(in.getEmail());
		List<User> email = userMapper.selectByExample(ue);
		if (email.size() != 0) {
			throw new HelperException(CodeMsg.EMAIL_ERROR.getCode(), CodeMsg.EMAIL_ERROR.getMsg());
		}
		Long id = companies.get(0).getId();
		User user = new User();
		user.setCity(in.getCity());
		user.setCompanyId(id);
		user.setCreateDate(ToolUtil.getTime(0));
		user.setIsMember(1);
		user.setEmail(in.getEmail());
		user.setIndustry(in.getIndustry());
		user.setMobile(in.getMobile());
		user.setSex(in.getSex());
		user.setPosition(in.getPosition());
		user.setName(in.getName());
		int num = userMapper.insertSelective(user);
		if (num == 0) {
			throw new HelperException(CodeMsg.USER_ERROR.getCode(), CodeMsg.USER_ERROR.getMsg());
		}
		Company company = companyMapper.selectByPrimaryKey(user.getCompanyId());
		String companyName = company.getCompanyName();
		String userName = user.getName();
		String mobile = user.getMobile();
		String msg = "新增会员名为" + userName + "(所属企业:" + companyName + ",手机号码:" + mobile + ")";
		logService.addLog(userId, msg, 2);
		return num >= 1 ? true : false;
	}

	/**
	 * 企业会员的删
	 */
	@Override
	@Transactional
	public int deleteByPrimaryKey(Long userId, Long id) {
		
		UserCourseExample userCourseExample = new UserCourseExample();
		userCourseExample.or().andUserIdEqualTo(id);
		userCourseMapper.deleteByExample(userCourseExample);
		
		User user = userMapper.selectByPrimaryKey(id);
		Company company = companyMapper.selectByPrimaryKey(user.getCompanyId());
		String companyName = "";
		if (company == null) {
			companyName = "无企业";
		}else {
			companyName = company.getCompanyName();
		}
		String userName = user.getName();
		String mobile = user.getMobile();
		String msg = "删除会员名为" + userName + "(所属企业:" + companyName + ",手机号码:" + mobile + ")";
		logService.addLog(userId, msg, 3);
		if (userMapper.deleteByPrimaryKey(id) == 0) {
			throw new HelperException("出错了");
		}
		return userMapper.deleteByPrimaryKey(id);
	}

	/**
	 * 企业会员的改
	 */
	@Override
	public boolean updateByPrimaryKeySelective(User user) {
		int n = userMapper.updateByPrimaryKeySelective(user);
		if (n != 1) {
			throw new HelperException("出错了");
		}
		return n == 1 ? true : false;
	}

	@Override
	public void updateUserFrozen(Long id, Integer state) {
		User user = new User();
		user.setId(id);
		user.setState(state);
		userMapper.updateByPrimaryKeySelective(user);
	}

	@Override
	public List<User> getUser() {
		return userMapper.getUser();
	}

	/*
	 * 企业员工按真实姓名或手机号或者邮箱条件查询
	 */
	public UserBackOut selectUserByOne(String name, String mobile, String email) {
		if (userMapper.selectUserByOne(name, mobile, email) == null) {
			throw new HelperException(CodeMsg.NO_MEMBER_ERROR.getCode(), CodeMsg.NO_MEMBER_ERROR.getMsg());
		}
		return userMapper.selectUserByOne(name, mobile, email);
	}

	/*
	 * 企业员工按时间或状态或公司名称或职位或是否是会员条件查询
	 */
	public List<UserBackOut> selectUserByMany(Integer pageNo, Integer pageSize, String companyName, Integer state,
			Integer position, Integer isMember, String name, String mobile, String email) {
		System.out.println("mobile============" + mobile);
		pageNo = (pageNo - 1) * pageSize;

		HashMap<String, Object> map = new HashMap<>();
		map.put("pageNo", pageNo);
		map.put("pageSize", pageSize);
		map.put("companyName", companyName);
		map.put("state", state);
		map.put("position", position);
		map.put("isMember", isMember);
		map.put("name", name);
		map.put("mobile", mobile);
		map.put("email", email);

		return userMapper.selectUserByMany(map);
	}

	@Override
	public UserPictureOut getActionById(Long UserId) {
		return userMapper.getActionById(UserId);
	}

	@Override
	public String exportExcel(Long companyId) {
		List<UserExcelOut> excelOuts = userMapper.exportExcel(companyId);
		ExportExcel excel = new ExportExcel();
		String path = excel.exportExcel(excelOuts);
		return path;
	}

	@Override
	public UcOut getUserById(Long id) {
		return userMapper.getAdminUserById(id);
	}

	@Override
	public Pagination<UcOut> getAllUser(Integer pageNo, Integer pageSize) {
		Pagination<UcOut> page = new Pagination<>(pageNo, pageSize);
		UserExample ex = new UserExample();
		pageNo = (pageNo - 1) * pageSize;
		List<UcOut> list = userMapper.getAllUser(pageNo, pageSize);
		int count = (int) userMapper.countByExample(ex);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public JSONObject getInfo() {

		// 企业总数
		CompanyExample ce = new CompanyExample();
		ce.createCriteria();
		Long companyTotal = companyMapper.countByExample(ce);

		// 会员总数
		UserExample ue = new UserExample();
		ue.createCriteria();
		Long userTotal = userMapper.countByExample(ue);

		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH) + 1;
		String theMonth = year + "-0" + month;
		// 当月企业总数
		Long companyTotalByMonth = companyMapper.getCompanyByMonth(theMonth);

		// 当月新会员
		Long userTotalByMonth = userMapper.getUserByMonth(theMonth);

		// 今日课程
		Long courseTotalByDay = courseMapper.getCourseByday();

		JSONObject jsonObject = new JSONObject();
		jsonObject.put("companyTotal", companyTotal);
		jsonObject.put("userTotal", userTotal);
		jsonObject.put("companyTotalByMonth", companyTotalByMonth);
		jsonObject.put("userTotalByMonth", userTotalByMonth);
		jsonObject.put("courseTotalByDay", courseTotalByDay);

		return jsonObject;
	}

	@Override
	public List<UcOut> getCompanyById(Long companyId) {
		return userMapper.getCompanyById(companyId);
	}

	@Override
	@Transactional
	public JSONObject importExcel(MultipartFile file) {
		JSONObject jsonObject = new JSONObject();
		String fileName = file.getOriginalFilename();
		if (!fileName.endsWith("xls") && !fileName.endsWith("xlsx")) {
			jsonObject.put("code", 600100);
			jsonObject.put("msg", "文件类型错误！");
			return jsonObject;
		}
		File f = null;
		InputStream ins;
		try {
			ins = file.getInputStream();
			f = new File(file.getOriginalFilename());
			ToolUtil.inputStreamToFile(ins, f);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		ImportExcel importExcel = new ImportExcel();
		List<ExcelIn> excelIns = null;
		jsonObject.put("code", 200);
		jsonObject.put("msg", "导入成功！");
		try {
			excelIns = importExcel.importExcel(f);
			for (ExcelIn excelIn : excelIns) {
				addUserByExcel(excelIn);
			}
		} catch (HelperException e) {
			jsonObject.put("code", e.getCode());
			jsonObject.put("msg", e.getMessage());
		} catch (EncryptedDocumentException e) {
			e.printStackTrace();
		} catch (InvalidFormatException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			File del = new File(f.toURI());  
			del.delete(); 
		}
		
		return jsonObject;
		
	}

	@Override
	public void addUserByExcel(ExcelIn in) {
		CompanyExample companyExample = new CompanyExample();
		companyExample.or().andCompanyCodeEqualTo(in.getCompanyCode());
		List<Company> companies = companyMapper.selectByExample(companyExample);
		if (companies.size() == 0) {
			throw new HelperException(CodeMsg.COMPANYCODE_ERROR.getCode(), CodeMsg.COMPANYCODE_ERROR.getMsg());
		}
		Long code = companies.get(0).getId();
		User user = new User();
		user.setCreateDate(ToolUtil.getTime(0));
		user.setEmail(in.getEmail());
		user.setMobile(in.getMobile());
		user.setName(in.getUsername());
		user.setSex(in.getSex());
		user.setPosition(in.getPosition());
		user.setCompanyId(code);
		userMapper.insertSelective(user);
	}

	@Override
	public void deleteExcel(String url) {
		File file =new File(url);
		if (file.exists()) {
			file.delete();
		}
	}

	@Override
	public void sendOrderSMS(String mobile,String name,String time,String location) {
		JSONObject param = new JSONObject();
		param.put("name", name);
		param.put("time", time);
		param.put("location", location);
		AliDayuUtils.sendSmsV2(aliDayuProperties, mobile, aliDayuProperties.getPhoneOrderTMID(), param, "");
	}

	@Override
	public void sendNoticeSMS(String mobile,String name,String time,String location) {
		JSONObject param = new JSONObject();
		param.put("name", name);
		param.put("time", time);
		param.put("location", location);
		AliDayuUtils.sendSmsV2(aliDayuProperties, mobile, aliDayuProperties.getPhoneNoticeTMID(), param, "");
	}

}
