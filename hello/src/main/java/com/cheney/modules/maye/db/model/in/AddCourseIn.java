package com.cheney.modules.maye.db.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.CourseWithBLOBs;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "课程-入")
public class AddCourseIn {

	@ApiModelProperty(value = "课程名称")
	private String name;

	@ApiModelProperty(value = "课程名称英语")
	private String nameEN;

	@ApiModelProperty(value = "课程地点")
	private String palce;

	@ApiModelProperty(value = "课程时间")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date time;

	@ApiModelProperty(value = "课程数量")
	private int num;
	
	@ApiModelProperty(value = "课程简介")
	private String introduction;
	
	@ApiModelProperty(value = "适合人群")
	private String crowd;
	
	@ApiModelProperty(value = "训练效果")
	private String trainingEffect;
	
	@ApiModelProperty(value = "训练准备")
	private String trainingPreparation;
	
	@ApiModelProperty(value = "其他事项")
	private String otherBusiness;

	@ApiModelProperty(value = "课程权限 1-高管 2-员工 3-全部")
	private int authority;

	@ApiModelProperty(value = "课程类型 1-员工课 2-户外课")
	private int type;

	@ApiModelProperty(value = "上课老师")
	private String teacher;

	@ApiModelProperty(value = "企业ID")
	private Long companyId;

	public CourseWithBLOBs toCourse() {
		CourseWithBLOBs course = new CourseWithBLOBs();
		BeanUtils.copyProperties(this, course);
		return course;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNameEN() {
		return nameEN;
	}

	public void setNameEN(String nameEN) {
		this.nameEN = nameEN;
	}

	public String getPalce() {
		return palce;
	}

	public void setPalce(String palce) {
		this.palce = palce;
	}

	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	public int getNum() {
		return num;
	}

	public void setNum(int num) {
		this.num = num;
	}

	public int getAuthority() {
		return authority;
	}

	public void setAuthority(int authority) {
		this.authority = authority;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String getTeacher() {
		return teacher;
	}

	public void setTeacher(String teacher) {
		this.teacher = teacher;
	}

	public Long getCompanyId() {
		return companyId;
	}

	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}

}