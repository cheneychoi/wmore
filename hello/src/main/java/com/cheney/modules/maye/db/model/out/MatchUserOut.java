package com.cheney.modules.maye.db.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "匹配用户-出")
public class MatchUserOut {
	
	@ApiModelProperty(value = "id")
	private Long id;

	@ApiModelProperty(value = "用户姓名")
	private String name;
	
	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "公司编号")
	private String code;
	
	@ApiModelProperty(value = "微信openId")
	private String wechatOpenid;
	
	@ApiModelProperty(value = "是否绑定")
	private Integer isBind;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getWechatOpenid() {
		return wechatOpenid;
	}

	public void setWechatOpenid(String wechatOpenid) {
		this.wechatOpenid = wechatOpenid;
	}

	public Integer getIsBind() {
		return isBind;
	}

	public void setIsBind(Integer isBind) {
		this.isBind = isBind;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	@Override
	public String toString() {
		return "MatchUserOut [id=" + id + ", name=" + name + ", email=" + email + ", mobile=" + mobile + ", code="
				+ code + ", wechatOpenid=" + wechatOpenid + ", isBind=" + isBind + "]";
	}

	



	
	
	
}