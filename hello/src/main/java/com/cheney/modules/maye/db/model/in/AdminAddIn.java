package com.cheney.modules.maye.db.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Admin;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value="管理员添加-输入")
public class AdminAddIn {
	@ApiModelProperty(value="昵称")
	private String nickname;
	@ApiModelProperty(value="管理员姓名")
	private String name;
	@ApiModelProperty(value="用户名")
	private String username;
	@ApiModelProperty(value="密码")
	private String password;
	@ApiModelProperty(value="创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date createDate;
	public Admin toAdminAddIn() {
		Admin a = new Admin();
		BeanUtils.copyProperties(this, a);
		return a;
	}
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public Date getCreateDate() {
		return createDate;
	}
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
	
}
