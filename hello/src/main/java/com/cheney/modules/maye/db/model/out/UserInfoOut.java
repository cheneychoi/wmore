package com.cheney.modules.maye.db.model.out;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.User;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "用户-出")
public class UserInfoOut {

	@ApiModelProperty(value = "用户ID")
	private Long id;

	@ApiModelProperty(value = "真实姓名")
	private String name;
	@ApiModelProperty(value = "昵称")
	private String nickname;
	@ApiModelProperty(value = "用户名")
	private String username;

	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "性别(1 男  2 女)")
	private Integer sex;

	@ApiModelProperty(value = "生日")
	private Date birthday;

	@ApiModelProperty(value = "城市")
	private String city;

	@ApiModelProperty(value = "学历")
	private String education;

	@ApiModelProperty(value = "行业")
	private String industry;

	@ApiModelProperty(value = "爱好")
	private String hobby;

	@ApiModelProperty(value = "薪资范围")
	private String incomeRange;

	@ApiModelProperty(value = "感兴趣的内容")
	private String interestingContent;
	
	@ApiModelProperty(value="图片地址")
	private String pictureUrl;
	
	@ApiModelProperty(value = "图片id")
	private Long pictureId;

	public static UserInfoOut from(User user) {
		UserInfoOut uo = new UserInfoOut();
		BeanUtils.copyProperties(user, uo);
		return uo;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getEducation() {
		return education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public String getPictureUrl() {
		return pictureUrl;
	}

	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}

	public Long getPictureId() {
		return pictureId;
	}

	public void setPictureId(Long pictureId) {
		this.pictureId = pictureId;
	}

	public String getIncomeRange() {
		return incomeRange;
	}

	public void setIncomeRange(String incomeRange) {
		this.incomeRange = incomeRange;
	}

	public String getInterestingContent() {
		return interestingContent;
	}

	public void setInterestingContent(String interestingContent) {
		this.interestingContent = interestingContent;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}
}