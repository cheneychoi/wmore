package com.cheney.modules.maye.db.model.in;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.User;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "添加用户-入")
public class AddUserIn {
	

	@ApiModelProperty(value = "真实姓名")
	private String name;

	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "手机号")
	private String mobile;
	
	@ApiModelProperty(value = "企业的ID")
	private Long companyId;
	
	@ApiModelProperty(value = "是否是会员 1-是 2-不是")
	private int isMember;
	

	public User toUser() {
		User user = new User();
		BeanUtils.copyProperties(this, user);
		return user;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getMobile() {
		return mobile;
	}


	public void setMobile(String mobile) {
		this.mobile = mobile;
	}


	public Long getCompanyId() {
		return companyId;
	}


	public void setCompanyId(Long companyId) {
		this.companyId = companyId;
	}


	public int getIsMember() {
		return isMember;
	}


	public void setIsMember(int isMember) {
		this.isMember = isMember;
	}
	
	

	
	
}