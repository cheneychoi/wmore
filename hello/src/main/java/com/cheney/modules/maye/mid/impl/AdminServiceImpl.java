package com.cheney.modules.maye.mid.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.fastjson.JSON;
import com.cheney.modules.maye.db.mapper.AdminMapper;
import com.cheney.modules.maye.db.mapper.AdminRoleMapper;
import com.cheney.modules.maye.db.mapper.AuthorityMapper;
import com.cheney.modules.maye.db.model.Admin;
import com.cheney.modules.maye.db.model.AdminExample;
import com.cheney.modules.maye.db.model.AdminExample.Criteria;
import com.cheney.modules.maye.db.model.AdminRole;
import com.cheney.modules.maye.db.model.out.AdminOut;
import com.cheney.modules.maye.mid.service.AdminService;
import com.cheney.modules.maye.mid.service.LogService;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.redis.AuthKey;
import com.cheney.share.mid.redis.RedisService;
import com.cheney.share.mid.utils.ToolUtil;
import com.cheney.share.mid.utils.ValidateCode;

@Service
public class AdminServiceImpl implements AdminService {

	@Autowired
	AdminMapper adminMapper;
	@Autowired
	LogService logService;
	@Autowired
	AdminRoleMapper adminRoleMapper;
	@Autowired
	AuthorityMapper authorityMapper;
	@Autowired
	RedisService redisService;

	@Transactional
	public Long login(String username, String password) {
		List<Admin> list = adminMapper.getUsername(username);
		int adminNum=list.size();
		// 判断密码是否正确
		List<Admin> admins = adminMapper.getPsd(username, password);
		Long id = null;
		if (adminNum > 0) {
			if (admins.size() > 0) {
				Admin admin = admins.get(0);
				id = admin.getId();
				adminMapper.updateLoginNum(id);
				String msg = "登陆Wmore管理平台";
				logService.addLog(id,msg,1);
				AdminRole adminRole = adminRoleMapper.selectByPrimaryKey(id);
				Long roleId = adminRole.getRoleId();
				List<String> lists = authorityMapper.getAuthorityByRole(roleId);
				redisService.set(AuthKey.getKey, id.toString(), JSON.toJSONString(lists).toString());
			} else {
				throw new HelperException(CodeMsg.PASSWORD_ERROR.getCode(), CodeMsg.PASSWORD_ERROR.getMsg());
			}
		} else {
			throw new HelperException(CodeMsg.USERNAME_ERROR.getCode(), CodeMsg.USERNAME_ERROR.getMsg());
		}
		return id;
	}

	public void getCode(HttpServletRequest request) {
		ValidateCode validateCode = new ValidateCode();
		String code = validateCode.getCode();
		HttpSession session = request.getSession();
		session.setAttribute("code", code);
	}

	@Override
	public boolean insertAdmin(String nickname, String name, String username, String password, Long roleId) {
		Admin a = new Admin();
		a.setNickname(nickname);
		a.setName(name);
		a.setUsername(username);
		a.setPassword(password);
		a.setCreateDate(ToolUtil.getTime(0));
		int n = adminMapper.insertSelective(a);
		if (n == 0) {
			throw new HelperException(CodeMsg.ADD_ADMIN_ERROR.getCode(), CodeMsg.ADD_ADMIN_ERROR.getMsg());
		}
		AdminRole adminRole = new AdminRole();
		adminRole.setAdminId(a.getId());
		adminRole.setRoleId(roleId);
		adminRoleMapper.insertSelective(adminRole);
		return n >= 1 ? true : false;
	}

	@Override
	public Admin getAdminById(Long id) {
		return adminMapper.selectByPrimaryKey(id);
	}

	@Override
	public boolean updateAdmin(Long id, String nickname, String name, String username, String password,Long roleId) {
		Admin a = new Admin();
		a.setId(id);
		a.setNickname(nickname);
		a.setName(name);
		a.setUsername(username);
		a.setPassword(password);
		
		AdminRole adminRole = new AdminRole();
		adminRole.setAdminId(id);
		adminRole.setRoleId(roleId);
		adminRoleMapper.updateByPrimaryKeySelective(adminRole);
		redisService.delete(AuthKey.getKey, id.toString());
		
		int num = adminMapper.updateByPrimaryKeySelective(a);
		if (num == 0) {
			throw new HelperException(CodeMsg.UPDATE_ADMIN_ERROR.getCode(), CodeMsg.UPDATE_ADMIN_ERROR.getMsg());
		}
		return num >= 1 ? true : false;
	}

	@Override
	public boolean deleteAdmin(Long id) {
		int num = adminMapper.deleteByPrimaryKey(id);
		if (num == 0) {
			throw new HelperException(CodeMsg.DELETE_ADMIN_ERROR.getCode(), CodeMsg.DELETE_ADMIN_ERROR.getMsg());
		}
		return num >= 1 ? true : false;
	}

	@Override
	public Pagination<AdminOut> getAllAdminByPage(Integer pageNo, Integer pageSize) {
		Pagination<AdminOut> page = new Pagination<>(pageNo, pageSize);
		pageNo = (pageNo - 1) * pageSize;
		List<AdminOut> list = adminMapper.getAllAdmin(pageNo, pageSize);
		page.setItems(list);
		page.setRecords(list.size());
		return page;
	}

	@Override
	public boolean deleteAdminBatch(String ids) {
		AdminExample example = new AdminExample();
		Criteria criteria = example.createCriteria();
		criteria.andIdIn(ToolUtil.toList(ids));
		int num = adminMapper.deleteByExample(example);
		return num >= 1 ? true : false;
	}

	@Override
	public boolean updatePsd(Long id, String password) {
		Admin admin = new Admin();
		admin.setId(id);
		admin.setPassword(password);
		int num = adminMapper.updateByPrimaryKeySelective(admin);
//		int num=adminMapper.updatePsd(id,password);
		return num >= 1 ? true : false;
	}

	@Override
	public void updateRole(Long adminId, Long roleId) {
		AdminRole adminRole = new AdminRole();
		adminRole.setAdminId(adminId);
		adminRole.setRoleId(roleId);
		adminRoleMapper.updateByPrimaryKeySelective(adminRole);

		redisService.delete(AuthKey.getKey, adminId.toString());
	}

}
