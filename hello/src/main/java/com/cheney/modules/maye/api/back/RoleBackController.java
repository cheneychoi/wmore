package com.cheney.modules.maye.api.back;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.db.model.Role;
import com.cheney.modules.maye.mid.service.RoleService;
import com.cheney.share.api.controller.BaseController;
import com.cheney.share.db.model.Pagination;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping(value = "/wmore_back")
@CrossOrigin
public class RoleBackController extends BaseController {
	@Autowired
	RoleService roleService;

	@ApiOperation(value = "分页查询所有角色列表", notes = "")
	@RequestMapping(value = "/role/getAllRoleByPage", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAllRoleByPage(
			@ApiParam(value = "当前页码", defaultValue = "1") @RequestParam("pn") Integer pageNo,
			@ApiParam(value = "每页记录数", defaultValue = "10") @RequestParam("ps") Integer pageSize) {
		Pagination<Role> role = roleService.getAllRoleByPage(pageNo, pageSize);
		return success(role);
	}

	@ApiOperation(value = "添加角色", notes = "")
	@RequestMapping(value = "/role/insertRole", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> insertRole(@ApiParam(value = "角色名") @RequestParam("name") String name,
			@ApiParam(value = "描述") @RequestParam("description") String description,
			@ApiParam(value = "权限ID") @RequestParam("authorityId") String authorityId) {
		return success(roleService.insertRole(name, description, authorityId));
	}

	@ApiOperation(value = "点击id查看角色", notes = "")
	@RequestMapping(value = "/admin/getRoleById", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getRoleById(@ApiParam(value = "角色id") @RequestParam("id") Long id) {
		return success(roleService.getRoleById(id));
	}

	@ApiOperation(value = "编辑角色", notes = "")
	@RequestMapping(value = "/role/updateRole", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateRole(@ApiParam("id") @RequestParam(value = "id") Long id,
			@ApiParam(value = "角色名") @RequestParam(value = "name", required = false) String name,
			@ApiParam(value = "描述") @RequestParam(value = "description", required = false) String description,
			@ApiParam(value = "权限ID") @RequestParam(value = "authorityId",required=false) String authorityIds) {
		System.out.println("先"+authorityIds);
		return success(roleService.updateRole(id, name, description,authorityIds));
	}

	@ApiOperation(value = "删除角色", notes = "")
	@RequestMapping(value = "/role/deleteRole", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> deleteRole(@ApiParam(value = "角色id") @RequestParam("id") Long id) {
		return success(roleService.deleteRole(id));
	}

	@ApiOperation(value = "批量删除角色", notes = "")
	@RequestMapping(value = "/role/deleteRoleBatch", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> deleteRoleBatch(@RequestParam("ids") String ids) {
		return success(roleService.deleteRoleBatch(ids));
	}

/*	@ApiOperation(value = "修改角色权限", notes = "")
	@RequestMapping(value = "/role/updateAuthority", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> updateAuthority(
			@ApiParam(value = "角色ID") @RequestParam(value = "roleId") Long roleId,
			@ApiParam(value = "权限ID") @RequestParam(value = "authorityId") String authorityIds) {
		roleService.updateAuthority(roleId, authorityIds);
		return success();
	}*/
	@ApiOperation(value = "根据角色查权限", notes = "")
	@RequestMapping(value = "/role/getRoleAuthority", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getRoleAuthority(
			@ApiParam(value = "角色ID") @RequestParam(value = "roleId") Long roleId) {
		return success(roleService.getRoleAuthority(roleId));
	}

}
