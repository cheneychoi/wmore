package com.cheney.modules.maye.db.model.out;


import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "评价-出")
public class EvaluateOut {

	@ApiModelProperty(value = "中间表id")
	private Long id;
	@ApiModelProperty(value = "用户id")
	private Long userId;
	@ApiModelProperty(value = "课程id")
	private Long courseId;
	@ApiModelProperty(value = "课程名称")
	private String courseName;
	@ApiModelProperty(value = "课程名称英语")
	private String courseNameEN;
	@ApiModelProperty(value = "上课时间")
	private Date courseDate;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseNameEN() {
		return courseNameEN;
	}

	public void setCourseNameEN(String courseNameEN) {
		this.courseNameEN = courseNameEN;
	}

	public Date getCourseDate() {
		return courseDate;
	}

	public void setCourseDate(Date courseDate) {
		this.courseDate = courseDate;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

}
