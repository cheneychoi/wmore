package com.cheney.modules.maye.db.model.in;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Company;

import io.swagger.annotations.ApiModelProperty;

public class CompanyStateIn {
	@ApiModelProperty(value = "企业id")
	private Long id;
	@ApiModelProperty(value = "企业状态：1：冻结，2：未冻结")
	private Integer state;
	public Company toCompanyStateIn() {
		Company c= new Company();
		BeanUtils.copyProperties(this, c);
		return c;
	}
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Integer getState() {
		return state;
	}

	public void setState(Integer state) {
		this.state = state;
	}
}
