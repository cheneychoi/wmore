package com.cheney.modules.maye.api.front;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.db.model.Notice;
import com.cheney.modules.maye.db.model.out.NoticeOut;
import com.cheney.modules.maye.mid.service.NoticeService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "登录")
@Controller
@RequestMapping(value = "/wmore")
public class NoticeController extends BaseController {

	@Resource
	NoticeService noticeService;

	@ApiOperation(value = "每日系统通知", notes = "每天第一次登陆平台会有上课通知以及之后每日签到通知", response = NoticeOut.class)
	@RequestMapping(value = "/notice/noticeByDay", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> noticeByDay(
			@ApiParam(value = "用户id") @RequestParam(name = "userId", required = false) Long userId) {
		logger.info("-----------进入每日系统通知的方法---------------");
		logger.info("userId----------------" + userId);
		if (userId != 0) {
			List<NoticeOut> noticeOuts = noticeService.getNoticeByDay(userId);
			logger.info("返回的数据----------------" + noticeOuts);
			return success(noticeOuts);
		}else {
			return success();
		}
	}

	@ApiOperation(value = "获取未读通知", notes = "获取未读通知")
	@RequestMapping(value = "/notice/getUnreadNotice", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getUnreadNotice(@ApiParam(value = "用户ID") @RequestParam("id") Long userId) {
		logger.info("-----------进入获取未读通知的方法---------------");
		logger.info("userId----------------" + userId);
		List<Notice> notices = noticeService.getUnreadNotice(userId);
		logger.info("返回的数据----------------" + notices);
		return success(notices);
	}

	@ApiOperation(value = "获取已读通知", notes = "获取已读通知")
	@RequestMapping(value = "/notice/getReadNotice", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getReadNotice(@ApiParam(value = "用户ID") @RequestParam("id") Long userId) {
		logger.info("-----------进入获取已读通知的方法---------------");
		logger.info("userId----------------" + userId);
		List<Notice> notices = noticeService.getReadNotice(userId);
		logger.info("返回的数据----------------" + notices);
		return success(notices);
	}

	@ApiOperation(value = "查看通知", notes = "将通知状态从未读变为已读")
	@RequestMapping(value = "/notice/toReadNotice", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> toReadNotice(@ApiParam(value = "通知Id") @RequestParam("noticeId") Long noticeId) {
		logger.info("-----------进入获取未读通知的方法---------------");
		logger.info("noticeId----------------" + noticeId);
		Notice notice = noticeService.toReadNotice(noticeId);
		logger.info("返回的数据----------------" + notice);
		return success(notice);
	}

}
