package com.cheney.modules.maye.db.model.in;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Picture;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value="BANNER的图片链接设置")
public class PictureIn {
	@ApiModelProperty(value="图片的ID")
	private Long id;
	@ApiModelProperty(value="图片路径")
	private String pictureUrl;
	public Picture toPictureIn() {
		Picture picture = new Picture();
		BeanUtils.copyProperties(this, picture);
		return picture;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getPictureUrl() {
		return pictureUrl;
	}
	public void setPictureUrl(String pictureUrl) {
		this.pictureUrl = pictureUrl;
	}
	
}
