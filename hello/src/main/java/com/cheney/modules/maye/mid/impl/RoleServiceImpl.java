package com.cheney.modules.maye.mid.impl;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheney.modules.maye.db.mapper.AdminRoleMapper;
import com.cheney.modules.maye.db.mapper.RoleAuthorityMapper;
import com.cheney.modules.maye.db.mapper.RoleMapper;
import com.cheney.modules.maye.db.model.AdminRole;
import com.cheney.modules.maye.db.model.AdminRoleExample;
import com.cheney.modules.maye.db.model.Role;
import com.cheney.modules.maye.db.model.RoleAuthority;
import com.cheney.modules.maye.db.model.RoleAuthorityExample;
import com.cheney.modules.maye.db.model.RoleExample;
import com.cheney.modules.maye.db.model.RoleExample.Criteria;
import com.cheney.modules.maye.mid.service.RoleService;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.redis.AuthKey;
import com.cheney.share.mid.redis.RedisService;
import com.cheney.share.mid.utils.ToolUtil;
@Service
public class RoleServiceImpl implements RoleService{

	@Autowired
	RoleMapper roleMapper; 
	
	@Autowired
	RoleAuthorityMapper roleAuthorityMapper;
	
	@Autowired
	AdminRoleMapper adminRoleMapper;
	
	@Autowired
	RedisService redisService;
	
	@Override
	public Pagination<Role> getAllRoleByPage(Integer pageNo, Integer pageSize) {
		Pagination<Role> page=new Pagination<>(pageNo, pageSize);
		RoleExample rx = new RoleExample();
		List<Role> list = roleMapper.selectByExample(rx);
		int count=(int) roleMapper.countByExample(rx);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public boolean insertRole(String name, String description,String authorityId) {
		Role r = new Role();
		r.setCreateTime(ToolUtil.getTime(0));
		r.setName(name);
		r.setDescription(description);
		int num=roleMapper.insertSelective(r);
		if(num==0) {
			throw new HelperException(CodeMsg.INSERT_ROLE_ERROR.getCode(), CodeMsg.INSERT_ROLE_ERROR.getMsg());
		}
		System.out.println(authorityId);
		List<String> aIds = Arrays.asList(authorityId.split(","));
		System.out.println(aIds);
		RoleAuthority roleAuthority = new RoleAuthority();
		for (String aId : aIds) {
			Long id=Long.valueOf(aId);
			roleAuthority.setMenuId(id);
			roleAuthority.setRoleId(r.getId());
			roleAuthorityMapper.insertSelective(roleAuthority);
		}
		
		return num >= 1 ? true : false;
	}

	@Override
	@Transactional
	public boolean updateRole(Long id,String name,String description,String authorityIds) {
		Role r = new Role();
		r.setId(id);
		r.setName(name);
		r.setDescription(description);
		
		RoleAuthorityExample rae = new RoleAuthorityExample();
		rae.or().andRoleIdEqualTo(id);
		roleAuthorityMapper.deleteByExample(rae);
		System.out.println("auids==============="+authorityIds);
		List<String> aIds = Arrays.asList(authorityIds.split(","));
		System.out.println("aids=========="+aIds);
		roleAuthorityMapper.deleteByPrimaryKey(id);
		for (String authorityId : aIds) {
			RoleAuthority ra = new RoleAuthority();
			ra.setRoleId(id);
			Long aId=Long.valueOf(authorityId);
			ra.setMenuId(aId);
			roleAuthorityMapper.insertSelective(ra);
		}
		
		AdminRoleExample are = new AdminRoleExample();
		are.or().andRoleIdEqualTo(id);
		
		List<AdminRole> adminRoles = adminRoleMapper.selectByExample(are);
		for (AdminRole adminRole : adminRoles) {
			redisService.delete(AuthKey.getKey, adminRole.getAdminId().toString());
		}
		
		int num=roleMapper.updateByPrimaryKeySelective(r);
		if(num==0) {
			throw new HelperException(CodeMsg.UPDATE_ROLE_ERROR.getCode(), CodeMsg.UPDATE_ADMIN_ERROR.getMsg());
		}
		return num >=1 ? true :false;
	}

	@Override
	public boolean deleteRole(Long id) {
		int num=roleMapper.deleteByPrimaryKey(id);
		if(num==0) {
			throw new HelperException(CodeMsg.DELETE_ADMIN_ERROR.getCode(), CodeMsg.DELETE_ADMIN_ERROR.getMsg());
		}
		return num >=1 ? true : false;
	}

	@Override
	public boolean deleteRoleBatch(String ids) {
		RoleExample example = new RoleExample();
		Criteria criteria = example.createCriteria();
		criteria.andIdIn(ToolUtil.toList(ids));
		int num=roleMapper.deleteByExample(example);
		if(num==0) {
			throw new HelperException(CodeMsg.DELETE_ADMIN_ERROR.getCode(), CodeMsg.DELETE_ADMIN_ERROR.getMsg());
		}
		return num >=1 ? true : false;
	}

	@Override
	public Role getRoleById(Long id) {
		return roleMapper.selectByPrimaryKey(id);
	}

	@Override
	@Transactional
	public void updateAuthority(Long roleId, List<Long> authorityIds) {
		RoleAuthorityExample rae = new RoleAuthorityExample();
		rae.or().andRoleIdEqualTo(roleId);
		roleAuthorityMapper.deleteByExample(rae);
		
		for (Long authorityId : authorityIds) {
			RoleAuthority ra = new RoleAuthority();
			ra.setRoleId(roleId);
			ra.setMenuId(authorityId);
			roleAuthorityMapper.insertSelective(ra);
		}
		
		AdminRoleExample are = new AdminRoleExample();
		are.or().andRoleIdEqualTo(roleId);
		
		List<AdminRole> adminRoles = adminRoleMapper.selectByExample(are);
		for (AdminRole adminRole : adminRoles) {
			redisService.delete(AuthKey.getKey, adminRole.getAdminId().toString());
		}
	}

	@Override
	public List<RoleAuthority> getRoleAuthority(Long roleId) {
		return roleMapper.getRoleAuthority(roleId);
	}
}
