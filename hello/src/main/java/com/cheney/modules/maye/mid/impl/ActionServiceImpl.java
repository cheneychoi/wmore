package com.cheney.modules.maye.mid.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cheney.modules.maye.db.mapper.ActionMapper;
import com.cheney.modules.maye.db.model.ActionWithBLOBs;
import com.cheney.modules.maye.db.model.out.ActionOut;
import com.cheney.modules.maye.mid.service.ActionService;

@Service
public class ActionServiceImpl implements ActionService {
	@Autowired
	ActionMapper actionMapper;

	@Override
	public List<ActionOut> getActionById(Long UserId) {
		return actionMapper.getActionById(UserId);
	}

	@Override
	public List<ActionOut> getActionByConditions(Integer pageNo, Integer pageSize, String startTime, String endTime,
			String userName) {
		pageNo = (pageNo - 1) + pageSize;
		return actionMapper.getActionByConditions(pageNo, pageSize, startTime, endTime, userName);
	}

	@Override
	public List<ActionOut> getAllActions() {
		return actionMapper.getAllActions();
	}

	@Override
	public void addAction(ActionWithBLOBs actionWithBLOBs) {
		actionMapper.insertSelective(actionWithBLOBs);
	}
}
