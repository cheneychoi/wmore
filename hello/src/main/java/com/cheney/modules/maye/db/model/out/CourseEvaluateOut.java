package com.cheney.modules.maye.db.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "评价-出")
public class CourseEvaluateOut {

	@ApiModelProperty(value = "中间表id")
	private Long id;
	@ApiModelProperty(value = "用户名")
	private String name;
	@ApiModelProperty(value = "课程名称")
	private String courseName;
	@ApiModelProperty(value = "教练专业性")
	private Integer professional;
	@ApiModelProperty(value = "课程功能性")
	private Integer functionality;
	@ApiModelProperty(value = "课程互动性")
	private Integer interaction;
	@ApiModelProperty(value = "预约流畅度")
	private Integer fluency;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public Integer getProfessional() {
		return professional;
	}

	public void setProfessional(Integer professional) {
		this.professional = professional;
	}

	public Integer getFunctionality() {
		return functionality;
	}

	public void setFunctionality(Integer functionality) {
		this.functionality = functionality;
	}

	public Integer getInteraction() {
		return interaction;
	}

	public void setInteraction(Integer interaction) {
		this.interaction = interaction;
	}

	public Integer getFluency() {
		return fluency;
	}

	public void setFluency(Integer fluency) {
		this.fluency = fluency;
	}

}
