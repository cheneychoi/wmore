package com.cheney.modules.maye.mid.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.cheney.modules.maye.db.mapper.UserMapper;
import com.cheney.modules.maye.db.model.User;
import com.cheney.modules.maye.db.model.UserExample;
import com.cheney.modules.maye.mid.service.WeChatLoginService;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.prop.WeChatProperties;
import com.cheney.share.mid.redis.RedisService;
import com.cheney.share.mid.redis.UrlKey;
import com.cheney.share.mid.utils.HttpsUtil;
import com.cheney.share.mid.utils.ToolUtil;

@Service
public class WeChatLoginServiceImpl implements WeChatLoginService {
	Logger logger = LoggerFactory.getLogger("login");

	@Resource
	WeChatProperties props;

	@Resource
	UserMapper userMapper;

	@Resource
	RedisService redisService;

	/*
	 * 获取微信用户信息(备用)
	 */
	public void getWeChatUserInfo(String access_token, String openid) {

		String infoUrl = "https://api.weixin.qq.com/sns/userinfo?access_token=" + access_token + "&openid=" + openid
				+ "&lang=zh_CN";
		String result = HttpsUtil.httpsRequestToString(infoUrl, "GET", null);
		JSONObject jsonObject = JSON.parseObject(result);
		logger.info("userinfo-------------------" + jsonObject.toJSONString());
		String url = jsonObject.getString("headimgurl");
		redisService.set(UrlKey.withExpire(0), openid, url);
	}

	/*
	 * 获取微信用户的OpenId
	 */
	public String getUserOpenId(String code) {

		String key = ToolUtil.getRandNum(6);
		logger.info("begin redisKey-----------------" + key);

		// redisService.set(SessionKey.withExpire(5 * 60 * 1000), key, openid);
		return code;
	}

	@Override
	public String getUserId(String key) {
		logger.info("end redisKey------------" + key);

		if (StringUtils.isEmpty(key)) {
			throw new HelperException(CodeMsg.WECHAT_OPENID_ERROR.getCode(), CodeMsg.WECHAT_OPENID_ERROR.getMsg());
		}

		UserExample ue = new UserExample();
		ue.or().andWechatOpenidEqualTo(key);
		List<User> users = userMapper.selectByExample(ue);

		logger.info("userNum---------------" + users.size());

		if (users.size() == 1) {
			return users.get(0).getId().toString();
		}
		return "0";
	}

}
