package com.cheney.modules.maye.mid.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.cheney.modules.maye.db.mapper.CompanyMapper;
import com.cheney.modules.maye.db.mapper.CourseMapper;
import com.cheney.modules.maye.db.mapper.PictureMapper;
import com.cheney.modules.maye.db.mapper.TaskMapper;
import com.cheney.modules.maye.db.mapper.UserCourseMapper;
import com.cheney.modules.maye.db.mapper.UserMapper;
import com.cheney.modules.maye.db.model.Company;
import com.cheney.modules.maye.db.model.CompanyExample;
import com.cheney.modules.maye.db.model.Course;
import com.cheney.modules.maye.db.model.CourseExample;
import com.cheney.modules.maye.db.model.CourseWithBLOBs;
import com.cheney.modules.maye.db.model.Task;
import com.cheney.modules.maye.db.model.TaskExample;
import com.cheney.modules.maye.db.model.User;
import com.cheney.modules.maye.db.model.UserCourse;
import com.cheney.modules.maye.db.model.UserCourseExample;
import com.cheney.modules.maye.db.model.UserCourseWithBLOBs;
import com.cheney.modules.maye.db.model.out.CourseBackOut;
import com.cheney.modules.maye.db.model.out.CourseDetailOut;
import com.cheney.modules.maye.db.model.out.CourseUserOut;
import com.cheney.modules.maye.db.model.out.MakeCourseOut;
import com.cheney.modules.maye.db.model.out.SignOut;
import com.cheney.modules.maye.mid.service.CourseService;
import com.cheney.modules.maye.mid.service.LogService;
import com.cheney.modules.maye.mid.service.PictureService;
import com.cheney.share.db.model.Pagination;
import com.cheney.share.mid.excel.ExportCourseExcel;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.model.QRCode;
import com.cheney.share.mid.prop.AliOSSProperties;
import com.cheney.share.mid.prop.WeChatProperties;
import com.cheney.share.mid.redis.RedisService;
import com.cheney.share.mid.utils.AliOSSUtils;
import com.cheney.share.mid.utils.ToolUtil;
import com.cheney.share.mid.zxing.CreateZXing;

@Service
public class CourseServiceImpl implements CourseService {

	Logger logger = LoggerFactory.getLogger("course");

	@Resource
	CourseMapper courseMapper;

	@Resource
	PictureMapper pictureMapper;

	@Resource
	CompanyMapper companyMapper;

	@Resource
	UserMapper userMapper;

	@Resource
	TaskMapper taskMapper;

	@Resource
	AliOSSProperties aliOSSProperties;

	@Resource
	UserCourseMapper userCourseMapper;

	@Resource
	WeChatProperties props;

	@Resource
	RedisService redisService;

	@Resource
	PictureService pictureService;
	
	@Resource
	LogService logService;

	/*
	 * 添加课程
	 */
	@Transactional
	public void addCourse(Long userId,String courseName, String courseNameEN, String coursePlace, Date courseDate, Integer task,
			Integer courseNum, String introduction, String crowd, String trainingEffect, String trainingPreparation,
			String otherBusiness, Integer courseAuthority, Integer type, List<MultipartFile> files,
			String companyName) {

		CompanyExample ce = new CompanyExample();
		ce.or().andCompanyNameEqualTo(companyName);
		List<Company> companies = companyMapper.selectByExample(ce);
		if (companies.size() != 1) {
			throw new HelperException(CodeMsg.SELECT_COMPANY_ERROR.getCode(), CodeMsg.SELECT_COMPANY_ERROR.getMsg());
		}
		Long id = companies.get(0).getId();

		CourseWithBLOBs course = new CourseWithBLOBs();
		course.setCourseName(courseName);
		course.setCourseNameen(courseNameEN);
		course.setCoursePlace(coursePlace);
		course.setCourseNum(courseNum);
		course.setCourseDate(courseDate);
		course.setIntroduction(introduction);
		course.setCrowd(crowd);
		course.setTrainingEffect(trainingEffect);
		course.setTrainingPreparation(trainingPreparation);
		course.setCourseAuthority(courseAuthority);

		List<String> result = Arrays.asList(otherBusiness.split("/"));
		JSONArray array = new JSONArray();
		for (int i = 0; i < result.size(); i++) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("otherBusiness", result.get(i));
			array.add(jsonObject);
		}

		course.setOtherBusiness(array.toJSONString());
		course.setType(type);
		course.setCompanyId(id);

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String date = formatter.format(courseDate);
		List<Course> courses = courseMapper.isOneDay(date, id);
		if (courses.size() > 0) {
			course.setIsday(2);
		} else {
			course.setIsday(1);
		}

		String key = null;
		JSONArray jsonArray = new JSONArray();
		if (files.size() == 0) {
			throw new HelperException(CodeMsg.PICTURE_NULL.getCode(), CodeMsg.PICTURE_NULL.getMsg());
		} else {
			for (MultipartFile file : files) {
				JSONObject json = new JSONObject();
				key = pictureService.uploadPicture(file, 0L, 5, "", 0L, 2);
				json.put("pictureUrl", key);
				jsonArray.add(json);
			}
		}

		course.setPictureUrl(jsonArray.toJSONString());
		course.setCreateDate(ToolUtil.getTime(0));
		int num = courseMapper.insertSelective(course);
		if (num == 0) {
			throw new HelperException(CodeMsg.ADD_COURSE_ERROR.getCode(), CodeMsg.ADD_COURSE_ERROR.getMsg());
		}

		if (task == 1) {
			if (ToolUtil.getMonth(courseDate) == ToolUtil.getMonth(new Date())) {
				for (int i = 0; i < 10; i++) {
					course.setCourseDate(ToolUtil.getNextWeek(course.getCourseDate()));
					if (ToolUtil.getMonth(course.getCourseDate()) <= ToolUtil.getMonth(courseDate) + 1) {
						courseMapper.insertSelective(course);
					}
				}
			} else if (ToolUtil.getMonth(courseDate) == ToolUtil.getMonth(new Date()) + 1) {
				for (int i = 0; i < 5; i++) {
					course.setCourseDate(ToolUtil.getNextWeek(course.getCourseDate()));
					if (ToolUtil.getMonth(course.getCourseDate()) < ToolUtil.getMonth(courseDate) + 1) {
						courseMapper.insertSelective(course);
					}
				}
			}
		} else if (task == 2) {
			if (ToolUtil.getMonth(courseDate) == ToolUtil.getMonth(new Date())) {
				course.setCourseDate(ToolUtil.getNextMonth(course.getCourseDate()));
				courseMapper.insertSelective(course);
			}
		}
		if (task != 0) {
			Task taskRecord = new Task();
			taskRecord.setTime(task);
			taskRecord.setCourseId(course.getId());
			taskMapper.insertSelective(taskRecord);
		}
		String msg = "新增课程名为"+courseName+"(所属企业:"+companyName+",上课时间:"+ToolUtil.getDate(courseDate)+")";
		logService.addLog(userId, msg, 2);
		
	}

	@Override
	public List<CourseUserOut> getCourseByCourseAuthority(Long id, String month) {

		User user = userMapper.selectByPrimaryKey(id);
		Long companyId = user.getCompanyId();
		List<CourseUserOut> list = courseMapper.getCourseByCourseAuthority(companyId, user.getPosition(), month,id);
		logger.info(list.size() + "");
		/*if (list.size() != 0) {
			UserCourseExample uce = new UserCourseExample();
			uce.or().andUserIdEqualTo(id);
			List<UserCourse> userCourses = userCourseMapper.selectByExample(uce);

			if (userCourses.size() == 0) {
				for (CourseUserOut courseUserOut : list) {
					courseUserOut.setState(0);
				}
			} else {
				for (CourseUserOut courseUserOut : list) {
					for (UserCourse userCourse : userCourses) {
						if (!courseUserOut.getId().equals(userCourse.getCourseId()) || courseUserOut.getState() == null) {
							courseUserOut.setState(0);
						} else if (courseUserOut.getId().equals(userCourse.getCourseId())) {
							courseUserOut.setState(1);
							break;
						} else if (courseUserOut.getState() == 2) {
							break;
						}
					}
				}
			}
		}*/
		if (list.size() != 0 ) {
			for (int i = 0; i < list.size(); i++) {
				CourseUserOut courseUserOut = list.get(i);
				Long cid = courseUserOut.getId();
				UserCourseExample example = new UserCourseExample();
				example.or().andCourseIdEqualTo(cid).andUserIdEqualTo(id);
				List<UserCourse> userCourses = userCourseMapper.selectByExample(example);
				if (userCourses.size()!=0) {
					UserCourse userCourse = userCourses.get(0);
					Integer state = userCourse.getState();
					courseUserOut.setState(state);
				}else {
					courseUserOut.setState(0);
				}
			}
		}
		
		
		return list;
	}

	@Override
	public Pagination<MakeCourseOut> getCourseByToDay(Integer pageNo, Integer pageSize) {
		Pagination<MakeCourseOut> page = new Pagination<>(pageNo, pageSize);
		pageNo = (pageNo - 1) * pageSize;
		List<MakeCourseOut> list = courseMapper.getCourseByToDay(pageNo, pageSize);
		int count = courseMapper.getTodayCounts();
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public Pagination<Course> getCourseByCancel(Integer pageNo, Integer pageSize) {
		Pagination<Course> page = new Pagination<>(pageNo, pageSize);
		CourseExample cx = new CourseExample();
		List<Course> list = courseMapper.getCourseByCancel();
		int count = (int) courseMapper.countByExample(cx);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public Pagination<MakeCourseOut> getCourseByWeek(Integer pageNo, Integer pageSize) {
		Pagination<MakeCourseOut> page = new Pagination<>(pageNo, pageSize);
		pageNo = (pageNo - 1) * pageSize;
		List<MakeCourseOut> list = courseMapper.getCourseByWeek(pageNo, pageSize);
		int count = courseMapper.counts();
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	/*
	 * 创建二维码 return 二维码路径
	 */
	public String createQRCode(Long courseId) {
		String codeUrl = null;
		Course c = courseMapper.selectByPrimaryKey(courseId);
		codeUrl = c.getCodeUrl();
		if (StringUtils.isEmpty(codeUrl)) {
			// 回调地址 courseId>0
			String url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=" + props.getAppId()
					+ "&redirect_uri=" + props.getBackUrl() + "&response_type=code&scope=snsapi_base&state=" + courseId
					+ "#wechat_redirect";

			// 开始生成二维码
			QRCode qrcode = new QRCode(url);
			String path = this.getClass().getResource("/").getPath();
			String filePath = path + courseId + ".png";
			CreateZXing.createQRCode(qrcode, filePath);

			logger.info("二维码路径-----------------" + filePath);

			// 获取二维码本地路径
			File file = new File(filePath);
			FileInputStream inputStream = null;
			try {
				inputStream = new FileInputStream(file);
			} catch (FileNotFoundException e) {
				throw new HelperException(CodeMsg.FIND_IMAGE_ERROR.getCode(), CodeMsg.FIND_IMAGE_ERROR.getMsg());
			}
			// 转成MultipartFile类型
			MultipartFile multipartFile = null;
			try {
				multipartFile = new MockMultipartFile(courseId + ".png", file.getName(), "application/x-png",
						inputStream);
			} catch (IOException e) {
				e.printStackTrace();
			}

			// 获取OSS配置文件信息
			String ossId = null;
			try {
				ossId = AliOSSUtils.uploadToOSS(aliOSSProperties, multipartFile.getInputStream());
				logger.info("ossID----------------" + ossId);
			} catch (IOException e) {
				e.printStackTrace();
			}

			// 如果ossId不为空 保存进数据库 并且删除本地文件
			CourseWithBLOBs course = new CourseWithBLOBs();
			codeUrl = aliOSSProperties.getBucket() + "." + aliOSSProperties.getGateway() + "/" + ossId;
			logger.info("oss服务器url----------------" + codeUrl);
			course.setId(courseId);
			course.setCodeUrl(codeUrl);
			courseMapper.updateByPrimaryKeySelective(course);
			file.delete();
		}
		return "http://wmore.wearewer.com/QRcode/QRcode.html?key=" + courseId;
	}

	@Override
	public Pagination<Course> getAllCourses(Integer pageNo, Integer pageSize) {
		Pagination<Course> page = new Pagination<>(pageNo, pageSize);
		CourseExample cx = new CourseExample();
		cx.createCriteria();
		pageNo = (pageNo - 1) * pageSize;
		List<Course> list = courseMapper.getAllCourse(pageNo, pageSize);
		int count = (int) courseMapper.countByExample(cx);
		page.setItems(list);
		page.setRecords(count);
		return page;
	}

	@Override
	public List<CourseBackOut> getCourseByCondition(Integer pageNo, Integer pageSize, String startTime, String endTime,
			String courseName) {
		pageNo = (pageNo - 1) * pageSize;
		return courseMapper.getCourseByCondition(pageNo, pageSize, startTime, endTime, courseName);
	}

	@Override
	public Pagination<Course> getCourseByFuzzy(String courseName, String companyName, Integer pageNo,
			Integer pageSize) {
		Pagination<Course> page = new Pagination<>(pageNo, pageSize);
		List<Course> list = courseMapper.getCourseByFuzzy(courseName, companyName);
		// int count = (int) courseMapper.countByExample(list);
		page.setItems(list);
		page.setRecords(list.size());
		return page;
	}

	@Override
	public JSONObject getCourseById(Long id) {
		Course course = courseMapper.selectByPrimaryKey(id);
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("course", course);
		TaskExample taskExample = new TaskExample();
		taskExample.or().andCourseIdEqualTo(id);
		List<Task> tasks = taskMapper.selectByExample(taskExample);
		if (tasks.isEmpty()) {
			jsonObject.put("task", 0);
		} else {
			jsonObject.put("task", tasks.get(0).getTime());
		}
		return jsonObject;
	}

	@Override
	@Transactional
	public void updateCourse(Long id, String courseName, String courseNameEN, String coursePlace, Date courseDate,
			Integer task, Integer courseNum, String introduction, String crowd, String trainingEffect,
			String trainingPreparation, String otherBusiness, Integer courseAuthority, Integer type,
			List<MultipartFile> files, String companyName) {
		CompanyExample ce = new CompanyExample();
		ce.or().andCompanyNameEqualTo(companyName);
		List<Company> companies = companyMapper.selectByExample(ce);
		if (companies.size() != 1) {
			throw new HelperException(CodeMsg.SELECT_COMPANY_ERROR.getCode(), CodeMsg.SELECT_COMPANY_ERROR.getMsg());
		}
		Long companyId = companies.get(0).getId();

		CourseWithBLOBs course = new CourseWithBLOBs();
		course.setId(id);
		course.setCourseName(courseName);
		course.setCourseNameen(courseNameEN);
		course.setCoursePlace(coursePlace);
		course.setCourseNum(courseNum);
		course.setCourseDate(courseDate);
		course.setIntroduction(introduction);
		course.setCrowd(crowd);
		course.setTrainingEffect(trainingEffect);
		course.setTrainingPreparation(trainingPreparation);
		course.setCourseAuthority(courseAuthority);
		List<String> result = Arrays.asList(otherBusiness.split("/"));
		JSONArray array = new JSONArray();
		for (int i = 0; i < result.size(); i++) {
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("otherBusiness", result.get(i));
			array.add(jsonObject);
		}

		course.setOtherBusiness(array.toJSONString());
		course.setType(type);
		course.setCompanyId(companyId);

		String key = null;
		JSONArray jsonArray = new JSONArray();
		if (files.size() != 0) {
			for (MultipartFile file : files) {
				JSONObject json = new JSONObject();
				key = pictureService.uploadPicture(file, 0L, 5, "", 0L, 2);
				json.put("pictureUrl", key);
				jsonArray.add(json);
				course.setPictureUrl(jsonArray.toJSONString());
			}
		}

		course.setUpdateDate(ToolUtil.getTime(0));
		int num = courseMapper.updateByPrimaryKeySelective(course);
		if (num == 0) {
			throw new HelperException(CodeMsg.ADD_COURSE_ERROR.getCode(), CodeMsg.ADD_COURSE_ERROR.getMsg());
		}
		
		UserCourseExample uce = new UserCourseExample();
		uce.or().andCourseIdEqualTo(id);
		UserCourseWithBLOBs userCourse = new UserCourseWithBLOBs();
		userCourse.setCourseDate(courseDate);
		userCourseMapper.updateByExampleSelective(userCourse, uce);
		
		TaskExample taskExample = new TaskExample();
		taskExample.or().andCourseIdEqualTo(id);
		taskMapper.deleteByExample(taskExample);

		if (task == 1) {
			for (int i = 0; i < 10; i++) {
				course.setCourseDate(ToolUtil.getNextWeek(course.getCourseDate()));
				if (ToolUtil.getMonth(course.getCourseDate()) <= ToolUtil.getMonth(courseDate) + 1) {
					courseMapper.updateByPrimaryKeySelective(course);
				}
			}
		} else if (task == 2) {
			course.setCourseDate(ToolUtil.getNextMonth(course.getCourseDate()));
			if (ToolUtil.getMonth(course.getCourseDate()) <= ToolUtil.getMonth(courseDate) + 1) {
				courseMapper.updateByPrimaryKeySelective(course);
			}
		}

		if (task != 0) {
			Task taskRecord = new Task();
			taskRecord.setCourseId(id);
			taskRecord.setTime(task);
			taskMapper.insertSelective(taskRecord);
		}

	}

	@Override
	public CourseDetailOut getCourseDetail(Long userId, Long courseId) {
			CourseDetailOut courseDetailOut = courseMapper.getCouDetail(courseId);
			UserCourseExample example = new UserCourseExample();
			example.or().andCourseIdEqualTo(courseId).andUserIdEqualTo(userId);
			List<UserCourse> userCourses = userCourseMapper.selectByExample(example);
			if (userCourses.size()!=0) {
				UserCourse userCourse = userCourses.get(0);
				Integer state = userCourse.getState();
				courseDetailOut.setState(state);
			}else {
				courseDetailOut.setState(0);
			}
			logger.info("预约课程-----------" + courseDetailOut.getState());
			return courseDetailOut;
	}

	@Override
	public Pagination<MakeCourseOut> getCourseByName(Integer pageNo,  Integer pageSize,String courseName,String startTime,String endTime) {
		Pagination<MakeCourseOut> page = new Pagination<>(pageNo, pageSize);
		pageNo = (pageNo - 1) * pageSize;
		List<MakeCourseOut> list = courseMapper.getCourseByName(courseName, startTime, endTime, pageNo, pageSize);
		page.setItems(list);
		page.setRecords(list.size());
		return page;
	}

	@Override
	public SignOut getSignInfo(Long courseId) {
		return courseMapper.getSignInfo(courseId);
	}

	@Override
	@Transactional
	public boolean delCourse(Long userId,Long id) {
		Course course = courseMapper.selectByPrimaryKey(id);
		int num = courseMapper.deleteByPrimaryKey(id);
		Company company =companyMapper.selectByPrimaryKey(course.getCompanyId());
		String companyName = "";
		if (company == null) {
			companyName = "无企业";
		}else {
			companyName = company.getCompanyName();
		}
		String date = ToolUtil.getDate(course.getCourseDate());
		String courseName = course.getCourseName();
		String msg = "删除课程名为"+courseName+"(所属企业:"+companyName+",上课时间:"+date+")";
		
		String picture = course.getPictureUrl();
		JSONArray jsonArray = JSONArray.parseArray(picture);
		for (int i = 0; i < jsonArray.size(); i++) {
			JSONObject jsonObject = (JSONObject) jsonArray.get(i);
			String pic = jsonObject.getString("pictureUrl");
			pic = pic.substring(pic.indexOf("/")+1);
			AliOSSUtils.removeFromOSS(aliOSSProperties, pic);
		}
		
		logService.addLog(userId, msg, 3);
		return num > 0 ? true : false;
	}

	@Override
	public List<MakeCourseOut> getCourseByName1(String courseName,String startTime,String endTime) {
		return courseMapper.getCourseByName1(courseName, startTime, endTime);
	}

	@Override
	public List<MakeCourseOut> getCompanyByName(Long companyId) {
		return courseMapper.getCompanyByName(companyId);
	}

	@Override
	public List<MakeCourseOut> getCompanyByName1(Long companyId) {
		return courseMapper.getCompanyByName1(companyId);
	}

	@Override
	public List<Course> getCourseByCompany(Long companyId) {
		return courseMapper.getCourseByCompany(companyId);
	}

	@Override
	public String exportCourse(String courseName, String startTime, String endTime, Long companyId) {
		List<MakeCourseOut> list = new ArrayList<>();
		if (companyId == 0 && StringUtils.isEmpty(startTime)&& StringUtils.isEmpty(endTime)&& StringUtils.isEmpty(courseName)) {
			list= courseMapper.getCourseByDay();
		}else {
			list = courseMapper.getCourse(courseName,startTime,endTime,companyId);
		}
		ExportCourseExcel excel = new ExportCourseExcel();
		return excel.exportExcel(list);
	}
	
}
