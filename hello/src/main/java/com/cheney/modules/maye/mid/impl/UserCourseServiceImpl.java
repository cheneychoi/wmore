package com.cheney.modules.maye.mid.impl;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.cheney.modules.maye.db.mapper.CourseMapper;
import com.cheney.modules.maye.db.mapper.UserCourseMapper;
import com.cheney.modules.maye.db.mapper.UserMapper;
import com.cheney.modules.maye.db.model.Course;
import com.cheney.modules.maye.db.model.CourseWithBLOBs;
import com.cheney.modules.maye.db.model.Notice;
import com.cheney.modules.maye.db.model.NoticeMasterplate;
import com.cheney.modules.maye.db.model.User;
import com.cheney.modules.maye.db.model.UserCourseWithBLOBs;
import com.cheney.modules.maye.db.model.UserExample;
import com.cheney.modules.maye.db.model.in.EvaluateIn;
import com.cheney.modules.maye.db.model.out.CourseEvaluateOut;
import com.cheney.modules.maye.db.model.out.EvaluateOut;
import com.cheney.modules.maye.mid.service.NoticeMasterplateService;
import com.cheney.modules.maye.mid.service.NoticeService;
import com.cheney.modules.maye.mid.service.PointService;
import com.cheney.modules.maye.mid.service.UserCourseService;
import com.cheney.modules.maye.mid.service.UserService;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;
import com.cheney.share.mid.utils.ToolUtil;

@Service
public class UserCourseServiceImpl implements UserCourseService {
	@Autowired
	UserCourseMapper userCourseMapper;

	@Autowired
	CourseMapper courseMapper;

	@Autowired
	UserMapper userMapper;

	@Autowired
	NoticeService noticeService;

	@Autowired
	PointService pointService;
	
	@Autowired
	UserService userService;

	@Autowired
	NoticeMasterplateService noticeMasterplateService;

	@Override
	@Transactional
	public Notice insertUserCourse(UserCourseWithBLOBs ucb) {
		Long courseId = ucb.getCourseId();
		Long userId = ucb.getUserId();
		
		CourseWithBLOBs cBloBs = courseMapper.selectByPrimaryKey(courseId);
		Integer type = cBloBs.getCourseState();
		
		// 判断是否可约 约后人数减1
		if (type != 1) {
			try {
				ucb.setState(1);
				ucb.setCreateDate(ToolUtil.getTime(0));
				ucb.setCourseDate(courseMapper.selectByPrimaryKey(courseId).getCourseDate());
				userCourseMapper.insertSelective(ucb);
				Integer num = cBloBs.getCourseNum();
				num = num - 1;
				cBloBs.setCourseNum(num);
				if (num == 0 ) {
					cBloBs.setCourseState(1);
				}
				courseMapper.updateByPrimaryKeySelective(cBloBs);
			} catch (Exception e) {
				e.printStackTrace();
			}
		} else {
			throw new HelperException(CodeMsg.ABOUT_NUM_ERROR.getCode(), CodeMsg.ABOUT_NUM_ERROR.getMsg());
		}
		pointService.plusPoint(5, userId, 1);

		NoticeMasterplate noticeMasterplate = noticeMasterplateService.getNoticeMasterplateByType(1);

		CourseWithBLOBs courseWithBLOBs = courseMapper.selectByPrimaryKey(courseId);
		String courseName = courseWithBLOBs.getCourseName();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
		String courseDate = sdf.format(courseWithBLOBs.getCourseDate()).toString();
		String coursePlace = courseWithBLOBs.getCoursePlace();

		String title = noticeMasterplate.getTitle();
		String content = noticeMasterplate.getContent();

		title = ToolUtil.replaceNotice(title, courseName, courseDate, coursePlace, "");
		content = ToolUtil.replaceNotice(content, courseName, courseDate, coursePlace, "");

		Notice notice = new Notice();
		notice.setUserId(userId);
		notice.setTitle(title);
		notice.setContent(content);
		notice.setCreateDate(ToolUtil.getTime(0));
		Course course = courseMapper.selectByPrimaryKey(courseId);
		Date date = course.getCourseDate();
		Calendar ca = Calendar.getInstance();// 得到一个Calendar的实例
		ca.setTime(date); // 设置时间为当前时间
		ca.add(Calendar.DAY_OF_YEAR, -1); // 年份减1
		Date lastDay = ca.getTime(); // 结果
		notice.setNoticeDate(lastDay);
		notice.setIsFirst(1);
		noticeService.addNotice(notice);

		Notice backNotice = new Notice();
		backNotice.setUserId(userId);
		backNotice.setTitle(title);
		backNotice.setContent(content);
		backNotice.setCreateDate(ToolUtil.getTime(0));
		noticeService.addNotice(backNotice);

		User user = userMapper.selectByPrimaryKey(userId);
		userService.sendOrderSMS(user.getMobile(), cBloBs.getCourseName(), ToolUtil.getDate(cBloBs.getCourseDate()), cBloBs.getCoursePlace());
		
		return backNotice;
	}

	/*
	 * 上课签到
	 */
	@Override
	public void sign(Long courseId, String openId) {
		HashMap<String, Object> map = new HashMap<>();
		map.put("openId", openId);
		map.put("courseId", courseId);
		map.put("updateDate", ToolUtil.getTime(0));
		userCourseMapper.sign(map);
		
		UserExample ue = new UserExample();
		ue.or().andWechatOpenidEqualTo(openId);
		List<User> users = userMapper.selectByExample(ue);
		if (users.size() == 1) {
			pointService.plusPoint(1, users.get(0).getId(), 1);
		}
	}

	/*
	 * 课程评分
	 */
	@Override
	@Transactional
	public Notice setScore(EvaluateIn in) {
		
		UserCourseWithBLOBs userCourseWithBLOBs = new UserCourseWithBLOBs();
		userCourseWithBLOBs.setCourseId(in.getCourseId());
		userCourseWithBLOBs.setId(in.getId());
		userCourseWithBLOBs.setUserId(in.getUserId());
		userCourseWithBLOBs.setFluency(in.getFluency());
		userCourseWithBLOBs.setFunctionality(in.getFunctionality());
		userCourseWithBLOBs.setInteraction(in.getInteraction());
		userCourseWithBLOBs.setProfessional(in.getProfessional());
		userCourseWithBLOBs.setIsEvaluate(1);
		userCourseWithBLOBs.setState(2);

		int num = userCourseMapper.updateByPrimaryKeySelective(userCourseWithBLOBs);

		if (num == 0) {
			throw new HelperException(CodeMsg.EVALUATE_COURSE_ERROR.getCode(), CodeMsg.EVALUATE_COURSE_ERROR.getMsg());
		}
		
		pointService.plusPoint(5, userCourseWithBLOBs.getUserId(), 1);

		NoticeMasterplate noticeMasterplate = noticeMasterplateService.getNoticeMasterplateByType(7);
		String title = noticeMasterplate.getTitle();
		String content = noticeMasterplate.getContent();

		CourseWithBLOBs courseWithBLOBs = courseMapper.selectByPrimaryKey(userCourseWithBLOBs.getCourseId());
		String courseName = courseWithBLOBs.getCourseName();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh-mm-ss");
		String courseDate = sdf.format(courseWithBLOBs.getCourseDate()).toString();
		String coursePlace = courseWithBLOBs.getCoursePlace();

		title = ToolUtil.replaceNotice(title, courseName, courseDate, coursePlace, "");
		content = ToolUtil.replaceNotice(content, courseName, courseDate, coursePlace, "");

		Notice notice = new Notice();
		notice.setUserId(userCourseWithBLOBs.getUserId());
		notice.setTitle(title);
		notice.setContent(content);
		notice.setCreateDate(ToolUtil.getTime(0));

		return notice;
	}

	@Override
	public List<EvaluateOut> getIsEvaluateCourse(Long userId) {
		return userCourseMapper.getIsEvaluateCourse(userId);
	}

	@Override
	public List<CourseEvaluateOut> getEvaluate(Long courseId) {
		return userCourseMapper.getEvaluate(courseId);
	}
}