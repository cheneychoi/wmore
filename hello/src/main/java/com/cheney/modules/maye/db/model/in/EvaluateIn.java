package com.cheney.modules.maye.db.model.in;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.UserCourseWithBLOBs;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "评价课程-入")
public class EvaluateIn {

	@ApiModelProperty(value = "中间表id")
	private Long id;
	@ApiModelProperty(value = "用户id")
	private Long userId;
	@ApiModelProperty(value = "课程id")
	private Long courseId;
	@ApiModelProperty(value = "课程名称")
	private String courseName;
	@ApiModelProperty(value = "课程名称英语")
	private String courseNameEN;
	@ApiModelProperty(value = "教练专业性")
	private Integer professional;
	@ApiModelProperty(value = "课程功能性")
	private Integer functionality;
	@ApiModelProperty(value = "课程互动性")
	private Integer interaction;
	@ApiModelProperty(value = "预约流畅度")
	private Integer fluency;

	public UserCourseWithBLOBs toEvaluateIn() {
		UserCourseWithBLOBs userCourseWithBLOBs = new UserCourseWithBLOBs();
		BeanUtils.copyProperties(this, userCourseWithBLOBs);
		return userCourseWithBLOBs;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getUserId() {
		return userId;
	}

	public void setUserId(Long userId) {
		this.userId = userId;
	}

	public Long getCourseId() {
		return courseId;
	}

	public void setCourseId(Long courseId) {
		this.courseId = courseId;
	}

	public String getCourseName() {
		return courseName;
	}

	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	public String getCourseNameEN() {
		return courseNameEN;
	}

	public void setCourseNameEN(String courseNameEN) {
		this.courseNameEN = courseNameEN;
	}

	public Integer getProfessional() {
		return professional;
	}

	public void setProfessional(Integer professional) {
		this.professional = professional;
	}

	public Integer getFunctionality() {
		return functionality;
	}

	public void setFunctionality(Integer functionality) {
		this.functionality = functionality;
	}

	public Integer getInteraction() {
		return interaction;
	}

	public void setInteraction(Integer interaction) {
		this.interaction = interaction;
	}

	public Integer getFluency() {
		return fluency;
	}

	public void setFluency(Integer fluency) {
		this.fluency = fluency;
	}

	@Override
	public String toString() {
		return "EvaluateIn [id=" + id + ", userId=" + userId + ", courseId=" + courseId + ", courseName=" + courseName
				+ ", courseNameEN=" + courseNameEN + ", professional=" + professional + ", functionality="
				+ functionality + ", interaction=" + interaction + ", fluency=" + fluency + "]";
	}

}
