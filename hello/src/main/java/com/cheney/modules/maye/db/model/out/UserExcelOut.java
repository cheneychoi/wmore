package com.cheney.modules.maye.db.model.out;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "会员表格-出")
public class UserExcelOut {
	@ApiModelProperty(value = "真实姓名")
	private String name;

	@ApiModelProperty(value = "邮箱")
	private String email;

	@ApiModelProperty(value = "手机号")
	private String mobile;

	@ApiModelProperty(value = "性别(1 男  2 女)")
	private Integer sex;

	@ApiModelProperty(value = "城市")
	private String city;

	@ApiModelProperty(value = "行业")
	private String industry;

	@ApiModelProperty(value = "爱好")
	private String hobby;

	@ApiModelProperty(value = "所属公司")
	private String companyName;
	
	@ApiModelProperty(value = "公司代码")
	private String companyCode;

	@ApiModelProperty(value = "职位")
	private Integer position;
	
	@ApiModelProperty(value="积分")
	private Integer points;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public Integer getSex() {
		return sex;
	}

	public void setSex(Integer sex) {
		this.sex = sex;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public String getHobby() {
		return hobby;
	}

	public void setHobby(String hobby) {
		this.hobby = hobby;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCompanyCode() {
		return companyCode;
	}

	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}

	public Integer getPosition() {
		return position;
	}

	public void setPosition(Integer position) {
		this.position = position;
	}

	public Integer getPoints() {
		return points;
	}

	public void setPoints(Integer points) {
		this.points = points;
	}

	@Override
	public String toString() {
		return "UserExcelOut [name=" + name + ", email=" + email + ", mobile=" + mobile + ", sex=" + sex + ", city="
				+ city + ", industry=" + industry + ", hobby=" + hobby + ", companyName=" + companyName
				+ ", companyCode=" + companyCode + ", position=" + position + ", points=" + points + "]";
	}

	
}
