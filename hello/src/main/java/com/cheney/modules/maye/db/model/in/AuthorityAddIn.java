package com.cheney.modules.maye.db.model.in;

import java.util.Date;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Authority;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel(value = "后台添加权限-入")
public class AuthorityAddIn {
	@ApiModelProperty(value = "路径")
	private String url;
	@ApiModelProperty(value = "菜单样式")
	private String menuClass;
	@ApiModelProperty(value = "菜单名称")
	private String menuName;
	@ApiModelProperty(value = "上级菜单编码")
	private String parentMenucode;
	@ApiModelProperty(value = "菜单类型（1-左导航栏 2-按钮）")
	private Integer menuType;
	@ApiModelProperty(value = "创建时间")
	@JsonFormat(pattern = "yyyy-MM-dd", timezone = "GMT+8")
	private Date createTime;
	public Authority toAuthorityAddIn() {
		Authority a = new Authority();
		BeanUtils.copyProperties(this, a);
		return a;
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getMenuClass() {
		return menuClass;
	}
	public void setMenuClass(String menuClass) {
		this.menuClass = menuClass;
	}
	public String getMenuName() {
		return menuName;
	}
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	public String getParentMenucode() {
		return parentMenucode;
	}
	public void setParentMenucode(String parentMenucode) {
		this.parentMenucode = parentMenucode;
	}
	public Integer getMenuType() {
		return menuType;
	}
	public void setMenuType(Integer menuType) {
		this.menuType = menuType;
	}
	public Date getCreateTime() {
		return createTime;
	}
	public void setCreateTime(Date createTime) {
		this.createTime = createTime;
	}
	
}
