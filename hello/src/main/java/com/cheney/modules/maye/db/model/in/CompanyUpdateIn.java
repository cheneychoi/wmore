package com.cheney.modules.maye.db.model.in;

import org.springframework.beans.BeanUtils;

import com.cheney.modules.maye.db.model.Company;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
@ApiModel("修改企业--入")
public class CompanyUpdateIn {
	@ApiModelProperty(value = "企业id")
	   private Long id;
		@ApiModelProperty(value = "公司名称")
	   private String companyName;
		@ApiModelProperty(value = "公司代号")
	   private String companyCode;
		@ApiModelProperty(value = "公司人数")
	   private Integer companyNumber;
		@ApiModelProperty(value = "企业状态：1：冻结，2：未冻结")
	   private Integer companyState;
		@ApiModelProperty(value="")
		private Long pictureId;
		public Company toCompanyUpdateIn() {
			Company c= new Company();
			BeanUtils.copyProperties(this, c);
			return c;
		}
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getCompanyName() {
			return companyName;
		}
		public void setCompanyName(String companyName) {
			this.companyName = companyName;
		}
		public String getCompanyCode() {
			return companyCode;
		}
		public void setCompanyCode(String companyCode) {
			this.companyCode = companyCode;
		}
		public Integer getCompanyNumber() {
			return companyNumber;
		}
		public void setCompanyNumber(Integer companyNumber) {
			this.companyNumber = companyNumber;
		}
		public Integer getCompanyState() {
			return companyState;
		}
		public void setCompanyState(Integer companyState) {
			this.companyState = companyState;
		}
		public Long getPictureId() {
			return pictureId;
		}
		public void setPictureId(Long pictureId) {
			this.pictureId = pictureId;
		}
		
		
}
