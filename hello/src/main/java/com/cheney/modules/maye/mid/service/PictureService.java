package com.cheney.modules.maye.mid.service;

import java.util.List;

import org.springframework.web.multipart.MultipartFile;

import com.cheney.modules.maye.db.model.Picture;
import com.cheney.share.db.model.Pagination;

/** 
 * 图片模块
 * @ClassName: PictureService 
 * @Description: TODO
 * @author: WangBo
 * @date: 2018年7月6日 上午11:31:33  
 */
public interface PictureService {
	
	/**
	 * 获取大Banner图片 
	* @return List<String>
	* @author WangBo
	* @date 2018年7月6日上午11:36:18
	*/ 
	List<Picture> getBigBannerPicture(int type);
	/**
	 * 获取小Banner图片
	 */
	List<Picture> getSmallBanner(int type);
	/**
	 * 点击相应课程展示课程介绍图文详情
	 * @param p
	 * @return
	 */
	List<Picture> getPicture(Long id);
	
	/** 
	 * 上架或下架banner
	* @param isBanner void
	* @author WangBo
	* @date 2018年7月17日下午6:24:59
	*/ 
	void isBanner(Long pictureId,Integer isBanner);
	
	/**
     * 获取所有banner图片
     */
	Pagination<Picture> getBanner(Integer pageNo, Integer pageSize);
	
	/**
	 * 上传Banner 
	*  void
	* @author WangBo
	* @date 2018年7月27日上午10:23:51
	*/ 
	void uploadBanner(MultipartFile file,String url,Integer type);
	
	/** 
	 * 上传图片
	*  void
	* @author WangBo
	* @date 2018年7月27日上午10:24:22
	*/ 
	String uploadPicture(MultipartFile file,Long pictureId,Integer type,String url,Long courseId,Integer isBanner);
	
	/**
	 * 获取大Banner 
	* @return List<String>
	* @author WangBo
	* @date 2018年7月6日上午11:36:18
	*/ 
	List<Picture> getBigBanner(int type);
	/**
	 * 删除图片
	 */
	boolean delPic(Long id);
	/** 
	 * 根据ID获取Banner
	* @param id
	* @return Object
	* @author WangBo
	* @date 2018年8月28日下午3:29:13
	*/ 
	Picture getBannerById(Long id);
	/**修改Banner 
	* @param id
	* @param contentUrl
	* @param type void
	* @author WangBo
	* @date 2018年8月28日下午3:33:07
	*/ 
	void editBanner(Long id, String contentUrl, Integer type);

}