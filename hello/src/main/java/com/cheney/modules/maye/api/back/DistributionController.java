package com.cheney.modules.maye.api.back;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.mid.service.DistributionService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "分配-后台")
@Controller
@RequestMapping(value = "/wmore_back")
public class DistributionController extends BaseController {

	@Autowired
	DistributionService distributionService;

	@ApiOperation(value = "为管理员分配权限", notes = "用于为管理员分配权限")
	@RequestMapping(value = "/distribution/addRole", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> addNoticeMasterplate(@ApiParam(value = "用户ID") @RequestParam("adminId") Long adminId,
			@ApiParam(value = "角色ID") @RequestParam("roleId") Long roleId) {
		distributionService.addRole(adminId, roleId);
		return success();
	}

	@ApiOperation(value = "根据ID查询角色", notes = "用于根据ID查询角色")
	@RequestMapping(value = "/distribution/getRoleByAdminId", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getRoleByAdminId(@ApiParam(value = "用户ID") @RequestParam("adminId") Long adminId) {
		return success(distributionService.getRoleByAdminId(adminId));
	}

	@ApiOperation(value = "为角色分配权限", notes = "用于为为角色分配权限")
	@RequestMapping(value = "/distribution/addAuthority", method = { RequestMethod.POST })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> addAuthority(@ApiParam(value = "角色ID") @RequestParam("roleId") Long roleId,
			@ApiParam(value = "权限ID组") @RequestParam("authIds") String authIds) {
		distributionService.addAuthority(roleId, authIds);
		return success();
	}

	@ApiOperation(value = "根据角色ID查询权限", notes = "用于根据角色ID查询权限")
	@RequestMapping(value = "/distribution/getAuthorityByRoleId", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getAuthorityByRoleId(@ApiParam(value = "角色ID") @RequestParam("roleId") Long roleId) {

		return success(distributionService.getAuthorityByRoleId(roleId));
	}
	
	
	@ApiOperation(value = "给用户移除角色", notes = "给用户移除角色")
	@RequestMapping(value = "/distribution/deleteUserRole", method = { RequestMethod.DELETE })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> deleteUserRole(@ApiParam("后台用户id") @RequestParam(value="adminId")Long adminId,@ApiParam(value = "角色ID") @RequestParam("roleId") Long roleId) {

		return success(distributionService.deleteUserRole(adminId, roleId));
	}
	
	
	@ApiOperation(value = "给角色移除权限", notes = "给角色移除权限")
	@RequestMapping(value = "/distribution/deleteRoleAut", method = { RequestMethod.DELETE })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> deleteRoleAut(@ApiParam(value = "角色ID") @RequestParam("roleId") Long roleId,@ApiParam("权限id") @RequestParam("menuId")Long menuId) {
		return success(distributionService.deleteRoleAut(roleId, menuId));
	}
}
