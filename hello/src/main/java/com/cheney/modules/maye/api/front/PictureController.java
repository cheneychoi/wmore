package com.cheney.modules.maye.api.front;

import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.modules.maye.db.model.Picture;
import com.cheney.modules.maye.db.model.out.BannerPictureOut;
import com.cheney.modules.maye.db.model.out.PictureOut;
import com.cheney.modules.maye.mid.service.PictureService;
import com.cheney.share.api.controller.BaseController;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@Api(value = "图片")
@Controller
@RequestMapping(value = "/wmore")
public class PictureController extends BaseController {

	Logger logger = LoggerFactory.getLogger("login");

	@Autowired
	PictureService pictureService;

	@ApiOperation(value = "获取图片路径", notes = "返回大Banner图片", response = BannerPictureOut.class)
	@RequestMapping(value = "/picture/getBigBannerPictures", method = { RequestMethod.GET })
	@CrossOrigin
	@ResponseBody
	public Map<String, Object> getBigBannerPicture() {
		logger.info("------------进入获取图片Banner的方法---------------");
		return success(pictureService.getBigBannerPicture(0));
	}

	@ApiOperation(value = "获取图片路径", notes = "返回小Banner图片", response = BannerPictureOut.class)
	@RequestMapping(value = "/picture/getSmallBannerPictures", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin // @ApiParam(value="Banner的类型") @RequestParam(value="picType")Integer type
	public Map<String, Object> getSmallBannerPicture() {
		return success(pictureService.getBigBannerPicture(1));
	}

	/**
	 * 点击相应课程展示课程介绍图文详情
	 * 
	 * @param courseId
	 * @return
	 */
	@ApiOperation(value = "点击相应课程展示课程介绍图文详情", notes = "返回数据", response = PictureOut.class)
	@RequestMapping(value = "/picture/getPicture", method = { RequestMethod.GET })
	@ResponseBody
	@CrossOrigin
	public Map<String, Object> getCoursePicture(@ApiParam(value = "课程id") @RequestParam("id") Long id) {
		logger.info("-----------进入点击相应课程展示课程介绍图文详情的方法---------------");
		logger.info("课程id----------------" + id);
		List<Picture> pictures = pictureService.getPicture(id);
		logger.info("返回的数据----------------" + pictures);
		return success(pictures);
	}
	
	
	@ApiOperation(value = "获取大Banner", notes = "返回大Banner图片", response = BannerPictureOut.class)
	@RequestMapping(value = "/picture/getBigBanner", method = { RequestMethod.GET })
	@CrossOrigin
	@ResponseBody
	public Map<String, Object> getBigBanner() {
		logger.info("------------进入获取图片Banner的方法---------------");
		return success(pictureService.getBigBanner(0));
	}
	
	@ApiOperation(value = "获取小Banner", notes = "返回小Banner图片", response = BannerPictureOut.class)
	@RequestMapping(value = "/picture/getSmallBanner", method = { RequestMethod.GET })
	@CrossOrigin
	@ResponseBody
	public Map<String, Object> getSmallBanner() {
		logger.info("------------进入获取图片Banner的方法---------------");
		return success(pictureService.getBigBanner(1));
	}
	

}
