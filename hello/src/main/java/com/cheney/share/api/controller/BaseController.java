package com.cheney.share.api.controller;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cheney.share.mid.exception.HelperException;

public class BaseController {
	public final Logger logger = LoggerFactory.getLogger(BaseController.class);

	/**
	 * 功能描述： 接口调用成功,code为0
	 * 
	 * @param data
	 * @return 返回成功的JSON数据
	 * @author chenheng 2016 Apr 25, 2016 5:35:03 PM
	 */
	public Map<String, Object> success(Object data) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("code", 0);
		result.put("data", data);
		result.put("message", "success");
		return result;
	}

	/**
	 * 功能描述： 接口调用成功,code为0
	 * 
	 * @return
	 * @author chenheng 2016 Apr 25, 2016 5:35:54 PM
	 */
	public Map<String, Object> success() {
		return success(null);
	}

	/**
	 * 
	 * 功能描述： 接口调用失败, code为-1
	 * 
	 * @param data
	 * @return 返回错误的json数据
	 * @author chenheng 2016 Apr 25, 2016 5:37:11 PM
	 */
	public Map<String, Object> fail(Object data) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("code", -1);
		result.put("data", data);
		result.put("message", data);
		return result;
	}

	/**
	 * 功能描述: 接口调用失败, code为错误码
	 * 
	 * @param code
	 * @param data
	 * @return
	 */
	public Map<String, Object> fail(int code, Object data) {
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("code", code);
		result.put("data", data);
		result.put("message", data);
		return result;
	}

	/**
	 * 
	 * 功能描述： 接口调用失败, code为-1
	 * 
	 * @return
	 * @author chenheng 2016 Apr 25, 2016 5:39:48 PM
	 */
	public Map<String, Object> fail() {
		return fail(null);
	}

	/*
	 * @ExceptionHandler public @ResponseBody
	 * 
	 * @ResponseStatus(value = HttpStatus.UNAUTHORIZED) Map<String, Object>
	 * handleUnauthorizedException(UnauthorizedException e) {
	 * logger.error("!!!! unauthorized exception[code:{}, message:{}]",
	 * e.getCode(), e.getMessage()); return fail(e.getMessage()); }
	 */

	@ExceptionHandler
	public @ResponseBody Map<String, Object> handleException(Exception e) {
		logger.error("!!!! exception:", e);
		return fail("服务出错啦，请稍后再试！");
	}

	@ExceptionHandler
	public @ResponseBody Map<String, Object> handleHelperException(HelperException e) {
		logger.error("!!!! exception: {}", e.getMessage());
		return fail(e.getCode(), e.getMessage());
	}

	public String getClientHost(HttpServletRequest request) {
		String ip = request.getHeader("x-forwarded-for");
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getHeader("WL-Proxy-Client-IP");
		}
		if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
			ip = request.getRemoteAddr();
		}
		return ip.equals("0:0:0:0:0:0:0:1") ? "127.0.0.1" : ip;
	}
}
