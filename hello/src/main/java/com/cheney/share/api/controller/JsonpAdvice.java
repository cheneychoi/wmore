package com.cheney.share.api.controller;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.AbstractJsonpResponseBodyAdvice;

@ControllerAdvice(basePackages = "com.xedu.aio")
public class JsonpAdvice extends AbstractJsonpResponseBodyAdvice {
	
	public JsonpAdvice() {
		super("callback", "jsonp", "jsoncallback");
	}
}
