package com.cheney.share.api.controller;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface Log {
	/**
	 * 操作动作
	 * @return
	 */
	String action() default "";
	/**
	 * 操作备注
	 * @return
	 */
	String remark() default "";
}
