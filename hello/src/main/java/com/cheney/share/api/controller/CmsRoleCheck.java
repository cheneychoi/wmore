package com.cheney.share.api.controller;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface CmsRoleCheck {

	/**
	 * 
	 * @return
	 */
	String value() default "";
	
	/**
	 * 权重值
	 * @return
	 */
	int weight() default 0;

}
