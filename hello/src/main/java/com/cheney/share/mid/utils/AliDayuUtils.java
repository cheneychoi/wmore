package com.cheney.share.mid.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;

public class AliDayuUtils {

	static Logger logger = LoggerFactory.getLogger(AliDayuUtils.class);

	static final String product = "Dysmsapi";// 短信API产品名称（短信产品名固定，无需修改）
	static final String domain = "dysmsapi.aliyuncs.com";// 短信API产品域名（接口地址固定，无需修改）

	static {
		// 设置超时时间-可自行调整
		System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
		System.setProperty("sun.net.client.defaultReadTimeout", "10000");
	}

//	@Deprecated
//	public static void sendSms(AliDayuProperties props, String phone, String templateCode, JSONObject param) {
//		sendSms(props, phone, templateCode, param, null);
//	}

//	@Deprecated
//	public static void sendSms(AliDayuProperties props, String phone, String templateCode, JSONObject param,
//			String extend) {
//		TaobaoClient client = new DefaultTaobaoClient(props.getGateway(), props.getAppId(), props.getAppKey());
//		AlibabaAliqinFcSmsNumSendRequest req = new AlibabaAliqinFcSmsNumSendRequest();
//		if (StringUtils.isNotBlank(extend)) {
//			req.setExtend(extend);
//		}
//		req.setSmsType("normal");
//		req.setSmsFreeSignName(props.getSign());
//		if (param != null) {
//			req.setSmsParamString(param.toJSONString());
//		}
//		req.setRecNum(phone);
//		req.setSmsTemplateCode(templateCode);
//		try {
//			AlibabaAliqinFcSmsNumSendResponse rsp = client.execute(req);
//			logger.info(">>>> sms body: {}", rsp.getBody());
//		} catch (Exception e) {
//			logger.warn("!!!! api exception: {}", e);
//		}
//	}

	/**
	 * 短信发送API(SendSms)---JAVA<br>
	 * wiki: https://help.aliyun.com/document_detail/55284.html?spm=5176.doc55288.2.3.iC1Qfo<br>
	 * 
	 * @param props 阿里短信配制参数
	 * @param phone 手机号
	 * @param templateCode 模块
	 * @param param json参数，替换模板中的变量
	 * @param extend 提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者，目前为phone_code表的主键
	 */
	public static void sendSmsV2(com.cheney.share.mid.prop.AliDayuProperties props, String phone, String templateCode, JSONObject param,
			String extend) {
		IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", props.getAppId(), props.getAppKey());
		try {
			DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", product, domain);
			IAcsClient acsClient = new DefaultAcsClient(profile);
			 //组装请求对象
			 SendSmsRequest request = new SendSmsRequest();
			 //使用post提交
			 request.setMethod(MethodType.POST);
			 //必填:待发送手机号。支持以逗号分隔的形式进行批量调用，批量上限为1000个手机号码,批量调用相对于单条调用及时性稍有延迟,验证码类型的短信推荐使用单条调用的方式
			 request.setPhoneNumbers(phone);
			 //必填:短信签名-可在短信控制台中找到
			 request.setSignName(props.getSign());
			 //必填:短信模板-可在短信控制台中找到
			 request.setTemplateCode(templateCode);
			 //可选:模板中的变量替换JSON串,如模板内容为"亲爱的${name},您的验证码为${code}"时,此处的值为
			 //友情提示:如果JSON中需要带换行符,请参照标准的JSON协议对换行符的要求,比如短信内容中包含\r\n的情况在JSON中需要表示成\\r\\n,否则会导致JSON在服务端解析失败
			 request.setTemplateParam(param.toJSONString());
			 //可选-上行短信扩展码(无特殊需求用户请忽略此字段)
			 //request.setSmsUpExtendCode("90997");
			 //可选:outId为提供给业务方扩展字段,最终在短信回执消息中将此值带回给调用者
			 request.setOutId(extend);
			//请求失败这里会抛ClientException异常
			SendSmsResponse sendSmsResponse = acsClient.getAcsResponse(request);
			if(sendSmsResponse.getCode() != null && sendSmsResponse.getCode().equals("OK")) {
				logger.info(">>>> send code success: {}-{}", phone, param.toJSONString());
			}
		} catch (ClientException e) {
			logger.warn("!!!! api exception: {}", e);
		}
	}
}
