/**********************************************************************
 **********************************************************************
 **    Project Name : QRcode
 **    Package Name : zxing								 
 **    Type    Name : CreateQRCode.java							     	
 **    Create  Time : 2018年1月10日								
 ** 																
 **    (C) Copyright Zensvision Information Technology Co., Ltd.	 
 **            Corporation 2016 All Rights Reserved.				
 **********************************************************************
 **	     注意： 本内容仅限于上海仁视信息科技有限公司内部使用，禁止转发		 **
 **********************************************************************
 */
package com.cheney.share.mid.zxing;

import java.io.File;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

import com.cheney.share.mid.model.QRCode;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

/**
 * @author wangbo
 * @create 2018年1月10日下午2:58:58
 * @version 1.0
 */
public class CreateZXing {
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void createQRCode(QRCode qrCode,String filePath) {

		//定义二维码参数
				Map hints=new HashMap();
				hints.put(EncodeHintType.CHARACTER_SET, "utf-8");//设置编码
				hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);//设置容错等级
				hints.put(EncodeHintType.MARGIN, 2);//设置边距默认是5
				
				try {
					BitMatrix bitMatrix=new MultiFormatWriter().encode(qrCode.getContent(), BarcodeFormat.QR_CODE, qrCode.getWidth(), qrCode.getHeight(), hints);
					Path path = new File(filePath).toPath();
					MatrixToImageWriter.writeToPath(bitMatrix, "png", path);//写到指定路径下
				} catch (Exception e) {
					e.printStackTrace();
				}
	}
}
