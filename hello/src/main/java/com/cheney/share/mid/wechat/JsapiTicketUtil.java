package com.cheney.share.mid.wechat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import net.sf.json.JSONObject;


/***
 * @author V型知识库 www.vxzsk.com
 *
 */
public class JsapiTicketUtil {
	
	 public static String sendGet(String url, String charset, int timeout)
     {
       String result = "";
       try
       {
         URL u = new URL(url);
         try
         {
           URLConnection conn = u.openConnection();
           conn.connect();
           conn.setConnectTimeout(timeout);
           BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), charset));
           String line="";
           while ((line = in.readLine()) != null)
           {
            
             result = result + line;
           }
           in.close();
         } catch (IOException e) {
           return result;
         }
       }
       catch (MalformedURLException e)
       {
         return result;
       }
      
       return result;
     }

	/***
	 * 获取acess_token
	 * 
	 * @return
	 */
	public static String getAccessToken(String appid, String appSecret) {
		String url = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" + appid + "&secret="
				+ appSecret;
//		String result = HttpsUtil.httpsRequestToString(url, "GET", null);
//		JSONObject jsonObject = JSON.parseObject(result);
//		String access_token = jsonObject.getString("access_token");
		 String backData=JsapiTicketUtil.sendGet(url, "utf-8", 10000);
         String accessToken = (String) JSONObject.fromObject(backData).get("access_token"); 
         System.out.println(accessToken);
		return accessToken;
	}

	/***
	 * 获取jsapiTicket
	 * 
	 * @return
	 */
	public static String getJSApiTicket(String appid, String appSecret) {
		// 获取token
		String acess_token = JsapiTicketUtil.getAccessToken(appid, appSecret);
		String urlStr = "https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=" + acess_token
				+ "&type=jsapi";
//		String result = HttpsUtil.httpsRequestToString(urlStr, "GET", null);
//		JSONObject jsonObject = JSON.parseObject(result);
//		String ticket = jsonObject.getString("ticket");
		// String backData=TestAcessToken.sendGet(urlStr, "utf-8", 10000);
		// String ticket = (String) JSONObject.fromObject(backData).get("ticket");
		 String backData=JsapiTicketUtil.sendGet(urlStr, "utf-8", 10000);  
	        String ticket = (String) JSONObject.fromObject(backData).get("ticket");  
		return ticket;

	}
	
	public static void main(String[] args) {
		String jsapiTicket = JsapiTicketUtil.getJSApiTicket("wx80e192bcf69779f1","4121074baaae6d69062fada87e7a4670");
        System.out.println("调用微信jsapi的凭证票为："+jsapiTicket);
	}

}