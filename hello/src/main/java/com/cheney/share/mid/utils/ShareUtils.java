package com.cheney.share.mid.utils;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ShareUtils {

	static Logger logger = LoggerFactory.getLogger(ShareUtils.class);

	/**
	 * 对密码里德md5加密
	 * 
	 * @param rawPassword
	 * @return
	 */
	public static String encryptPassword(String rawPassword) {
		return DigestUtils.md5Hex(String.format("%s-xshop", rawPassword));
	}

	/**
	 * 是否是直辖市
	 * 
	 * @param cityName
	 * @return
	 */
	public static boolean isZXS(String cityName) {
		if (StringUtils.isBlank(cityName)) {
			return false;
		}
		if (cityName.contains("北京") || cityName.contains("上海") || cityName.contains("天津") || cityName.contains("重庆")) {
			return true;
		}
		return false;
	}
}