package com.cheney.share.mid.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("umeng")
public class UmengProperties {
	private String androidAppkey;
	private String androidAppMasterSecret;
	private String iosAppkey;
	private String iosAppMasterSecret;

	public String getAndroidAppkey() {
		return androidAppkey;
	}

	public void setAndroidAppkey(String androidAppkey) {
		this.androidAppkey = androidAppkey;
	}

	public String getAndroidAppMasterSecret() {
		return androidAppMasterSecret;
	}

	public void setAndroidAppMasterSecret(String androidAppMasterSecret) {
		this.androidAppMasterSecret = androidAppMasterSecret;
	}

	public String getIosAppkey() {
		return iosAppkey;
	}

	public void setIosAppkey(String iosAppkey) {
		this.iosAppkey = iosAppkey;
	}

	public String getIosAppMasterSecret() {
		return iosAppMasterSecret;
	}

	public void setIosAppMasterSecret(String iosAppMasterSecret) {
		this.iosAppMasterSecret = iosAppMasterSecret;
	}
}
