package com.cheney.share.mid.model;

import com.alibaba.fastjson.JSON;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.utils.CodecUtils;

/**
 * 用户token
 * 
 * @author chenheng
 *
 */
public class UToken  {
	/**
	 * 用户id
	 */
	Long id;
	/**
	 * 环境
	 */
	String env;
	/**
	 * 时间截
	 */
	Long timestamp;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public static UToken from(String utk, String key) throws HelperException {
		try {
			String js = CodecUtils.decrypt(utk, key);
			UToken token = JSON.toJavaObject(JSON.parseObject(js), UToken.class);
			return token;
		} catch (Exception e) {
			throw new HelperException("未识别的登录凭证,请重新登录获取用户凭证");
		}
	}
}
