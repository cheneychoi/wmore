package com.cheney.share.mid.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("文件")
public class FileIn {
	@ApiModelProperty(value = "阿里存储文件id", example = "2017090512490035682")
	private String oid;
	@ApiModelProperty(value = "本地文件名(原文件名)", example = "我是一个测试文件.txt")
	private String fileName;

	public FileIn() {

	}

	public FileIn(String oid, String fileName) {
		this.oid = oid;
		this.fileName = fileName;
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
