package com.cheney.share.mid.redis;

public class UrlKey extends BasePrefix{

	public UrlKey(int expireSeconds, String prefix) {
		super(expireSeconds, prefix);
	}

	public static UrlKey withExpire(int expireSeconds) {
		return new UrlKey(expireSeconds, "url");
	}
}
