package com.cheney.share.mid.zxing;

import java.awt.image.BufferedImage;
import java.io.File;
import java.util.HashMap;

import javax.imageio.ImageIO;

import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.common.HybridBinarizer;

/**
 * @author wangbo
 * @create 2018年1月10日下午2:50:41
 * @version 1.0
 */
public class ReadZXing {

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Result readQRCode(String path) {
		Result result = null;
		try {
			//二维码解析
			MultiFormatReader multiFormatReader = new MultiFormatReader();

			File file = new File(path);
			//获取图片流
			BufferedImage image = ImageIO.read(file);

			BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));

			HashMap hints = new HashMap();
			hints.put(EncodeHintType.CHARACTER_SET, "utf-8");

			 result = multiFormatReader.decode(binaryBitmap, hints);
			 System.out.println(result);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result;
	}

}
