package com.cheney.share.mid.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.commons.lang.StringUtils;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.DefaultAlipayClient;
import com.alipay.api.domain.AlipayTradeAppPayModel;
import com.alipay.api.request.AlipayTradeAppPayRequest;
import com.alipay.api.response.AlipayTradeAppPayResponse;
import com.cheney.share.mid.prop.AliPayProperties;

public class AliPayUtils {
	static AlipayClient alipayClient;

	public static AlipayClient getAlipayClient(AliPayProperties props) {
		if (alipayClient == null) {
			Lock lock = new ReentrantLock();
			lock.lock();
			try {
				alipayClient = new DefaultAlipayClient("https://openapi.alipay.com/gateway.do", props.getAppId(),
						props.getAppPrivateKey(), "json", "UTF-8", props.getAlipayPublicKey(), "RSA2");
			} finally {
				lock.unlock();
			}
		}
		return alipayClient;
	}

	public static AlipayTradeAppPayResponse aliAppUnifiedorder(String tradeNo, String body, Integer fee,
			AliPayProperties props) {
		// 实例化客户端
		AlipayClient alipayClient = getAlipayClient(props);
		// 实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
		AlipayTradeAppPayRequest request = new AlipayTradeAppPayRequest();
		// SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
		AlipayTradeAppPayModel model = new AlipayTradeAppPayModel();
		model.setBody(body);
		model.setSubject(body);
		model.setOutTradeNo(tradeNo);
		model.setTimeoutExpress("30m");
		model.setTotalAmount(String.format("%.2f", fee / 100));
		model.setProductCode("QUICK_MSECURITY_PAY");
		request.setBizModel(model);
		request.setNotifyUrl(props.getNotifyUrl());
		try {
			// 这里和普通的接口调用不同，使用的是sdkExecute
			AlipayTradeAppPayResponse response = alipayClient.sdkExecute(request);
			return response;
		} catch (AlipayApiException e) {
			return null;
		}
	}

	public static Map<String, String> notifyMap(Map<String, String[]> paramMap) {
		Map<String, String> nm = new HashMap<String, String>();
		for (Entry<String, String[]> en : paramMap.entrySet()) {
			String val = StringUtils.join(en.getValue(), ",");
			nm.put(en.getKey(), val);
		}
		return nm;
	}
}
