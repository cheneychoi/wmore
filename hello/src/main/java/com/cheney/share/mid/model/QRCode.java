package com.cheney.share.mid.model;

public class QRCode {

	private int width;
	private int height;
	private String content;

	public QRCode(String content) {
		super();
		this.content = content;
		this.width = 500;
		this.height = 500;
	}
	

	public QRCode(int width, int height, String content) {
		super();
		this.width = width;
		this.height = height;
		this.content = content;
	}


	public int getWidth() {
		return width;
	}

	public void setWidth(int width) {
		this.width = width;
	}

	public int getHeight() {
		return height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

}
