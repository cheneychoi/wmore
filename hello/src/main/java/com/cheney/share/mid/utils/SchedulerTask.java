package com.cheney.share.mid.utils;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.cheney.modules.maye.db.mapper.CourseMapper;
import com.cheney.modules.maye.db.mapper.TaskMapper;
import com.cheney.modules.maye.db.mapper.UserCourseMapper;
import com.cheney.modules.maye.db.model.CourseWithBLOBs;
import com.cheney.modules.maye.db.model.Task;
import com.cheney.modules.maye.db.model.TaskExample;
import com.cheney.modules.maye.db.model.out.TomorrowCourseOut;
import com.cheney.modules.maye.mid.service.UserService;

@Component
public class SchedulerTask {

	@Resource
	CourseMapper courseMapper;

	@Resource
	TaskMapper taskMapper;

	@Resource
	UserService userService;

	@Resource
	UserCourseMapper userCourseMapper;

	@Scheduled(cron = "0 0 0 1 * ?")
	private void processByWeek() {
		List<CourseWithBLOBs> courseWithBLOBs = courseMapper.getCourseByTask(1);
		for (CourseWithBLOBs course : courseWithBLOBs) {
			Long courseId = course.getId();
			for (int i = 0; i < 6; i++) {
				course.setCourseDate(ToolUtil.getNextWeek(course.getCourseDate()));
				if (ToolUtil.getMonth(course.getCourseDate()) <= ToolUtil.getMonth(new Date())) {
					courseMapper.insertSelective(course);
				}
			}
			TaskExample taskExample = new TaskExample();
			taskExample.or().andCourseIdEqualTo(courseId);
			taskMapper.deleteByExample(taskExample);
			Task task = new Task();
			task.setCourseId(course.getId());
			task.setTime(1);
		}
	}

	@Scheduled(cron = "0 0 0 1 * ?")
	private void processByMonth() {
		List<CourseWithBLOBs> courseWithBLOBs = courseMapper.getCourseByTask(2);
		for (CourseWithBLOBs course : courseWithBLOBs) {
			Long courseId = course.getId();
			course.setCourseDate(ToolUtil.getNextMonth(course.getCourseDate()));
			courseMapper.insertSelective(course);
			TaskExample taskExample = new TaskExample();
			taskExample.or().andCourseIdEqualTo(courseId);
			taskMapper.deleteByExample(taskExample);
			Task task = new Task();
			task.setCourseId(course.getId());
			task.setTime(2);
			taskMapper.insertSelective(task);
		}
	}

	@Scheduled(cron = "0 0 18 * * ?")
	private void processByDay() {
		String tomorrowDate = ToolUtil.getTomorrowDate();
		List<TomorrowCourseOut> tomorrowCourseOuts = userCourseMapper.getTomorrowCourseInfo(tomorrowDate);
		for (TomorrowCourseOut tomorrowCourseOut : tomorrowCourseOuts) {
			userService.sendNoticeSMS(tomorrowCourseOut.getMobile(), tomorrowCourseOut.getCourseName(),
					ToolUtil.getDate(tomorrowCourseOut.getCourseDate()), tomorrowCourseOut.getCoursePlace());
		}
	}

}