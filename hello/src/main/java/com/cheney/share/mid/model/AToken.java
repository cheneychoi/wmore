package com.cheney.share.mid.model;

import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSON;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.utils.CodecUtils;

/**
 * Api接口访问token
 * 
 * @author chenheng
 *
 */
public class AToken {
	/**
	 * 应用id
	 */
	String appId;
	/**
	 * 环境
	 */
	String env;
	/**
	 * 时间截
	 */
	Long timestamp;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}

	public static AToken from(String atk, String key) throws HelperException {
		try {
			String js = CodecUtils.decrypt(atk, key);
			AToken token = JSON.toJavaObject(JSON.parseObject(js), AToken.class);
			return token;
		} catch (Exception e) {
			throw new HelperException("未识别的登录凭证,请登录后重试");
		}
	}

	public static Map<String, String> APPLICATION = new HashMap<String, String>();
	static {
		APPLICATION.put("10000", "27b511b52ccc0787c54ffd44a28f39d5");// 前端应用
		APPLICATION.put("10001", "9bd09615a83c65d8e9a8ea2f99683536");// 商家端应用
		APPLICATION.put("10002", "2b63b38c645a97212e93fb45f9f63632");// 平台后台应用
	}
}
