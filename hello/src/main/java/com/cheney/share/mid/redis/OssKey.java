package com.cheney.share.mid.redis;

public class OssKey extends BasePrefix{

	public OssKey(int expireSeconds, String prefix) {
		super(expireSeconds, prefix);
	}

	public static OssKey withExpire(int expireSeconds) {
		return new OssKey(expireSeconds, "OSS");
	}
}
