package com.cheney.share.mid.model;

/**
 * 三元组
 * 
 * @author chenheng
 *
 */
public class Tuple<K, V, R> {
	private K key;
	private V value;
	private R remark;

	public K getKey() {
		return key;
	}

	public void setKey(K key) {
		this.key = key;
	}

	public V getValue() {
		return value;
	}

	public void setValue(V value) {
		this.value = value;
	}

	public R getRemark() {
		return remark;
	}

	public void setRemark(R remark) {
		this.remark = remark;
	}
}
