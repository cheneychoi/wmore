package com.cheney.share.mid.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("wxTempMsg")
public class WxTempMsgProperties {
	private String attendClassId;
	private String finishClassId;

	public String getAttendClassId() {
		return attendClassId;
	}

	public void setAttendClassId(String attendClassId) {
		this.attendClassId = attendClassId;
	}

	public String getFinishClassId() {
		return finishClassId;
	}

	public void setFinishClassId(String finishClassId) {
		this.finishClassId = finishClassId;
	}

}
