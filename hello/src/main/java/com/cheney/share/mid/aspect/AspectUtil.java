package com.cheney.share.mid.aspect;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectUtil {
	
	@Pointcut("@annotation(com.cheney.share.mid.aspect.MyAnnotation)")
	public void log() {}
	
	
	@AfterReturning("log()")
	public void addLog(JoinPoint joinPoint) {
//		Object[] objects = joinPoint.getArgs();
	}
	
}
