package com.cheney.share.mid.excel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;

import com.cheney.modules.maye.db.model.out.UserExcelOut;

public class ExportExcel {
	@SuppressWarnings("resource")
	public String exportExcel(List<UserExcelOut> excelOuts) {
		HSSFWorkbook wb = new HSSFWorkbook(); // --->创建了一个excel文件
		HSSFSheet sheet = wb.createSheet("会员列表"); // --->创建了一个工作簿

		sheet.setDefaultColumnWidth(11);
		sheet.setColumnWidth(0, 15 * 256);
		sheet.setColumnWidth(1, 15 * 256);
		sheet.setColumnWidth(2, 15 * 256);
		sheet.setColumnWidth(3, 10 * 256);
		sheet.setColumnWidth(4, 20 * 256);
		sheet.setColumnWidth(5, 25 * 256);
		sheet.setColumnWidth(6, 15 * 256);
		sheet.setColumnWidth(7, 30 * 256);
		sheet.setColumnWidth(8, 10 * 256);
		sheet.setColumnWidth(9, 30 * 256);
		sheet.setColumnWidth(10, 10 * 256);

		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyle.setFillForegroundColor(HSSFColor.BLACK.index);
		HSSFFont font = wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setColor(HSSFColor.WHITE.index);
		cellStyle.setFont(font);
		// 表格第一行
		HSSFRow row1 = sheet.createRow(0);
		HSSFCell cell_head = row1.createCell(0);
		cell_head.setCellValue("企业名称");
		cell_head.setCellStyle(cellStyle);
		HSSFCell cell_head1 = row1.createCell(1);
		cell_head1.setCellValue("企业代码");
		cell_head1.setCellStyle(cellStyle);
		HSSFCell cell_head2 = row1.createCell(2);
		cell_head2.setCellValue("真实姓名");
		cell_head2.setCellStyle(cellStyle);
		HSSFCell cell_head3 = row1.createCell(3);
		cell_head3.setCellValue("性别(男/女)");
		cell_head3.setCellStyle(cellStyle);
		HSSFCell cell_head4 = row1.createCell(4);
		cell_head4.setCellValue("手机");
		cell_head4.setCellStyle(cellStyle);
		HSSFCell cell_head5 = row1.createCell(5);
		cell_head5.setCellValue("邮箱");
		cell_head5.setCellStyle(cellStyle);
		HSSFCell cell_head6 = row1.createCell(6);
		cell_head6.setCellValue("职位");
		cell_head6.setCellStyle(cellStyle);
		HSSFCell cell_head7 = row1.createCell(7);
		cell_head7.setCellValue("城市");
		cell_head7.setCellStyle(cellStyle);
		HSSFCell cell_head8 = row1.createCell(8);
		cell_head8.setCellValue("行业");
		cell_head8.setCellStyle(cellStyle);
		HSSFCell cell_head9 = row1.createCell(9);
		cell_head9.setCellValue("兴趣");
		cell_head9.setCellStyle(cellStyle);
		HSSFCell cell_head10 = row1.createCell(10);
		cell_head10.setCellValue("积分");
		cell_head10.setCellStyle(cellStyle);

		CellStyle cellStyle1 = wb.createCellStyle();
		HSSFFont font1 = wb.createFont();
		font1.setFontName("微软雅黑");
		font1.setFontHeightInPoints((short) 12);
		cellStyle1.setFont(font1);

		// 表格第三行
		for (int i = 0; i < excelOuts.size(); i++) {
			UserExcelOut excelOut = excelOuts.get(i);
			HSSFRow row2 = sheet.createRow(i + 1);
			HSSFCell cell = row2.createCell(0);
			cell.setCellValue(excelOut.getCompanyName());
			cell.setCellStyle(cellStyle1);
			HSSFCell cell_1 = row2.createCell(1);
			cell_1.setCellValue(excelOut.getCompanyCode());
			cell_1.setCellStyle(cellStyle1);
			HSSFCell cell2 = row2.createCell(2);
			cell2.setCellValue(excelOut.getName());
			cell2.setCellStyle(cellStyle1);
			HSSFCell cell3 = row2.createCell(3);
			String sex = null;
			if (excelOut.getSex() == 1) {
				sex = "男";
			} else {
				sex = "女";
			}
			cell3.setCellValue(sex);
			cell3.setCellStyle(cellStyle1);
			HSSFCell cell4 = row2.createCell(4);
			cell4.setCellValue(excelOut.getMobile());
			cell4.setCellStyle(cellStyle1);
			HSSFCell cell5 = row2.createCell(5);
			cell5.setCellValue(excelOut.getEmail());
			cell5.setCellStyle(cellStyle1);
			HSSFCell cell6 = row2.createCell(6);
			String position = null;
			if (excelOut.getPosition() == 1) {
				position = "高管";
			} else if (excelOut.getPosition() == 2) {
				position = "普通职工";
			} else {
				position = "";
			}
			cell6.setCellValue(position);
			cell6.setCellStyle(cellStyle1);
			HSSFCell cell7 = row2.createCell(7);
			cell7.setCellValue(excelOut.getCity() == null ? "" : excelOut.getCity());
			cell7.setCellStyle(cellStyle1);
			HSSFCell cell8 = row2.createCell(8);
			cell8.setCellValue(excelOut.getIndustry() == null ? "" : excelOut.getIndustry());
			cell8.setCellStyle(cellStyle1);
			HSSFCell cell9 = row2.createCell(9);
			cell9.setCellValue(excelOut.getHobby() == null ? "" : excelOut.getHobby());
			cell9.setCellStyle(cellStyle1);
			HSSFCell cell10 = row2.createCell(10);
			cell10.setCellValue(Integer.toString(excelOut.getPoints()));
			cell10.setCellStyle(cellStyle1);
		}
		String filePath = "员工列表" + new SimpleDateFormat("yyMMddHHmmss").format(new Date()) + ".xls";

		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(filePath);
			wb.write(fileOut);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileOut != null) {
				try {
					fileOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return filePath;
	}
}
