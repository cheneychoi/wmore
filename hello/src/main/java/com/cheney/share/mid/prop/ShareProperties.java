package com.cheney.share.mid.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("share")
public class ShareProperties {
	private String apiGateway;
	private String wwwGateway;
	private String aeskey;
	private String env;
	private String domain;
	/**
	 * 企业用户购买商品总佣金占比（占实现支付金额比例）
	 */
	private float firmUserCommissionRate;
	/**
	 * 企业用户购买商品平台佣金占比（以此可得出推荐人占比：firmUserCommissionRate -
	 * firmUserCommissionPlatRate）
	 */
	private float firmUserCommissionPlatRate;
	/**
	 * 普通用户购买商品总佣金占比（占实现支付金额比例）
	 */
	private float commonUserCommissionRate;
	/**
	 * 普通用户购买商品平台佣金占比（以此可得出推荐人占比：firmUserCommissionRate -
	 * firmUserCommissionPlatRate）
	 */
	private float commonUserCommissionPlatRate;
	/**
	 * 商品自动上下架周期
	 */
	private int autoShelfDays;
	/**
	 * 检查商品销售区域与收货地址匹配关系
	 */
	private boolean checkAddress;
	/**
	 * 邮费
	 */
	private int postage;
	/**
	 * 包邮阈值
	 */
	private int freeShipping;

	public String getApiGateway() {
		return apiGateway;
	}

	public void setApiGateway(String apiGateway) {
		this.apiGateway = apiGateway;
	}

	public String getWwwGateway() {
		return wwwGateway;
	}

	public void setWwwGateway(String wwwGateway) {
		this.wwwGateway = wwwGateway;
	}

	public String getAeskey() {
		return aeskey;
	}

	public void setAeskey(String aeskey) {
		this.aeskey = aeskey;
	}

	public String getEnv() {
		return env;
	}

	public void setEnv(String env) {
		this.env = env;
	}

	public float getFirmUserCommissionRate() {
		return firmUserCommissionRate;
	}

	public void setFirmUserCommissionRate(float firmUserCommissionRate) {
		this.firmUserCommissionRate = firmUserCommissionRate;
	}

	public float getFirmUserCommissionPlatRate() {
		return firmUserCommissionPlatRate;
	}

	public void setFirmUserCommissionPlatRate(float firmUserCommissionPlatRate) {
		this.firmUserCommissionPlatRate = firmUserCommissionPlatRate;
	}

	public float getCommonUserCommissionRate() {
		return commonUserCommissionRate;
	}

	public void setCommonUserCommissionRate(float commonUserCommissionRate) {
		this.commonUserCommissionRate = commonUserCommissionRate;
	}

	public float getCommonUserCommissionPlatRate() {
		return commonUserCommissionPlatRate;
	}

	public void setCommonUserCommissionPlatRate(float commonUserCommissionPlatRate) {
		this.commonUserCommissionPlatRate = commonUserCommissionPlatRate;
	}

	public int getAutoShelfDays() {
		return autoShelfDays;
	}

	public void setAutoShelfDays(int autoShelfDays) {
		this.autoShelfDays = autoShelfDays;
	}

	public boolean isCheckAddress() {
		return checkAddress;
	}

	public void setCheckAddress(boolean checkAddress) {
		this.checkAddress = checkAddress;
	}

	public String getDomain() {
		return domain;
	}

	public void setDomain(String domain) {
		this.domain = domain;
	}

	public int getPostage() {
		return postage;
	}

	public void setPostage(int postage) {
		this.postage = postage;
	}

	public int getFreeShipping() {
		return freeShipping;
	}

	public void setFreeShipping(int freeShipping) {
		this.freeShipping = freeShipping;
	}
}
