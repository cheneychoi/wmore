package com.cheney.share.mid.excel;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.CellStyle;

import com.cheney.modules.maye.db.model.out.MakeCourseOut;

public class ExportCourseExcel {
	@SuppressWarnings("resource")
	public String exportExcel(List<MakeCourseOut> makeCourseOuts) {
		HSSFWorkbook wb = new HSSFWorkbook(); // --->创建了一个excel文件
		HSSFSheet sheet = wb.createSheet("预约列表"); // --->创建了一个工作簿

		sheet.setDefaultColumnWidth(11);
		sheet.setColumnWidth(0, 20 * 256);
		sheet.setColumnWidth(1, 30 * 256);
		sheet.setColumnWidth(2, 20 * 256);
		sheet.setColumnWidth(3, 15 * 256);
		sheet.setColumnWidth(4, 20 * 256);
		sheet.setColumnWidth(5, 30 * 256);
		sheet.setColumnWidth(6, 10 * 256);
		sheet.setColumnWidth(7, 30 * 256);
		sheet.setColumnWidth(8, 10 * 256);
		sheet.setColumnWidth(9, 30 * 256);

		CellStyle cellStyle = wb.createCellStyle();
		cellStyle.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
		cellStyle.setFillForegroundColor(HSSFColor.BLACK.index);
		HSSFFont font = wb.createFont();
		font.setFontHeightInPoints((short) 12);
		font.setColor(HSSFColor.WHITE.index);
		cellStyle.setFont(font);
		// 表格第一行
		HSSFRow row1 = sheet.createRow(0);
		HSSFCell cell_head = row1.createCell(0);
		cell_head.setCellValue("课程名称");
		cell_head.setCellStyle(cellStyle);
		HSSFCell cell_head1 = row1.createCell(1);
		cell_head1.setCellValue("课程时间");
		cell_head1.setCellStyle(cellStyle);
		HSSFCell cell_head2 = row1.createCell(2);
		cell_head2.setCellValue("课程类别");
		cell_head2.setCellStyle(cellStyle);
		HSSFCell cell_head3 = row1.createCell(3);
		cell_head3.setCellValue("用户名");
		cell_head3.setCellStyle(cellStyle);
		HSSFCell cell_head4 = row1.createCell(4);
		cell_head4.setCellValue("手机");
		cell_head4.setCellStyle(cellStyle);
		HSSFCell cell_head5 = row1.createCell(5);
		cell_head5.setCellValue("邮箱");
		cell_head5.setCellStyle(cellStyle);
		HSSFCell cell_head6 = row1.createCell(6);
		cell_head6.setCellValue("职位");
		cell_head6.setCellStyle(cellStyle);
		HSSFCell cell_head7 = row1.createCell(7);
		cell_head7.setCellValue("公司名称");
		cell_head7.setCellStyle(cellStyle);
		HSSFCell cell_head8 = row1.createCell(8);
		cell_head8.setCellValue("签到状态");
		cell_head8.setCellStyle(cellStyle);
		HSSFCell cell_head9 = row1.createCell(9);
		cell_head9.setCellValue("签到时间");
		cell_head9.setCellStyle(cellStyle);

		CellStyle cellStyle1 = wb.createCellStyle();
		HSSFFont font1 = wb.createFont();
		font1.setFontName("微软雅黑");
		font1.setFontHeightInPoints((short) 12);
		cellStyle1.setFont(font1);

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		// 表格第三行
		for (int i = 0; i < makeCourseOuts.size(); i++) {
			MakeCourseOut makeCourseOut = makeCourseOuts.get(i);
			HSSFRow row2 = sheet.createRow(i + 1);
			HSSFCell cell = row2.createCell(0);
			cell.setCellValue(makeCourseOut.getCourseName());
			cell.setCellStyle(cellStyle1);
			HSSFCell cell_1 = row2.createCell(1);
			cell_1.setCellValue(formatter.format(makeCourseOut.getCourseDate()));
			cell_1.setCellStyle(cellStyle1);

			String type = "";
			Integer cType = makeCourseOut.getType();
			if (cType == 1) {
				type = "员工课";
			} else if (cType == 2) {
				type = "团建课";
			} else if (cType == 3) {
				type = "户外课";
			} else if (cType == 4) {
				type = "高管课";
			}
			HSSFCell cell2 = row2.createCell(2);
			cell2.setCellValue(type);
			cell2.setCellStyle(cellStyle1);
			HSSFCell cell3 = row2.createCell(3);
			cell3.setCellValue(makeCourseOut.getName());
			cell3.setCellStyle(cellStyle1);
			HSSFCell cell4 = row2.createCell(4);
			cell4.setCellValue(makeCourseOut.getMobile());
			cell4.setCellStyle(cellStyle1);
			HSSFCell cell5 = row2.createCell(5);
			cell5.setCellValue(makeCourseOut.getEmail());
			cell5.setCellStyle(cellStyle1);
			HSSFCell cell6 = row2.createCell(6);
			String position = null;
			if (makeCourseOut.getPosition() == 1) {
				position = "高管";
			} else if (makeCourseOut.getPosition() == 2) {
				position = "普通职工";
			} else {
				position = "";
			}
			cell6.setCellValue(position);
			cell6.setCellStyle(cellStyle1);
			HSSFCell cell7 = row2.createCell(7);
			cell7.setCellValue(makeCourseOut.getCompanyName());
			cell7.setCellStyle(cellStyle1);

			Integer cState = makeCourseOut.getSign();
			String sign = "";
			if (cState == 1) {
				sign = "已签到";
			} else if (cState == 2) {
				sign = "未签到";
			}
			HSSFCell cell8 = row2.createCell(8);
			cell8.setCellValue(sign);
			cell8.setCellStyle(cellStyle1);
			HSSFCell cell9 = row2.createCell(9);
			Date date = makeCourseOut.getDate();
			String signDate = "无时间";
			if (date != null) {
				signDate = formatter.format(date);
			}
			cell9.setCellValue(signDate);
			cell9.setCellStyle(cellStyle1);
		}
		String filePath = "预约列表" + new SimpleDateFormat("yyMMddHHmmss").format(new Date()) + ".xls";

		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(filePath);
			wb.write(fileOut);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (fileOut != null) {
				try {
					fileOut.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
		return filePath;
	}
}
