package com.cheney.share.mid.utils;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class QQUtils {

	static Logger logger = LoggerFactory.getLogger(QQUtils.class);

	/**
	 * 
	 * @param str
	 * 示例: access_token=0DAC492CD25BFC99275708F1F129BB22&expires_in=7776000&refresh_token=40304397D26B16F34B75FF11D039E630
	 * @return
	 */
	public static Map<String, Object> accessTokenString2Map(String str) {
		Map<String, Object> map = new HashMap<String, Object>();
		String[] items = str.split("&");
		for (String item: items) {
			String[] kv = item.split("=");
			if (kv.length == 2) {
				map.put(kv[0], kv[1]);
			}
		}
		return map;
	}
}