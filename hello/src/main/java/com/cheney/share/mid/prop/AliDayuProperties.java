package com.cheney.share.mid.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("alidayu")
public class AliDayuProperties {
	private String appId;
	private String appKey;
	private String sign;
	private String product;
	/**
	 * 模版类型: 验证码 <br>
	 * 模版名称: 手机注册通知 <br>
	 * 模版CODE: SMS_85520073 <br>
	 * 模版内容: 尊敬的E彩商城客户，您的注册验证码为：${number}
	 */
	private String phoneRegisterTMID;
	/**
	 * 模版类型: 验证码<br>
	 * 模版名称: 手机号绑定<br>
	 * 模版CODE: SMS_85350073<br>
	 * 模版内容: 尊敬的E彩商城客户，本次手机验证码为：${number}
	 */
	private String phoneBindTMID;
	
	private String phoneNoticeTMID;
	
	private String phoneOrderTMID;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getPhoneRegisterTMID() {
		return phoneRegisterTMID;
	}

	public void setPhoneRegisterTMID(String phoneRegisterTMID) {
		this.phoneRegisterTMID = phoneRegisterTMID;
	}

	public String getPhoneBindTMID() {
		return phoneBindTMID;
	}

	public void setPhoneBindTMID(String phoneBindTMID) {
		this.phoneBindTMID = phoneBindTMID;
	}

	public String getSign() {
		return sign;
	}

	public void setSign(String sign) {
		this.sign = sign;
	}

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getPhoneNoticeTMID() {
		return phoneNoticeTMID;
	}

	public void setPhoneNoticeTMID(String phoneNoticeTMID) {
		this.phoneNoticeTMID = phoneNoticeTMID;
	}

	public String getPhoneOrderTMID() {
		return phoneOrderTMID;
	}

	public void setPhoneOrderTMID(String phoneOrderTMID) {
		this.phoneOrderTMID = phoneOrderTMID;
	}

}
