package com.cheney.share.mid.prop;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("wx")
public class WxProperties {
	/**
	 * 公众号appId
	 */
	private String appId;
	/**
	 * 公众号appKey
	 */
	private String appKey;
	/**
	 * 商户号id
	 */
	private String merchantId;
	/**
	 * 商户号key
	 */
	private String merchantKey;
	/**
	 * 用户端实际ip
	 */
	private String spbillIp;
	/**
	 * 商户号证书地址
	 */
	private String certPath;
	/**
	 * 公众号原始id
	 */
	private String ghid;
	/**
	 * 开放平台审核通过的应用appId
	 */
	private String openAppId;
	/**
	 * 开放平台审核通过的应用key
	 */
	private String openAppKey;
	/**
	 * 支付通知接口
	 */
	private String notifyUrl;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getAppKey() {
		return appKey;
	}

	public void setAppKey(String appKey) {
		this.appKey = appKey;
	}

	public String getMerchantId() {
		return merchantId;
	}

	public void setMerchantId(String merchantId) {
		this.merchantId = merchantId;
	}

	public String getMerchantKey() {
		return merchantKey;
	}

	public void setMerchantKey(String merchantKey) {
		this.merchantKey = merchantKey;
	}

	public String getSpbillIp() {
		return spbillIp;
	}

	public void setSpbillIp(String spbillIp) {
		this.spbillIp = spbillIp;
	}

	public String getCertPath() {
		return certPath;
	}

	public void setCertPath(String certPath) {
		this.certPath = certPath;
	}

	public String getGhid() {
		return ghid;
	}

	public void setGhid(String ghid) {
		this.ghid = ghid;
	}

	public String getOpenAppId() {
		return openAppId;
	}

	public void setOpenAppId(String openAppId) {
		this.openAppId = openAppId;
	}

	public String getOpenAppKey() {
		return openAppKey;
	}

	public void setOpenAppKey(String openAppKey) {
		this.openAppKey = openAppKey;
	}

	public String getNotifyUrl() {
		return notifyUrl;
	}

	public void setNotifyUrl(String notifyUrl) {
		this.notifyUrl = notifyUrl;
	}
}
