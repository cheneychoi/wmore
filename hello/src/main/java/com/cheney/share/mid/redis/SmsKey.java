package com.cheney.share.mid.redis;

public class SmsKey extends BasePrefix{

	public SmsKey(int expireSeconds, String prefix) {
		super(expireSeconds, prefix);
	}

	public static SmsKey withExpire(int expireSeconds) {
		return new SmsKey(expireSeconds, "SMS");
	}
}
