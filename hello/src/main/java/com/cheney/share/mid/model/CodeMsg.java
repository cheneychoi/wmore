package com.cheney.share.mid.model;

public class CodeMsg {

	private int code;
	private String msg;

	// 用户模块 500100
	public static CodeMsg USER_ERROR = new CodeMsg(500101, "用户异常");
	public static CodeMsg ADD_USER_ERROR = new CodeMsg(500102, "添加用户失败");
	public static CodeMsg USERNAME_ERROR = new CodeMsg(500103, "用户名不存在");
	public static CodeMsg USER_SAVE_ERROR = new CodeMsg(500104, "用户更新失败");
	public static CodeMsg OLD_PASSWORD_ERROR = new CodeMsg(500105, "原密码错误");
	public static CodeMsg NEW_PASSWORD_ERROR = new CodeMsg(500106, "新密码更新失败");
	public static CodeMsg PASSWORD_ERROR = new CodeMsg(500107, "密码错误");
	public static CodeMsg CODE_ERROR = new CodeMsg(500108, "验证码错误");
	public static CodeMsg BIND_OPENID_ERROR = new CodeMsg(500109, "用户绑定微信号失败");
	public static CodeMsg MATCH_USER_ERROR = new CodeMsg(500110, "匹配用户失败");
	public static CodeMsg INCREASE_POINT_ERROR = new CodeMsg(500111, "积分增加失败");
	public static CodeMsg WECHAT_OPENID_ERROR = new CodeMsg(500112, "没有获取到用户微信id");
	public static CodeMsg NO_MEMBER_ERROR = new CodeMsg(500113, "没有搜到会员");
	public static CodeMsg MOBILE_ERROR = new CodeMsg(500114, "手机号已被占用");
	public static CodeMsg EMAIL_ERROR = new CodeMsg(500115, "邮箱已被占用");

	// 图片模块 500200
	public static CodeMsg FILE_NOT_PICTURE = new CodeMsg(500201, "该文件不为图片类型");
	public static CodeMsg PICTURE_SAVE_ERROR = new CodeMsg(500202, "图片保存失败");
	public static CodeMsg FIND_IMAGE_ERROR = new CodeMsg(500203, "未找到二维码图片");
	public static CodeMsg PICTURE_ERROR = new CodeMsg(500204, "设置连接异常");
	public static CodeMsg PICTURE_NULL = new CodeMsg(500205, "图片为空");
	public static CodeMsg UPDATE_PICTURE_ERROR = new CodeMsg(500206, "编辑图片失败");

	// 企业模块 500300
	public static CodeMsg COMPANY_ERROR = new CodeMsg(500301, "企业模块异常");
	public static CodeMsg FROZEN_ERROR = new CodeMsg(500302, "企业冻结异常");
	public static CodeMsg SELECT_COMPANY_ERROR = new CodeMsg(500303, "未找到该公司");
	public static CodeMsg NO_COMPANY_ERROR = new CodeMsg(500304, "未搜到该公司");

	// 课程模块 500400
	public static CodeMsg ABOUT_ERROR = new CodeMsg(500401, "已预约过不可重复预约");
	public static CodeMsg ABOUT_NUM_ERROR = new CodeMsg(500402, "预约已满");
	public static CodeMsg ADD_COURSE_ERROR = new CodeMsg(500403, "课程添加失败");
	public static CodeMsg EDIT_COURSE_ERROR = new CodeMsg(500404, "课程编辑失败");
	public static CodeMsg EVALUATE_COURSE_ERROR = new CodeMsg(500405, "评价课程失败");

	// 配置模块500500
	public static CodeMsg SMS_CONFIG_ERROR = new CodeMsg(500501, "短信服务器配置失败");
	public static CodeMsg GET_SMS_ERROR = new CodeMsg(500502, "获取短信服务器配置失败");
	public static CodeMsg OSS_CONFIG_ERROR = new CodeMsg(500503, "OSS服务器配置失败");
	public static CodeMsg GET_OSS_ERROR = new CodeMsg(500504, "获取OSS服务器配置失败");
	public static CodeMsg ADD_LOGIN_ERROR = new CodeMsg(500505, "插入登陆日志失败");

	// 后台管理员模块500600
	public static CodeMsg DELETE_ADMIN_ERROR = new CodeMsg(500601, "删除失败");
	public static CodeMsg ADD_ADMIN_ERROR = new CodeMsg(500602, "删除失败");
	public static CodeMsg UPDATE_ADMIN_ERROR = new CodeMsg(500603, "编辑失败");
	public static CodeMsg NO_AUTHORITY_ERROR = new CodeMsg(500604, "用户权限不足");

	// 权限模块500700
	public static CodeMsg UPDATE_AUTHORITY_ERROR = new CodeMsg(500701, "编辑失败");
	public static CodeMsg ADD_AUTHORITY_ERROR = new CodeMsg(500702, "编辑失败");
	public static CodeMsg DELETE_AUTHORITY_ERROR = new CodeMsg(500703, "编辑失败");

	// 角色模块500800
	public static CodeMsg INSERT_ROLE_ERROR = new CodeMsg(500801, "编辑失败");
	public static CodeMsg UPDATE_ROLE_ERROR = new CodeMsg(500802, "编辑失败");
	public static CodeMsg DELETE_ROLE_ERROR = new CodeMsg(500803, "编辑失败");

	// 通知模块500900
	public static CodeMsg ADD_NOTICE_ERROR = new CodeMsg(500901, "添加通知失败");
	public static CodeMsg ADD_NOTICEMASTERPLATE_ERROR = new CodeMsg(500902, "添加通知模板失败");
	public static CodeMsg POINT_RULE_ERROR = new CodeMsg(500903, "最多启用一个相同类型的积分规则模版，请在后台设置");
	public static CodeMsg NO_POINT_ERROR = new CodeMsg(500904, "不存在此积分规则模版");
	public static CodeMsg NOTICE_ERROR = new CodeMsg(500905, "最多启用一个相同类型的通知模版，请在后台设置");
	public static CodeMsg NO_NOTICE_ERROR = new CodeMsg(500906, "不存在此通知模版");

	// excel模块
	public static CodeMsg FILE_ERROR = new CodeMsg(600100, "文件类型错误");
	public static CodeMsg SEX_ERROR = new CodeMsg(600100, "性别错误");
	public static CodeMsg POSITION_ERROR = new CodeMsg(600100, "职位名称错误");
	public static CodeMsg COMPANYNAME_NULL_ERROR = new CodeMsg(600100, "公司代码为空");
	public static CodeMsg NAME_NULL_ERROR = new CodeMsg(600100, "真实姓名为空");
	public static CodeMsg SEX_NULL_ERROR = new CodeMsg(600100, "性别为空");
	public static CodeMsg MOBILE_NULL_ERROR = new CodeMsg(600100, "手机为空");
	public static CodeMsg EMAIL_NULL_ERROR = new CodeMsg(600100, "邮箱为空");
	public static CodeMsg POSITION_NULL_ERROR = new CodeMsg(600100, "职位为空");
	public static CodeMsg COMPANYCODE_ERROR = new CodeMsg(600100, "公司代码错误");
	public static CodeMsg TEMPLATE_ERROR = new CodeMsg(600100, "请导入正确模板");

	private CodeMsg() {
	}

	private CodeMsg(int code, String msg) {
		this.code = code;
		this.msg = msg;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public CodeMsg fillArgs(Object... args) {
		int code = this.code;
		String message = String.format(this.msg, args);
		return new CodeMsg(code, message);
	}

	@Override
	public String toString() {
		return "CodeMsg [code=" + code + ", msg=" + msg + "]";
	}

}
