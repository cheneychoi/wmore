package com.cheney.share.mid.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("微信js签名－出")
public class WxJSOut {
	@ApiModelProperty("公众号id")
	private String appId;
	@ApiModelProperty("时间截（秒)")
	private String timestamp;
	@ApiModelProperty("随机数")
	private String nonceStr;
	@ApiModelProperty("签名")
	private String signature;

	public String getAppId() {
		return appId;
	}

	public void setAppId(String appId) {
		this.appId = appId;
	}

	public String getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}

	public String getNonceStr() {
		return nonceStr;
	}

	public void setNonceStr(String nonceStr) {
		this.nonceStr = nonceStr;
	}

	public String getSignature() {
		return signature;
	}

	public void setSignature(String signature) {
		this.signature = signature;
	}
}