package com.cheney.share.mid.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("访问token, API接口都需要通过access token访问")
public class ATokenOut {
	@ApiModelProperty(value = "访问token", example = "p1P4fNO7eQjnx1pjNjGREExIrf3LpTy/LIk2OAfipcY8jJ7YeEcId+QY2uysqnCUi8ZgJiH2Czns6y+4kAPIhg==")
	String accessToken;
	@ApiModelProperty(value = "生成access token的时间截, 用于计算access token有效期", example="1504590777355")
	Long timestamp;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}
}
