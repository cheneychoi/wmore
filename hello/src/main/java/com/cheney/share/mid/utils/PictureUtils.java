package com.cheney.share.mid.utils;

import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.springframework.web.multipart.MultipartFile;

import com.cheney.modules.maye.db.model.Picture;

public class PictureUtils {
	/** 
	 * 获取图片属性
	* @param file
	* @return Picture
	* @author WangBo
	* @date 2018年7月17日下午6:17:10
	*/ 
	public Picture getPictureModel(MultipartFile file) {
		String fileName = file.getOriginalFilename();
		String name = fileName.substring(0, fileName.indexOf("."));
		String suffix = fileName.substring(fileName.indexOf("."), fileName.length());
		String size = String.valueOf(file.getSize());
		BufferedImage image;
		String height = null;
		String width = null;
		try {
			image = ImageIO.read(file.getInputStream());
			height = String.valueOf(image.getHeight());
			width = String.valueOf(image.getWidth());
		} catch (IOException e) {
			e.printStackTrace();
		}
		Picture picture = new Picture();
		picture.setCreateDate(ToolUtil.getTime(0));
		picture.setPictureHeight(height);
		picture.setPictureName(name);
		picture.setPictureSize(size);
		picture.setPictureSuffix(suffix);
		picture.setPictureWidth(width);
		return picture;
	}
}
