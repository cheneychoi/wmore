package com.cheney.share.mid.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.cheney.modules.maye.db.model.in.ExcelIn;
import com.cheney.share.mid.exception.HelperException;
import com.cheney.share.mid.model.CodeMsg;

public class ImportExcel {

	public List<ExcelIn> importExcel(File file) throws EncryptedDocumentException, InvalidFormatException, IOException,HelperException{
		InputStream inputStream = new FileInputStream(file);
		Workbook workbook = WorkbookFactory.create(inputStream);
		Sheet sheet = workbook.getSheetAt(0);

		List<ExcelIn> excelIns = new ArrayList<>();
		
		for (Row row : sheet) {
			if (row.getRowNum() == 0) {
				String nString = row.getCell(0).getStringCellValue();
				if (!"企业代码".equals(nString)) {
					throw new HelperException(CodeMsg.TEMPLATE_ERROR.getCode(), CodeMsg.TEMPLATE_ERROR.getMsg());
				}
				continue;
			}
			if (isRowEmpty(row)) {
				break;
			}
			ExcelIn excelIn = new ExcelIn();
			row.getCell(0).setCellType(Cell.CELL_TYPE_STRING);
			String companyName = row.getCell(0).getStringCellValue();
			String name = row.getCell(1).getStringCellValue();
			String sex = row.getCell(2).getStringCellValue();
			row.getCell(3).setCellType(Cell.CELL_TYPE_STRING);
			String mobile = row.getCell(3).getStringCellValue();
			String email = row.getCell(4).getStringCellValue();
			String position = row.getCell(5).getStringCellValue();
			if (StringUtils.isEmpty(companyName)) {
				throw new HelperException(CodeMsg.COMPANYNAME_NULL_ERROR.getCode(),
						CodeMsg.COMPANYNAME_NULL_ERROR.getMsg());
			}
			excelIn.setCompanyCode(companyName);
			if (StringUtils.isEmpty(name)) {
				throw new HelperException(CodeMsg.NAME_NULL_ERROR.getCode(), CodeMsg.NAME_NULL_ERROR.getMsg());
			}
			excelIn.setUsername(name);
			Integer sexNum = null;
			if (StringUtils.isEmpty(sex)) {
				throw new HelperException(CodeMsg.SEX_NULL_ERROR.getCode(), CodeMsg.SEX_NULL_ERROR.getMsg());
			}
			if ("男".equals(sex)) {
				sexNum = 1;
			} else if ("女".equals(sex)) {
				sexNum = 2;
			} else {
				throw new HelperException(CodeMsg.SEX_ERROR.getCode(), CodeMsg.SEX_ERROR.getMsg());
			}
			excelIn.setSex(sexNum);
			if (StringUtils.isEmpty(mobile)) {
				throw new HelperException(CodeMsg.MOBILE_NULL_ERROR.getCode(), CodeMsg.MOBILE_NULL_ERROR.getMsg());
			}
			excelIn.setMobile(mobile);
			if (StringUtils.isEmpty(email)) {
				throw new HelperException(CodeMsg.EMAIL_NULL_ERROR.getCode(), CodeMsg.EMAIL_NULL_ERROR.getMsg());
			}
			excelIn.setEmail(email);
			Integer positionNum = null;
			if (StringUtils.isEmpty(position)) {
				throw new HelperException(CodeMsg.POSITION_NULL_ERROR.getCode(), CodeMsg.POSITION_NULL_ERROR.getMsg());
			}
			if ("高管".equals(position)) {
				positionNum = 1;
			} else if ("普通员工".equals(position)) {
				positionNum = 2;
			} else {
				throw new HelperException(CodeMsg.POSITION_ERROR.getCode(), CodeMsg.POSITION_ERROR.getMsg());
			}
			excelIn.setPosition(positionNum);
			excelIns.add(excelIn);
		}
		return excelIns;
	}
	
	public static boolean isRowEmpty(Row row) {
	    for (int c = row.getFirstCellNum(); c < row.getLastCellNum(); c++) {
	        Cell cell = row.getCell(c);
	        if (cell != null && cell.getCellType() != Cell.CELL_TYPE_BLANK){
	            return false;
	        }
	    }
	    return true;
	}
}
