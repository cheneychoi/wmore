package com.cheney.share.mid.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class ToolUtil {
	public static Date getTime(Integer num) {
		SimpleDateFormat sdf = null;
		if (num == 0) {
			sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		} else {
			sdf = new SimpleDateFormat("yyyy-MM-dd");
		}
		String time = sdf.format(new Date());
		Date date = null;
		try {
			date = sdf.parse(time);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}

	public static List<Long> toList(String value) {
		return Arrays.stream(value.split(",")).map(s -> Long.parseLong(s.trim())).collect(Collectors.toList());
	}

	public static String replaceNotice(String info, String courseName, String courseDate, String coursePlace,
			String point) {
		return info.replaceAll("%课程名称%", courseName).replaceAll("%上课时间%", courseDate).replaceAll("%上课地点%", coursePlace)
				.replaceAll("%积分数目%", point);
	}

	public static String getRandNum(int charCount) {
		String charValue = "";
		for (int i = 0; i < charCount; i++) {
			char c = (char) (randomInt(0, 10) + '0');
			charValue += String.valueOf(c);
		}
		return charValue;
	}

	public static int randomInt(int from, int to) {
		Random r = new Random();
		return from + r.nextInt(to - from);
	}

	public static Date getNextWeek(Date date) {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(date);
		// 设置一个星期的第一天，按中国的习惯一个星期的第一天是星期一
		calendar1.setFirstDayOfWeek(Calendar.MONDAY);

		// 判断要计算的日期是否是周日，如果是则减一天计算周六的，否则会出问题，计算到下一周去了
		int dayOfWeek = calendar1.get(Calendar.DAY_OF_WEEK);// 获得当前日期是一个星期的第几天
		if (1 == dayOfWeek) {
			calendar1.add(Calendar.DAY_OF_MONTH, -1);
		}
		// 获取当前日期前（下）x周同星几的日期
		calendar1.add(Calendar.DATE, 7 * 1);
		return calendar1.getTime();
	}

	public static Date getNextMonth(Date date) {
		Calendar calendar1 = Calendar.getInstance();
		calendar1.setTime(date);
		calendar1.add(Calendar.MONTH, 1);
		return calendar1.getTime();
	}

	public static Integer getMonth(Date date) {
		Calendar c = Calendar.getInstance();
		c.setTime(date);
		return c.get(Calendar.MONTH) + 1;
	}

	public static String getDate(Date date) {
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(date);
		return dateString;
	}

	/**
	 * 转成file
	 * 
	 * @param ins
	 * @param file
	 *            void
	 * @author WangBo
	 * @date 2018年8月23日下午2:53:53
	 */
	public static void inputStreamToFile(InputStream ins, File file) {
		try {
			OutputStream os = new FileOutputStream(file);
			int bytesRead = 0;
			byte[] buffer = new byte[8192];
			while ((bytesRead = ins.read(buffer, 0, 8192)) != -1) {
				os.write(buffer, 0, bytesRead);
			}
			os.close();
			ins.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static String getTomorrowDate() {
		Date date=new Date();//取时间
		Calendar calendar = new GregorianCalendar();
		calendar.setTime(date);
		calendar.add(Calendar.DATE,1);//把日期往后增加一天.整数往后推,负数往前移动
		date=calendar.getTime(); //这个时间就是日期往后推一天的结果 
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String dateString = formatter.format(date);
		return dateString;
	}
	
	
}
