package com.cheney.share.mid.redis;

public class SessionKey extends BasePrefix{

	public SessionKey(int expireSeconds, String prefix) {
		super(expireSeconds, prefix);
	}

	public static SessionKey withExpire(int expireSeconds) {
		return new SessionKey(expireSeconds, "session");
	}
}
