package com.cheney.share.db.model;

import java.io.Serializable;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

public abstract class ModelWithBLOBs implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public abstract String getData();

	public abstract void setData(String data);

	public String getFromJson(String key) {
		if (StringUtils.isBlank(getData())) {
			return null;
		}
		return JSONObject.parseObject(getData()).getString(key);
	}

	public JSONObject mergeJsonData(JSONObject json1) {
		return mergeJsonData(json1, false);
	}

	public JSONObject mergeJsonData(JSONObject json1, boolean overwrite) {
		if (json1 == null) {
			if (StringUtils.isBlank(getData())) {
				setData("{}");
			}
			return JSONObject.parseObject(getData());
		}
		if (StringUtils.isNotBlank(getData())) {
			JSONObject jd = JSONObject.parseObject(getData());
			for (Entry<String, Object> entry : jd.entrySet()) {
				if (!json1.containsKey(entry.getKey())) {
					json1.put(entry.getKey(), entry.getValue());
				} else {
					if (entry.getValue() instanceof JSONArray && !overwrite) {
						JSONArray arr = (JSONArray) entry.getValue();
						JSONArray arr1 = json1.getJSONArray(entry.getKey());
						arr1.addAll(arr);
						json1.put(entry.getKey(), arr1);
					}
				}
			}
		}
		return json1;
	}

	public JSONObject mergeJsonData(String string) {
		if (StringUtils.isBlank(string)) {
			if (StringUtils.isBlank(getData())) {
				setData("{}");
			}
			return JSONObject.parseObject(getData());
		}
		JSONObject json1 = JSONObject.parseObject(string);
		return mergeJsonData(json1, false);
	}

	public JSONObject mergeJsonData(String string, boolean overwrite) {
		if (StringUtils.isBlank(string)) {
			if (StringUtils.isBlank(getData())) {
				setData("{}");
			}
			return JSONObject.parseObject(getData());
		}
		JSONObject json1 = JSONObject.parseObject(string);
		return mergeJsonData(json1, overwrite);
	}

	public JSONObject getJsonData() {
		if (StringUtils.isNotBlank(getData())) {
			JSONObject json = JSONObject.parseObject(getData());
			return json;
		}
		return null;
	}

	public String toString() {
		return JSON.toJSONString(this);
	}
	
}