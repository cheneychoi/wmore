package com.cheney.share.db.model;

import java.io.Serializable;

import com.alibaba.fastjson.JSON;

public class Model implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public String toString() {
		return JSON.toJSONString(this);
	}
}