package com.cheney.share.db.model;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("分页基础类")
public class Pagination<T> {
	public Pagination(int page, int rows) {
		this.page = page;
		this.rows = rows;
	}

	public Pagination(List<T> items, int records, int page, int rows) {
		this.items = items;
		this.records = records;
		this.page = page;
		this.rows = rows;
	}

	@ApiModelProperty("分页记录，特定资源数据")
	private List<T> items;
	@ApiModelProperty("当前页,如第3页")
	private int page;
	@ApiModelProperty("每页最大记录数，如每页10条记录")
	private int rows;
	@ApiModelProperty("总记录数,如特定资源共1000条记录")
	private int records;
	@ApiModelProperty("总页数")
	private int total;

	public List<T> getItems() {
		return items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}

	public int getTotal() {
		rows = rows == 0 ? 1 : rows;
		if (records % rows == 0) {
			total = records / rows;
		} else {
			total = records / rows + 1;
		}
		return total;
	}

	public int getPage() {
		return page;
	}

	public void setPage(int page) {
		this.page = page;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}

	public int getRecords() {
		return records;
	}

	public void setRecords(int records) {
		this.records = records;
	}

	/**
	 * 查询启始偏移量
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public static int offset(int pageNo, int pageSize) {
		pageNo = pageNo < 1 ? 1 : pageNo;
		int limit = limit(pageSize);
		int offset = (pageNo - 1) * limit;
		return offset;
	}

	/**
	 * 查询最大限制量
	 * 
	 * @param pageNo
	 * @param pageSize
	 * @return
	 */
	public static int limit(int pageSize) {
		int limit = pageSize < 1 ? 10 : pageSize;
		limit = pageSize > 100 ? 100 : pageSize;
		return limit;
	}
}
