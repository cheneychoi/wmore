/**
 * 基于json 的ajax
 */
(function() {
	function ajaxSend(options) {
		$.ajax({
			type : options.type,
			url : options.url,
			dataType : 'json',
			data: JSON.stringify(options.data),
			contentType : 'application/json',
			processData : false,
			success : options.callback,
			error : function(xhr, status, error) {
				console.log("status: " + status + ", error: " + error);
			}
		});
	}

	var Api = {
		get : function(url, callback, el) {
			var options = {
				"type" : "GET",
				"url" : url,
				"callback" : callback,
				"el": el
			}
			ajaxSend(options);
		},
		page: function(url, callback, el) {
			$(el).html($(el + "-template").html());
			var options = {
				"type" : "GET",
				"url" : url,
				"callback" : callback,
				"el": el
			}
			ajaxSend(options);
		},
		put : function(url, data, callback, el) {
			var options = {
				"type" : "PUT",
				"url" : url,
				"callback" : callback,
				"data": data,
				"el": el
			}
			ajaxSend(options);
		},
		post : function(url, data, callback, el) {
			var options = {
				"type" : "POST",
				"url" : url,
				"callback" : callback,
				"data": data,
				"el": el
			}
			ajaxSend(options);
		},
		del : function(url, callback, el) {
			var options = {
				"type" : "DELETE",
				"url" : url,
				"callback" : callback,
				"el": el
			}
			ajaxSend(options);
		},
		upload: function(url, form, callback) {
			var formData = null;
			if (form instanceof FormData) {
				formData = form;
			} else {
				formData = new FormData($(form)[0]);
			}
			$.ajax({
				url: url,
				type: 'POST',  
		        data: formData,  
		        async: false,  
		        cache: false,  
		        contentType: false,  
		        processData: false,
		        success: function (e) {
		        	callback(e);
		        },  
		        error: function (e) {  
		        	console.log(e);
		        }
			});
		}
	}

	window.Api = Api;
})();